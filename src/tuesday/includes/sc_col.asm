colram = 0xD800
baby1_col_to_baby2_col:
    ldy #$00
    sty colram+$01a3
    iny
    sty colram+$0131
    sty colram+$0177
    ldy #$0b
    sty colram+$014e
    sty colram+$0159
    sty colram+$0180
    sty colram+$01c8
    sty colram+$01cc
    sty colram+$01ce
    iny
    sty colram+$0181
    ldy #$0f
    sty colram+$0126
    sty colram+$0127
    sty colram+$014f
    sty colram+$0158
    sty colram+$021f
    rts
baby2_col_to_baby1_col:
    ldy #$01
    sty colram+$0127
    sty colram+$0158
    sty colram+$01cc
    ldy #$0b
    sty colram+$0126
    sty colram+$0131
    sty colram+$014f
    sty colram+$021f
    iny
    sty colram+$01c8
    ldy #$0f
    sty colram+$014e
    sty colram+$0159
    sty colram+$0177
    sty colram+$0180
    sty colram+$0181
    sty colram+$01a3
    sty colram+$01ce
    rts
