                    !cpu 6510

DEBUG = 0
LIB_INCLUDE = 0
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 0
LIB_HEX2SCREEN = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 0

LOGO = 1
SPRITES = 1
; ==============================================================================
ENABLE              = 0x20
ENABLE_JMP          = 0x4C
DISABLE             = 0x2C

BLACK               = 0x00
WHITE               = 0x01
RED                 = 0x02
CYAN                = 0x03
PURPLE              = 0x04
GREEN               = 0x05
BLUE                = 0x06
YELLOW              = 0x07
ORANGE              = 0x08
BROWN               = 0x09
PINK                = 0x0A
DARK_GREY           = 0x0B
GREY                = 0x0C
LIGHT_GREEN         = 0x0D
LIGHT_BLUE          = 0x0E
LIGHT_GREY          = 0x0F

BARCOLOR0           = PURPLE
BARCOLOR1           = CYAN
BGCOL0              = BLACK
BGCOL1              = BLUE

MEMCFG              = 0x35

IRQ_LINE0           = 0x31

OFFSET_LOGO         = 80
COLOR_SCROLL        = YELLOW
; ==============================================================================
zp_start            = 0x02
flag_irq_ready      = zp_start
zp_temp0            = flag_irq_ready+1
zp_temp0_lo         = zp_temp0
zp_temp0_hi         = zp_temp0+1
zp_temp1            = zp_temp0_hi+1
zp_temp1_lo         = zp_temp1
zp_temp1_hi         = zp_temp1+1

; ==============================================================================
KEY_CRSRUP          = 0x91
KEY_CRSRDOWN        = 0x11
KEY_CRSRLEFT        = 0x9D
KEY_CRSRRIGHT       = 0x1D
KEY_RETURN          = 0x0D
KEY_STOP            = 0x03

getin               = 0xFFE4
keyscan             = 0xEA87
; ==============================================================================
code_start          = 0x2000
vicbank0            = 0x4000
charset0            = vicbank0+0x2800
charset1            = vicbank0+0x2000
vidmem0             = vicbank0+0x3000
vidmem1             = vicbank0+0x3400
sprite_data         = vicbank0+0x3800
sprite_base         = <((sprite_data-vicbank0)/0x40)
dd00_val0           = <!(vicbank0/0x4000) & 3
d018_val0           = <(((vidmem0-vicbank0)/0x400) << 4)+ <(((charset0-vicbank0)/0x800) << 1)
d018_val1           = <(((vidmem0-vicbank0)/0x400) << 4)+ <(((charset1-vicbank0)/0x800) << 1)
d018_val2           = <(((vidmem1-vicbank0)/0x400) << 4)+ <(((charset0-vicbank0)/0x800) << 1)
data_area           = 0x8000
; ==============================================================================
                    !macro flag_set .flag {
                        lda #1
                        sta .flag
                    }
                    !macro flag_clear .flag {
                        lda #0
                        sta .flag
                    }
                    !macro flag_get .flag {
                        lda .flag
                    }
                    !source"../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !source "../../lib/sync.asm"
                    *= $1ffe
                    !byte <code_start
                    !byte >code_start
                    *= sprite_data
                    !if SPRITES=1 {
                        !bin "includes/mysprites.spd",,3
                    } else {
                        !bin "includes/testsprites.bin"
                    }
sprites_scroller:   !fi 256, 0x00
                    *= vidmem0
                    !fi OFFSET_LOGO, 0xFF
                    !if LOGO = 1 {
                        !bin "includes/logo-screendata.bin"
                    } else {
                      !fi 8*40, 0x20
                    }
                    !fi 40, 0xFF
                    !for i, 0, 79 {
                        !byte 0x20
                    }
                    !fi 11*40, 0x80
                    *= vidmem1
                    !fi 9*40, 0xFF

                    !if LOGO=1 {
                    *= charset0
                    !bin "includes/logo-fontdata.bin"
                    }
                    !for i, 0, 0x3F {
                        *= charset1+(i*8)
                        !bin "includes/insertcoin.chr",1,(i*8)
                        !bin "includes/insertcoin.chr",1,(i*8)
                        !bin "includes/insertcoin.chr",1,(i*8)+1
                        !bin "includes/insertcoin.chr",1,(i*8)+1
                        !bin "includes/insertcoin.chr",1,(i*8)+2
                        !bin "includes/insertcoin.chr",1,(i*8)+2
                        !bin "includes/insertcoin.chr",1,(i*8)+3
                        !bin "includes/insertcoin.chr",1,(i*8)+3
                    }
                    !for i, 0x40, 0x7F {
                        *= charset1+(i*8)
                        !bin "includes/insertcoin.chr",1,((i-0x40 )*8)+4
                        !bin "includes/insertcoin.chr",1,((i-0x40 )*8)+4
                        !bin "includes/insertcoin.chr",1,((i-0x40 )*8)+5
                        !bin "includes/insertcoin.chr",1,((i-0x40 )*8)+5
                        !bin "includes/insertcoin.chr",1,((i-0x40 )*8)+6
                        !bin "includes/insertcoin.chr",1,((i-0x40 )*8)+6
                        !bin "includes/insertcoin.chr",1,((i-0x40 )*8)+7
                        !bin "includes/insertcoin.chr",1,((i-0x40 )*8)+7
                    }
; ==============================================================================
                    *= code_start
                    lda #0x0B
                    sta 0xD011
                    !if RELEASE=1 { +sync 0x003166 + ( 18 * SAFETY_OFFSET ) }
                    clc
                    lda fw_global_ct
                    adc #0x00
                    sta end_time_a+1
                    lda fw_global_ct+1
                    adc #0x05
                    sta end_time_b+1
                    lda fw_global_ct+2
                    adc #0x00
                    sta end_time_c+1
init_code:          jsr init_vic
                    jsr sprites_setup
                    jsr init_irq
                    jmp mainloop

init_irq:           jsr fw_wait_irq
                    sei
                    lda #IRQ_LINE0
                    sta 0xD012
                    lda #<irq
                    sta 0xFFFE
                    lda #>irq
                    sta 0xFFFF
                    lda #0x1B
                    sta 0xD011
                    asl 0xD019
                    cli
                    rts

init_vic:           lda #dd00_val0
                    sta 0xDD00
                    lda #d018_val2
                    sta 0xD018
                    lda #0x00
                    sta vicbank0+0x3FFF
                    ldx #0x00
-                   lda colram_logo_src,x
                    sta 0xD800+OFFSET_LOGO,x
                    lda colram_logo_src+0x100,x
                    sta 0xD900+OFFSET_LOGO,x
                    inx
                    bne -
                    lda #COLOR_SCROLL
-                   sta 0xD800+(10*40),x
                    sta 0xD800+(11*40),x
                    inx
                    cpx #0x28
                    bne -
                    lda #BLACK
                    sta 0xD800+(10*40)+39
                    sta 0xD800+(11*40)+39
                    lda #0x0A
                    sta 0xD022
                    lda #0x07
                    sta 0xD023
                    rts
code_exit:          sei
                    lda #0
                    sta 0xD015
                    lda #<fw_irq
                    sta $fffe
                    lda #>fw_irq
                    sta $ffff
                    lda #$0b
                    sta $d011
                    lda #$00
                    sta $d012
                    dec $d019
                    cli
                    !if RELEASE = 0 {
                         jmp *
                    } else {
                         jmp fw_back2framework
                    }
; ==============================================================================
                    !zone MAINLOOP
mainloop:           jsr wait_irq
enable_wait:        jsr wait
enable_logo:        bit logo_show
enable_scroller:    bit scroller_start
enable_fld:         bit fld_start
                    jsr check_end
                    lda fade_out_flag
                    bne mainloop_fade_out
                    jmp mainloop
mainloop_fade_out:  jsr wait0
                    lda #d018_val2
                    sta d018_val_mod+1
                    jsr wait0
                    jmp code_exit
fade_out_flag:      !byte 0x00
check_end:          rts
                    lda fw_global_ct
end_time_a:         cmp #0
                    bne +
                    lda fw_global_ct+1
end_time_b:         cmp #0
                    bne +
                    lda fw_global_ct+2
end_time_c:         cmp #0
                    bne +
                    inc fade_out_flag
+                   rts
; ==============================================================================
logo_show:          lda #d018_val0
                    sta d018_val_mod+1
                    lda #DISABLE
                    sta enable_logo
                    lda #ENABLE
                    sta enable_wait
                    rts
fld_start:          lda #0xE8
                    sta fld_mod
                    lda #DISABLE
                    sta enable_fld
                    rts
scroller_start:     lda #<scroller
                    sta enable_scroller+1
                    lda #>scroller
                    sta enable_scroller+2
                    lda #ENABLE
                    sta enable_scroller
                    sta enable_wait
                    rts
; ==============================================================================
                    !zone WAIT
wait_irq:           +flag_clear flag_irq_ready
.wait_irq:          +flag_get flag_irq_ready
                    beq .wait_irq
                    rts
wait:               lda #0xC0
                    beq +
                    dec wait+1
                    rts
+                   lda waittime
                    sta wait+1
                    lda #DISABLE
                    sta enable_wait
.enable_pt:         ldx #0
                    lda .enable_tab_lo,x
                    sta .enable+1
                    lda .enable_tab_hi,x
                    sta .enable+2
                    lda #ENABLE
.enable:            sta 0x0000
                    inc .enable_pt+1
                    rts
.enable_tab_lo:     !byte <enable_logo
                    !byte <enable_scroller
                    !byte <enable_fld
                    !byte <enable_fld
                    !byte <enable_fld

.enable_tab_hi:     !byte >enable_logo
                    !byte >enable_scroller
                    !byte >enable_fld
                    !byte <enable_fld
                    !byte <enable_fld

waittime:           !byte 0xC0
wait0:              ldx waittime
-                   jsr wait_irq
                    dex
                    bne -
                    rts
; ==============================================================================
                    !zone SPRITE_SETUP
sprites_setup:      lda #IRQ_LINE0+0x19+2
                    sta 0xD009
                    sta 0xD00B
                    sta 0xD00D
                    sta 0xD00F
                    !if DEBUG=1 {
                        lda #0x20
                    } else {
                        lda #0xEB
                    }
                    sta 0xD008
                    !if DEBUG=1 {
                        lda #0x40
                    } else {
                        lda #0x00
                    }
                    sta 0xD00A
                    !if DEBUG=1 {
                        lda #0x10
                    } else {
                        lda #0x58
                    }
                    sta 0xD00C
                    !if DEBUG=1 {
                        lda #0x30
                    } else {
                        lda #0x64
                    }
                    sta 0xD00E
                    lda #0x00
                    sta 0xD017
                    sta 0xD01D
                    sta 0xD01C
                    !if DEBUG=1 {
                        lda #%11000000
                    } else {
                        lda #%11010000
                    }
                    sta 0xD010
                    lda #sprite_base+0
                    sta vidmem1+0x3F8+4
                    sta vidmem1+0x3F8+5
                    sta vidmem1+0x3F8+6
                    sta vidmem1+0x3F8+7
                    rts
; ==============================================================================
                    !zone IRQ
                    !align 255,0,0
                                                  ; ( 07 / 07 )
irq:                sta .irq_savea+1              ; ( 04 / 11 )
                    stx .irq_savex+1              ; ( 04 / 15 )
                    sty .irq_savey+1              ; ( 04 / 19 )
                    lda #<irq_next                ; ( 02 / 21 )
                    sta 0xFFFE                    ; ( 04 / 25 )
                    lda #>irq_next                ; ( 02 / 27 )
                    sta 0xFFFF                    ; ( 04 / 31 )
                    inc 0xD012                    ; ( 06 / 37 )
                    asl 0xD019                    ; ( 06 / 43 )
                    tsx                           ; ( 02 / 45 )
                    cli                           ; ( 02 / 47 )
                    !fi 14, 0xEA
irq_next:           txs
                    ldx #0x08
-                   dex
                    bne -
                    bit 0xEA
                    nop
                    lda #IRQ_LINE0+1
                    cmp 0xD012
                    beq +
+                   ; irq starts here / Zeile $33
                    lda #0x18
                    sta 0xD016
badline_delay:      ldx #0x11
--                  lda 0xD012        ; 04 (04)
-                   cmp 0xD012        ; 04 (08)
                    beq -             ; 02 (10)

                    clc               ; 02 (12)
                    lda 0xD011        ; 04 (16)
                    adc #$01          ; 02 (18)
                    and #$07          ; 02 (20)
                    ora #$18          ; 02 (22)
                    sta 0xD011        ; 04 (26) // 31 (05 von unten)

                    jsr fake          ; 12 (43)
                    jsr fake          ; 12 (55)

                    bit 0xEA          ; 03 (58)
                    bit 0xEA          ; 03 (61)
                    nop               ; 02 (63)

                    dex               ; 02 (02)
                    bne --            ; 03 (05) siehe oben
                    clc
                    lda 0xD012
                    and #%00000111
                    adc #$01
                    sta 0xD011
                    jsr fake
                    lda #BARCOLOR0
                    sta 0xD020
                    sta 0xD021
                    ldx #0x0A
-                   dex
                    bne -
                    nop
                    lda #BARCOLOR1
                    sta 0xD020
                    sta 0xD021
                    ldx #0x0A
-                   dex
                    bne -
                    nop
                    lda #BARCOLOR0
                    sta 0xD020
                    sta 0xD021
                    ldx #0x08
-                   dex
                    bne -
                    bit 0xEA
                    bit 0xEA
                    nop
                    lda #BGCOL0
                    sta 0xD021
                    sta 0xD020
                    bit 0xEA
                    bit 0xEA
                    ldx #0x02
--                  lda 0xD012
-                   cmp 0xD012
                    beq -
                    dex
                    bne --
                    lda #0x10
                    ldy #0x18
                    jsr fake
                    jsr fake
                    bit 0xEA
                    bit 0xEA
                    bit 0xEA
                    nop
                    nop
                    lda #BLUE
                    sta 0xD02E
                    lda #0x10
                    jsr fake
                    jsr fake
                    jsr fake
                    jsr .ob_sprite_line_fix   ; 02 (02)
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line_fix   ; (10)
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line_fix   ; (18)
                    bit 0xEA
                    bit 0xEA
                    !if DEBUG=1 {
                        sta 0xD021
                        sty 0xD021            ; (19)
                    } else {
                        sta 0xD016
                        sty 0xD016
                    }
sprite_y01:         lda #0x3B+0x11+21
                    sta 0xD009
                    sta 0xD00B
                    sta 0xD00D
                    sta 0xD00F
                    jsr fake
                    bit 0xEA
                    nop
                    nop
                    lda #0x10
                    bit 0xEA
                    nop
                    !if DEBUG=1 {
                        sta 0xD021
                        sty 0xD021            ; (20)
                    } else {
                        sta 0xD016
                        sty 0xD016
                    }
                    lda #sprite_base+0
                    sta vidmem0+0x3F8+4
                    lda #sprite_base+6
                    sta vidmem0+0x3F8+5
                    lda #sprite_base+7
                    sta vidmem0+0x3F8+6
                    lda #sprite_base+0
                    sta vidmem0+0x3F8+7
                    jsr fake
                    lda #0x10
                    jsr .ob_sprite_line       ; (21)
                    jsr .ob_sprite_line       ; 01
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line_fix   ; 01 (05)
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line_fix
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line       ; (18)
                    bit 0xEA
                    bit 0xEA
                    !if DEBUG=1 {
                        sta 0xD021
                        sty 0xD021            ; (19)
                    } else {
                        sta 0xD016
                        sty 0xD016
                    }
sprite_y02:         lda #0x3B+0x11+42
                    sta 0xD009
                    sta 0xD00B
                    sta 0xD00D
                    sta 0xD00F
                    lda #sprite_base+0
                    sta vidmem0+0x3F8+4
                    lda #sprite_base+0
                    sta vidmem0+0x3F8+5
                    lda #sprite_base+5
                    sta vidmem0+0x3F8+6
                    lda #sprite_base+4
                    sta vidmem0+0x3F8+7
                    lda #0x10
                    !if DEBUG=1 {
                        sta 0xD021            ; (20)
                        sty 0xD021
                        sta 0xD021,x
                        sty 0xD021
                    } else {
                        sta 0xD016            ; (20)
                        sty 0xD016
                        sta 0xD016,x
                        sty 0xD016
                    }
                    jsr fake
                    jsr fake
                    jsr fake
                    nop
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line_fix
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line_fix
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    bit 0xEA
                    bit 0xEA
                    !if DEBUG=1 {
                        sta 0xD021
                        sty 0xD021            ; (19)
                    } else {
                        sta 0xD016
                        sty 0xD016
                    }
sprite_y03:         lda #0x3B+0x11+42+21
                    sta 0xD009
                    sta 0xD00B
                    sta 0xD00D
                    sta 0xD00F
                    lda #PINK
                    sta 0xD02C
                    sta 0xD02D
                    nop
                    lda #RED
                    sta 0xD02B
                    sta 0xD02E
                    nop
                    lda #0x10
                    !if DEBUG=1 {
                        sta 0xD021
                        sty 0xD021            ; (19)
                    } else {
                        sta 0xD016
                        sty 0xD016
                    }
spr_pt_mod0:        lda #sprite_base+8
                    sta vidmem0+0x3F8+4
spr_pt_mod1:        lda #sprite_base+9
                    sta vidmem0+0x3F8+5
spr_pt_mod2:        lda #sprite_base+10
                    sta vidmem0+0x3F8+6
spr_pt_mod3:        lda #sprite_base+11
                    sta vidmem0+0x3F8+7
                    lda #d018_val1
                    sta 0xD018
d016_b0:            lda #0x00
d016_b1:            ldy #0x08
                    nop
                    nop
                    sta 0xD016
                    sty 0xD016
                    lda #0x09
                    sta 0xD00A
                    lda #0x51
                    sta 0xD00C
                    lda #0x51+24
                    sta 0xD00E
                    lda #0xE9
                    sta 0xD008
                    jsr fake
                    nop
                    nop
                    lda d016_b0+1
                    jsr .ob_sprite_line_fix
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line_fix
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line_fix
                    jsr .ob_sprite_line
                    jsr .ob_sprite_line
                    ldx #0x03
--                  lda 0xD012
-                   cmp 0xD012
                    beq -
                    dex
                    bne --
.modxdelay:         ldx #0
                    lda delay_table,x
                    sta badline_delay+1
fld_mod:            nop ;inx
                    stx .modxdelay+1
                    jsr fake
                    jsr fake
                    bit 0xEA
                    bit 0xEA
                    nop
                    lda #BARCOLOR0
                    sta 0xD020
                    sta 0xD021
                    ldx #0x0A
-                   dex
                    bne -
                    nop
                    lda #BARCOLOR1
                    sta 0xD020
                    sta 0xD021
                    ldx #0x02
-                   dex
                    bne -
                    nop
                    lda #BARCOLOR0
                    sta 0xD020
                    sta 0xD021
                    ldx #0x08
-                   dex
                    bne -
                    bit 0xEA
                    bit 0xEA
                    nop
                    lda #BGCOL1
                    sta 0xD021
                    sta 0xD020
                    lda #0x1B
                    sta 0xD011
                    lda #0x18
                    sta 0xD016
                    +flag_set flag_irq_ready
                    lda #0x00
                    sta 0xD012
                    lda #<irq_top
                    sta 0xFFFE
                    lda #>irq_top
                    sta 0xFFFF
irq_end:            asl 0xD019
.irq_savea:         lda #0
.irq_savex:         ldx #0
.irq_savey:         ldy #0
                    rti
irq_top:            sta .irq_savea+1
                    stx .irq_savex+1
                    sty .irq_savey+1
                    lda #0x1B
                    sta 0xD011
                    lda #0x18
                    sta 0xD016
                    lda #sprite_base+1
                    sta vidmem0+0x3F8+4
                    lda #sprite_base+2
                    sta vidmem0+0x3F8+5
                    lda #sprite_base+3
                    sta vidmem0+0x3F8+6
                    lda #sprite_base+0
                    sta vidmem0+0x3F8+7
                    clc
                    lda #0x3B
                    adc badline_delay+1
                    sta 0xD009
                    sta 0xD00B
                    sta 0xD00D
                    sta 0xD00F

                    clc
                    lda #0x3B+21
                    adc badline_delay+1
                    sta sprite_y01+1

                    clc
                    lda #0x3B+42
                    adc badline_delay+1
                    sta sprite_y02+1

                    clc
                    lda #0x3B+42+21
                    adc badline_delay+1
                    sta sprite_y03+1

                    lda #%11110000
                    sta 0xD015

                    lda #RED
                    sta 0xD02B
                    sta 0xD02C

                    lda #BLUE
                    sta 0xD02D
                    lda #BLACK
                    sta 0xD02E

                    !if DEBUG=1 {
                        lda #0x20
                    } else {
                        lda #0xEB
                    }
                    sta 0xD008
                    !if DEBUG=1 {
                        lda #0x40
                    } else {
                        lda #0x00
                    }
                    sta 0xD00A

                    !if DEBUG=1 {
                        lda #0x10
                    } else {
                        lda #0x58
                    }
                    sta 0xD00C
                    !if DEBUG=1 {
                        lda #0x30
                    } else {
                        lda #0x64
                    }
                    sta 0xD00E

d018_val_mod:       lda #d018_val2
                    sta 0xD018

                    lda d016_b0+1
                    and #%11111000
                    ora d016_bits012
                    sta d016_b0+1

                    lda d016_b1+1
                    and #%11111000
                    ora d016_bits012
                    sta d016_b1+1

                    lda #IRQ_LINE0
                    sta 0xD012
                    lda #<irq
                    sta 0xFFFE
                    lda #>irq
                    sta 0xFFFF
                    !if DEBUG=1 {
                        inc 0xD020
                        dec 0xD020
                    }
                    jmp irq_end
!align 255,0
.ob_normal_line:    !if DEBUG=1 {
                        sta 0xD021
                        sty 0xD021
                    } else {
                        sta 0xD016
                        sty 0xD016
                    }
                    ldx #0x08
-                   dex
                    bne -
                    nop
                    rts
.ob_sprite_line:    !if DEBUG=1 {
                        sta 0xD021
                        sty 0xD021
                    } else {
                        sta 0xD016
                        sty 0xD016
                    }
                    ldx #0x05           ; 2
-                   dex                 ; .
                    bne -               ; 5 | 10 | 15 | 20 | 24
                    bit 0xEA            ; 3 ( 27 )
                    bit 0xEA            ; 3 ( 30 )
                    rts                 ; 6 ( 36 )
.ob_sprite_line_fix:!if DEBUG=1 {
                        sta 0xD021
                        sty 0xD021
                        sta 0xD021,x
                        sty 0xD021
                    } else {
                        sta 0xD016
                        sty 0xD016
                        sta 0xD016,x
                        sty 0xD016
                    }
                    ldx #0x05
-                   dex
                    bne -
                    bit 0xEA
                    bit 0xEA
fake:               rts
d016_bits012:       !byte 0x00
; ==============================================================================
scroller:           LINE_SCROLLER       = vidmem0+(10*40)
.scroll:            lda #0x07
                    sec
                    sbc #0x02
                    bcs +
                    jsr .get_text
                    sta char_next+6
                    jsr .char_to_buffer_r
                    jsr .char_to_buffer_l
                    lda char_next
                    sta LINE_SCROLLER+39
                    eor #0x40
                    sta LINE_SCROLLER+(1*40)+39
                    jsr .hardscroll
                    lda #0x07
+                   sta .scroll+1
                    sta d016_bits012
                    jsr .rol_right
                    jsr .rol_right
                    jsr .rol_left
                    jsr .rol_left
                    rts
.hardscroll:        ldx #0
-                   lda LINE_SCROLLER+1,x
                    sta LINE_SCROLLER,x
                    lda LINE_SCROLLER+(1*40)+1,x
                    sta LINE_SCROLLER+(1*40),x
                    inx
                    cpx #0x27
                    bne -
                    lda char_next+1
                    sta char_next
                    lda char_next+2
                    sta char_next+1
                    lda char_next+3
                    sta char_next+2
                    lda char_next+4
                    sta char_next+3
                    lda char_next+5
                    sta char_next+4
                    lda char_next+6
                    sta char_next+5
                    rts
.get_text:
.pt_scrolltext:     lda scrolltext
                    cmp #0xFF
                    beq .text_reset
                    tay
                    clc
                    lda .pt_scrolltext+1
                    adc #0x01
                    sta .pt_scrolltext+1
                    lda .pt_scrolltext+2
                    adc #0x00
                    sta .pt_scrolltext+2
                    tya
                    rts
.text_reset:        lda #<scrolltext
                    sta .pt_scrolltext+1
                    lda #>scrolltext
                    sta .pt_scrolltext+2
                    lda #0x01
                    sta fade_out_flag
                    lda #' '
                    rts
.char_to_buffer_r:  asl
                    bcc +
                    inc .mod0+2
                    clc
+                   asl
                    bcc +
                    inc .mod0+2
                    clc
+                   asl
                    bcc +
                    inc .mod0+2
+                   sta .mod0+1
                    ldy #1
-                   ldx #7
.mod0:              lda charset1,x
.mod1:              sta charbuffer_r,x
                    dex
                    bpl .mod0
                    inc .mod0+2
                    inc .mod0+2
                    clc
                    lda .mod1+1
                    adc #8
                    sta .mod1+1
                    dey
                    bpl -
                    lda #<charset1
                    sta .mod0+1
                    lda #>charset1
                    sta .mod0+2
                    lda #<charbuffer_r
                    sta .mod1+1
                    rts
.char_to_buffer_l:  lda LINE_SCROLLER+1
                    asl
                    bcc +
                    inc .mod4+2
                    clc
+                   asl
                    bcc +
                    inc .mod4+2
                    clc
+                   asl
                    bcc +
                    inc .mod4+2
+                   sta .mod4+1
                    ldy #1
-                   ldx #7
.mod4:              lda charset1,x
.mod5:              sta charbuffer_l,x
                    dex
                    bpl .mod4
                    inc .mod4+2
                    inc .mod4+2
                    clc
                    lda .mod5+1
                    adc #8
                    sta .mod5+1
                    dey
                    bpl -
                    lda #<charset1
                    sta .mod4+1
                    lda #>charset1
                    sta .mod4+2
                    lda #<charbuffer_l
                    sta .mod5+1
                    rts
.rol_right:         ldy #0
                    ldx #0
.mod2:              asl charbuffer_r
                    rol sprites_scroller+((3*64)+2)+6,x
                    rol sprites_scroller+((3*64)+1)+6,x
                    rol sprites_scroller+((3*64)+0)+6,x
                    rol sprites_scroller+((2*64)+2)+6,x
                    rol sprites_scroller+((2*64)+1)+6,x
                    rol sprites_scroller+((2*64)+0)+6,x
                    inx
                    inx
                    inx
                    inc .mod2+1
                    iny
                    cpy #16
                    bne .mod2
                    lda #<charbuffer_r
                    sta .mod2+1
                    rts
.rol_left:          ldy #0
                    ldx #0
.mod3:              asl charbuffer_l
                    rol sprites_scroller+((1*64)+2)+6,x
                    rol sprites_scroller+((1*64)+1)+6,x
                    rol sprites_scroller+((1*64)+0)+6,x
                    rol sprites_scroller+((0*64)+2)+6,x
                    rol sprites_scroller+((0*64)+1)+6,x
                    rol sprites_scroller+((0*64)+0)+6,x
                    inx
                    inx
                    inx
                    inc .mod3+1
                    iny
                    cpy #16
                    bne .mod3
                    lda #<charbuffer_l
                    sta .mod3+1
                    rts
                    !align 16, 0, 0
charbuffer_l:       !fi 16,0
charbuffer_r:       !fi 16,0
char_next:          !fi 7, 0x20
                    !align 1, 0, 0
scrolltext:
!scr "  greetings time! cheers and beers to:          "
!scr " + abyss connection + arsenic + atlantis + "
!scr "bauknecht + bcc + bliss + bonzai + booze + "
!scr "camelot + cascade + censor + chorus + cosine + covert bitops + crest + "
!scr "dekadence + delysid + desire + dmagic + dreams + "
!scr "excess + exclusive on + extend + "
!scr "f4cg + fairlight + finnish gold + fossil + "
!scr "genesis project + "
!scr "hack n'trade + hoaxers + hokuto force + "
;!scr "insane + "
!scr "k2 + "
!scr "laxity + "
!scr "multistyle labs + "
!scr "neoplasia + "
!scr "offence + onslaught + oxyron + "
!scr "padua + panda design + performers + plush + poo-brain + prosonix + pvm + "
!scr "rabenauge + radwar + resource + role + "
!scr "samar + siesta + singular + sitzgruppe + stonehead + "
!scr "the solution + triad + trsi + "
!scr "vision + vulture design + "
!scr "wow + wrath designs   "
!fi 49, 0x20
!byte 0xFF
; ==============================================================================
                    !zone LIB
library:            !if LIB_INCLUDE = 1 {
                         !source"../../lib/library.asm"
                    }
code_end:
; ==============================================================================
                    !zone DATA
                    *= data_area
colram_logo_src:    !bin "includes/logo-colordata.bin"
colram_logo_src_end:
                    !align 255, 0, 0
delay_table:
                    !byte 17,17,17,18,18,19,21,22,24,26,28,30,32,34,37,39
                    !byte 42,44,47,49,51,53,56,58,59,61,62,64,65,66,66,66
                    !byte 66,66,66,65,64,63,62,61,59,57,55,53,51,48,46,43
                    !byte 41,39,36,34,31,29,27,25,23,22,20,19,18,17,17,17
                    !byte 17,17,18,19,20,22,24,26,29,31,34,37,41,44,47,51
                    !byte 54,58,61,64,67,70,73,76,78,80,82,84,85,86,86,86
                    !byte 86,86,85,84,83,81,79,76,74,71,68,65,62,59,55,52
                    !byte 48,45,42,38,35,32,29,27,25,22,21,19,18,17,17,17
                    !byte 17,17,17,18,19,20,21,23,25,27,29,31,33,36,38,40
                    !byte 43,45,48,50,52,55,57,58,60,62,63,64,65,66,66,66
                    !byte 66,66,66,65,64,63,61,60,58,56,54,52,50,47,45,42
                    !byte 40,37,35,33,30,28,26,24,23,21,20,19,18,17,17,17
                    !byte 17,17,18,19,20,22,24,26,29,31,34,37,41,44,47,51
                    !byte 54,58,61,64,67,70,73,76,78,80,82,84,85,86,86,86
                    !byte 86,86,85,84,83,81,79,76,74,71,68,65,62,59,55,52
                    !byte 48,45,42,38,35,32,29,27,25,22,21,19,18,17,17,17
data_end:
; ==============================================================================
