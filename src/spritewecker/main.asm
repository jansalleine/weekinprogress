DEBUG = 0
LIB_INCLUDE = 1
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 1
LIB_HEX2SCREEN = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 1

ENABLE              = 0x20
DISABLE             = 0x2C

BLACK               = 0x00
WHITE               = 0x01
RED                 = 0x02
CYAN                = 0x03
PURPLE              = 0x04
GREEN               = 0x05
BLUE                = 0x06
YELLOW              = 0x07
ORANGE              = 0x08
BROWN               = 0x09
PINK                = 0x0A
DARK_GREY           = 0x0B
GREY                = 0x0C
LIGHT_GREEN         = 0x0D
LIGHT_BLUE          = 0x0E
LIGHT_GREY          = 0x0F

BGCOL0              = BROWN
BGCOL1              = LIGHT_GREEN
BGCOL2              = BLACK
BGCOL3              = LIGHT_GREY

COLOR_BOTTOM        = GREY

LINE_IRQ            = 0xF9

FIRSTDOT_X          = 19
FIRSTDOT_Y          = 6

IMPORTANT_POINTER   = 0x1FFD
; ==============================================================================
zp_start            = 0x10
irq_ready           = zp_start
; ==============================================================================
vicbank0            = 0x0000
charset0            = vicbank0+0x2000
vidmem0             = vicbank0+0x0400
sprite_base         = <((sprite_data-vicbank0)/0x40)
colram              = 0xD800
dd00_val0           = <!(vicbank0/0x4000) & 3
d018_val0           = <(((vidmem0-vicbank0)/0x400) << 4)+ <(((charset0-vicbank0)/0x800) << 1)
; ==============================================================================
                    !cpu 6510
                    !source"../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !source "../../lib/sync.asm"
; ==============================================================================
                    *= $1ffe
                    !byte <code_start
                    !byte >code_start
; ==============================================================================
                    !byte $00 XOR $ff, $00 XOR $ff, $00 XOR $ff, $00 XOR $ff
                    !byte $00 XOR $ff, $00 XOR $ff, $00 XOR $ff, $00 XOR $ff ;0
                    !byte $00 XOR $ff, $01 XOR $ff, $03 XOR $ff, $07 XOR $ff
                    !byte $0f XOR $ff, $1f XOR $ff, $3f XOR $ff, $7f XOR $ff ;1
                    !byte $ff XOR $ff, $ff XOR $ff, $ff XOR $ff, $ff XOR $ff
                    !byte $ff XOR $ff, $ff XOR $ff, $ff XOR $ff, $ff XOR $ff ;2
                    !byte $00 XOR $ff, $80 XOR $ff, $c0 XOR $ff, $e0 XOR $ff
                    !byte $f0 XOR $ff, $f8 XOR $ff, $fc XOR $ff, $fe XOR $ff ;3
                    !byte $ff XOR $ff, $7f XOR $ff, $3f XOR $ff, $1f XOR $ff
                    !byte $0f XOR $ff, $07 XOR $ff, $03 XOR $ff, $01 XOR $ff ;4
                    !byte $ff XOR $ff, $fe XOR $ff, $fc XOR $ff, $f8 XOR $ff
                    !byte $f0 XOR $ff, $e0 XOR $ff, $c0 XOR $ff, $80 XOR $ff ;5
                    !byte $00 XOR $ff, $00 XOR $ff, $00 XOR $ff, $00 XOR $ff
                    !byte $0f XOR $ff, $0f XOR $ff, $0f XOR $ff, $0f XOR $ff ;6
                    !byte $00 XOR $ff, $00 XOR $ff, $00 XOR $ff, $00 XOR $ff
                    !byte $f0 XOR $ff, $f0 XOR $ff, $f0 XOR $ff, $f0 XOR $ff ;7
                    !byte $0f XOR $ff, $0f XOR $ff, $0f XOR $ff, $0f XOR $ff
                    !byte $00 XOR $ff, $00 XOR $ff, $00 XOR $ff, $00 XOR $ff ;8
                    !byte $f0 XOR $ff, $f0 XOR $ff, $f0 XOR $ff, $f0 XOR $ff
                    !byte $00 XOR $ff, $00 XOR $ff, $00 XOR $ff, $00 XOR $ff ;9
                    ;~ !byte $00 XOR $00, $00 XOR $00, $00 XOR $00, $00 XOR $00
                    ;~ !byte $00 XOR $00, $00 XOR $00, $00 XOR $00, $00 XOR $00 ;10
                    !align 0x40, 0, 0
                    !fi 0x40, 0
sprite_data:        !bin "includes/sprites.spd",, 3
; ==============================================================================
                    !zone IRQ
irq:                pha
                    lda $dd04
                    eor #7
                    sta *+4
                    bpl *+2
                    cmp #$c9
                    cmp #$c9
                    bit $ea24
                    bit $ea24
                    jmp *+8
                    nop
                    nop
                    jmp *+3
                    txa
                    pha
                    tya
                    pha
color_bottom:       lda #COLOR_BOTTOM
                    jsr fake
                    jsr fake
                    nop
                    nop
                    nop
                    nop
                    ldx #0
                    sta 0xD020,x
                    !if DEBUG=1 { dec 0xD020 }
enable_anim_sync:   bit anim_sync
enable_anim_dots:   bit anim_dots
enable_anim_all:    bit anim_all
                    lda 0xD012
-                   cmp 0xD012
                    beq -
                    lda #<irq_top
                    sta 0xFFFE
                    lda #>irq_top
                    sta 0xFFFF
                    lda #0x00
                    sta 0xD012
                    lda #0x5B
                    sta 0xD011
                    !if DEBUG=1 { inc 0xD020 }
                    lda #0x01
                    sta irq_ready
irq_end:            asl 0xD019
                    pla
                    tay
                    pla
                    tax
                    pla
                    rti
irq_top:            pha
                    txa
                    pha
                    tya
                    pha
                    lda #BLACK
                    sta 0xD020
                    lda #BGCOL2
                    sta 0xD023
                    !if DEBUG=1 { dec 0xD020 }
enable_sprites_anim:jsr sprites_anim
enable_fade_in:     bit fade_in
enable_fade_out_screen:
                    bit fade_out_screen
                    !if DEBUG=1 { inc 0xD020 }
                    lda 0xD012
-                   cmp 0xD012
                    beq -
                    lda #<irq
                    sta 0xFFFE
                    lda #>irq
                    sta 0xFFFF
                    lda #LINE_IRQ
                    sta 0xD012
                    jmp irq_end
fake:               rts
; ==============================================================================
                    !zone CODE_START
code_start:         lda #0x0B
                    sta 0xD011
                    ; ?!? macht eigentlich wenig Sinn hier +sync 0x20, 0x00, 0x00
                    ;~ lda #7
                    ;~ sta IMPORTANT_POINTER
                    jsr init_day
                    jsr init_vic
                    jsr sprites_init
                    lda #0xFF
                    sta 0xD015
                    jsr init_irq
mainloop:           jsr wait_irq
                    lda #0xA0
-                   cmp 0xD012
                    bne -
ml_enable0:         bit print_number
ml_enable1:         bit print_last_plus
disable_mainloop:   jmp mainloop
mainloop_wait:      jsr wait_irq
                    lda fade_out_ready_flag
                    beq mainloop_wait
code_exit:          inc IMPORTANT_POINTER
                    lda IMPORTANT_POINTER
                    cmp #8
                    bne +
                    sei
                    lda #0x37
                    sta 0x01
                    jmp $fce2
+                   sei
                    lda #<fw_irq
                    sta $fffe
                    lda #>fw_irq
                    sta $ffff
                    lda #$0b
                    sta $d011
                    lda #$00
                    sta $d012
                    dec $d019
                    cli
                    !if RELEASE = 0 {
                         jmp *
                    } else {
                         jmp fw_back2framework
                    }
wait_irq:           lda #0x00
                    sta irq_ready
-                   lda irq_ready
                    beq -
                    rts
fade_out_ready_flag:!byte 0x00
; ==============================================================================
                    !zone INIT
init_vic:           lda #dd00_val0
                    sta 0xDD00
                    lda #d018_val0
                    sta 0xD018
                    lda #BLACK
                    sta 0xD020
                    lda #BGCOL0
                    sta 0xD021
                    lda #BGCOL1
                    sta 0xD022
                    lda #BGCOL2
                    sta 0xD023
                    lda #BGCOL3
                    sta 0xD024
                    lda #0x08
                    sta 0xD016
                    lda #0
                    jsr lib_colramfill
                    ldx #>vidmem0
                    jsr lib_vidmemfill
                    ;~ ldx #39
                    ;~ lda #0x8A
;~ -                   sta vidmem0 + ( 24 * 40 ),x
                    ;~ dex
                    ;~ bpl -
                    rts
init_irq:           jsr fw_wait_irq
                    sei
                    lda #<irq_top
                    sta 0xFFFE
                    lda #>irq_top
                    sta 0xFFFF
                    lda #0x00
                    sta 0xD012
                    lda #0x0B
                    sta 0xD011
                    asl 0xD019
                    cli
                    rts
init_day:           ldx IMPORTANT_POINTER
                    !if RELEASE=1 {
                        lda sync_tab_lo,x
                        sta .sync+1
                        lda sync_tab_hi,x
                        sta .sync+2
.sync:                  jsr fake
                    }
                    cpx #7
                    bne +
                    inc fade_out_mod+1
+                   lda day_tab_lo,x
                    sta .day_data0+1
                    sta .day_data1+1
                    lda day_tab_hi,x
                    sta .day_data0+2
                    sta .day_data1+2
                    lda rand_tab_lo,x
                    sta rand_tab_mod+1
                    lda rand_tab_hi,x
                    sta rand_tab_mod+2
                    ldx #11
.day_data0:         lda 0x0000,x
                    sta current_day0,x
                    dex
                    bpl .day_data0
                    ldx #12
.day_data1:         lda 0x0000,x
                    sta number_to_print-12,x
                    inx
                    cpx #16
                    bne .day_data1
                    rts
; ==============================================================================
                    !zone ANIM
                    ;ANIM_DOTS_SPEED = 25
anim_dots:
                    lda vidmem0 + ( ( FIRSTDOT_Y + 0 ) *40 ) + ( FIRSTDOT_X + 0 )
                    eor #( 0x06 XOR 0x46 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 0 ) *40 ) + ( FIRSTDOT_X + 0 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 2 ) *40 ) + ( FIRSTDOT_X + 0 )
                    lda vidmem0 + ( ( FIRSTDOT_Y + 0 ) *40 ) + ( FIRSTDOT_X + 1 )
                    eor #( 0x07 XOR 0x47 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 0 ) *40 ) + ( FIRSTDOT_X + 1 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 2 ) *40 ) + ( FIRSTDOT_X + 1 )
                    lda vidmem0 + ( ( FIRSTDOT_Y + 1 ) *40 ) + ( FIRSTDOT_X + 0 )
                    eor #( 0x08 XOR 0x48 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 1 ) *40 ) + ( FIRSTDOT_X + 0 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 3 ) *40 ) + ( FIRSTDOT_X + 0 )
                    lda vidmem0 + ( ( FIRSTDOT_Y + 1 ) *40 ) + ( FIRSTDOT_X + 1 )
                    eor #( 0x09 XOR 0x49 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 1 ) *40 ) + ( FIRSTDOT_X + 1 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 3 ) *40 ) + ( FIRSTDOT_X + 1 )
                    cmp #0x49
                    bne +
disable_enable0:    lda #ENABLE
                    sta ml_enable1
+                   lda #DISABLE
                    sta enable_anim_dots
                    rts
dotcounter:         !byte 0x00
                    ANIM_SPEED = 25
anim_sync:          lda #ANIM_SPEED
                    beq +
                    dec anim_sync+1
                    rts
+                   lda #ANIM_SPEED
                    sta anim_sync+1
                    lda #ENABLE
                    sta enable_anim_dots
disable_enable1:    lda #DISABLE
                    sta enable_anim_all
                    rts
anim_all:           lda 0xD022
                    eor #( BGCOL1 XOR BGCOL0 )
                    sta 0xD022
                    lda #DISABLE
                    sta enable_anim_all
                    dec counter_all
                    lda counter_all
                    bne +
                    lda #ENABLE
                    sta enable_fade_out_screen
                    lda #DISABLE
                    sta disable_mainloop
+                   rts
counter_all:        !byte 0x04
; ==============================================================================
                    !zone PRINT
print_digit:        ; Y = COLUMN of digit 0 - 3
                    ; X = NUMBER of digit 0 - 9
                    lda digit_tab_lo,x
                    sta .load_digit+1
                    lda digit_tab_hi,x
                    sta .load_digit+2
                    lda digit_col_tab_lo,y
                    sta .store_digit+1
                    lda digit_col_tab_hi,y
                    sta .store_digit+2
                    ldx #0x00
-                   ldy #0x00
.load_digit:        lda 0x0000,x
.store_digit:       sta 0x0000,y
                    inx
                    iny
                    cpy #0x09
                    bne .load_digit
                    clc
                    lda .store_digit+1
                    adc #0x28
                    sta .store_digit+1
                    lda .store_digit+2
                    adc #0x00
                    sta .store_digit+2
                    cpx #0x7E
                    bne -
                    rts
print_number:       ldy #3
-                   tya
                    pha
                    lda number_to_print,y
                    tax
                    jsr print_digit
                    pla
                    tay
                    dey
                    bpl -
                    lda #0x46
                    sta vidmem0 + ( ( FIRSTDOT_Y + 0 ) *40 ) + ( FIRSTDOT_X + 0 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 2 ) *40 ) + ( FIRSTDOT_X + 0 )
                    lda #0x47
                    sta vidmem0 + ( ( FIRSTDOT_Y + 0 ) *40 ) + ( FIRSTDOT_X + 1 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 2 ) *40 ) + ( FIRSTDOT_X + 1 )
                    lda #0x48
                    sta vidmem0 + ( ( FIRSTDOT_Y + 1 ) *40 ) + ( FIRSTDOT_X + 0 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 3 ) *40 ) + ( FIRSTDOT_X + 0 )
                    lda #0x49
                    sta vidmem0 + ( ( FIRSTDOT_Y + 1 ) *40 ) + ( FIRSTDOT_X + 1 )
                    sta vidmem0 + ( ( FIRSTDOT_Y + 3 ) *40 ) + ( FIRSTDOT_X + 1 )
                    lda #DISABLE
                    sta ml_enable0
                    rts
number_to_print:    !byte 0, 7, 1, 8

print_last_plus:    ldy #3
                    inc number_to_print+3
                    lda number_to_print+3
                    cmp #10
                    bne .print
                    inc number_to_print+2
                    lda number_to_print+2
                    dey
.print:             tax
                    tya
                    pha
                    jsr print_digit
                    pla
                    tay
                    cpy #2
                    bne +
                    iny
                    lda #0
                    sta number_to_print+3
                    jmp .print
+                   lda #DISABLE
                    sta ml_enable1
                    dec count_counter
                    lda count_counter
                    bne +
                    lda #DISABLE
                    sta disable_enable0+1
                    lda #ENABLE
                    sta disable_enable1+1
+                   rts
count_counter:      !byte 0x03
; ==============================================================================
                    !zone SPRITES
                    X_OFFSET        = 24
                    Y_OFFSET        = 66
sprites_init:       lda #0x0c                 ; sprite multicolor 1
                    sta 0xD025
                    lda #0x0b                 ; sprite multicolor 2
                    sta 0xD026
                    ;lda #%11111111
d015_val:           lda #0
                    sta 0xd015                ; enable all sprites
                    lda #%00001100            ; set multicolor mode for Sprites
                    sta 0xd01c
                    lda #%11111111
                    sta 0xD017
                    sta 0xd01d
                    lda #0
                    sta 0xD010
                    ; sprite pointers for the klappwecker case
                    lda #sprite_base + 2
                    sta vidmem0 + 0x3fc    ; wecker farbe 2 links
                    lda #sprite_base + 3
                    sta vidmem0 + 0x3fd    ; wecker farbe 2 rechts
                    lda #sprite_base + 0
                    sta vidmem0 + 0x3fe    ; wecker farbe 1 links
                    lda #sprite_base + 1
                    sta vidmem0 + 0x3ff    ; wecker farbe 2 rechts
                    ; position sprites for klappwecker case
                    ; sprite 0
                    lda #0xa0 - X_OFFSET
                    sta 0xd00c   ; x
                    lda #0x90 + Y_OFFSET
                    sta 0xd00d   ; y
                    lda #0x0a
                    sta 0xd02d   ; color
                    ; sprite 1
                    lda #0xb8
                    sta 0xd00e   ; x
                    lda #0x90 + Y_OFFSET
                    sta 0xd00f   ; y
                    lda #0x0a
                    sta 0xd02e   ; color
                    ; sprite 2
                    lda #0xa0 - X_OFFSET
                    sta 0xd008   ; x
                    lda #0x90 + Y_OFFSET
                    sta 0xd009   ; y
                    lda #0x02
                    sta 0xd02b   ; color
                    ; sprite 3
                    lda #0xb8
                    sta 0xd00a   ; x
                    lda #0x90 + Y_OFFSET
                    sta 0xd00b   ; y
                    lda #0x02
                    sta 0xd02c   ; color
                    ; position sprites for klappwecker klappen
                    ; sprite 4
                    lda #0xa0 - X_OFFSET
                    sta 0xd004   ; x
                    lda #0x90 + Y_OFFSET
                    sta 0xd005   ; y
                    lda #0x0f
                    sta 0xd029   ; color
                    ; sprite 5
                    lda #0xb8
                    sta 0xd006   ; x
                    lda #0x90 + Y_OFFSET
                    sta 0xd007   ; y
                    lda #0x0f
                    sta 0xd02a   ; color
                    ; position sprites for klappwecker schrift
                    ; sprite 6
                    lda #0xa0 - X_OFFSET
                    sta 0xd000   ; x
                    lda #0x90 + Y_OFFSET
                    sta 0xd001   ; y
                    lda #0x01
                    sta 0xd027   ; color
                    ; sprite 7
                    lda #0xb8
                    sta 0xd002   ; x
                    lda #0x90 + Y_OFFSET
                    sta 0xd003   ; y
                    lda #0x01
                    sta 0xd028   ; color
                    lda #sprite_base - 1
                    sta vidmem0 + 0x3f8
                    lda #sprite_base - 1
                    sta vidmem0 + 0x3f9
                    lda #sprite_base + 4
                    sta vidmem0 + 0x3fa
                    lda #sprite_base + 5
                    sta vidmem0 + 0x3fb
                    rts
                    SPRITES_ANIM_SPEED = 5
                    SPRITES_ANIM_INITIAL_DELAY = 40
sprites_anim:       lda #SPRITES_ANIM_INITIAL_DELAY
                    beq +
                    dec sprites_anim+1
                    rts
+                   lda #SPRITES_ANIM_SPEED
                    sta sprites_anim+1
                    ldx .anim_pt
                    lda current_day0,x
                    sta vidmem0 + 0x3F8
                    lda current_day1,x
                    sta vidmem0 + 0x3F9
                    lda klapp_anim0,x
                    sta vidmem0 + 0x3FA
                    lda klapp_anim1,x
                    sta vidmem0 + 0x3FB
                    dex
                    bne +
                    lda #DISABLE
                    sta enable_sprites_anim
                    lda #ENABLE
                    sta enable_fade_in
                    ldx #0x05
+                   stx .anim_pt
                    rts
.anim_pt:           !byte 0x05
; ==============================================================================
current_day0:       !byte sprite_base + 18, sprite_base + 16, sprite_base + 14, sprite_base + 12, sprite_base - 1, sprite_base - 1
current_day1:       !byte sprite_base + 19, sprite_base + 17, sprite_base + 15, sprite_base + 13, sprite_base - 1, sprite_base - 1

klapp_anim0:        !byte sprite_base + 6, sprite_base + 4, sprite_base + 10, sprite_base + 8, sprite_base + 6, sprite_base + 4
klapp_anim1:        !byte sprite_base + 7, sprite_base + 5, sprite_base + 11, sprite_base + 9, sprite_base + 7, sprite_base + 5
; ==============================================================================
                    !zone FADE
                    FADE_OUT_SCREEN_SPEED = 0
                    FADE_OUT_SCREEN_INITIAL_DELAY = 16
fade_out_screen:    lda #FADE_OUT_SCREEN_INITIAL_DELAY
                    beq +
                    dec fade_out_screen+1
                    rts
+
fade_out_mod:       lda #FADE_OUT_SCREEN_SPEED
                    sta fade_out_screen+1
.fade_out_pt:       ldy #0
rand_tab_mod:       lda rand_tab40_0,y
                    tax
                    jsr .clear_line
                    iny
                    cpy #40
                    bne +
                    lda #DISABLE
                    sta enable_fade_out_screen
                    lda #0
                    sta color_bottom+1
                    sta 0xD015
                    lda #1
                    sta fade_out_ready_flag
                    ldy #0
+                   sty .fade_out_pt+1
                    rts
.clear_line:        lda #0x80
                    !for i, 1, 14 {
                        sta vidmem0+(i*40),x
                    }
                    rts
rand_tab40_0:       !byte 1, 23, 19, 18, 25, 36, 39, 15
                    !byte 21, 27, 13, 2, 26, 12, 37, 6
                    !byte 24, 20, 11, 30, 28, 31, 10, 0
                    !byte 29, 7, 38, 16, 4, 34, 8, 3
                    !byte 33, 5, 32, 35, 22, 14, 17, 9
rand_tab40_1:       !byte 23, 33, 29, 34, 31, 21, 14, 36
                    !byte 19, 39, 16, 0, 10, 17, 27, 4
                    !byte 37, 12, 24, 26, 1, 7, 13, 30
                    !byte 2, 35, 32, 3, 6, 5, 15, 28
                    !byte 11, 9, 22, 8, 20, 25, 38, 18
rand_tab40_2:       !byte 10, 35, 9, 17, 29, 24, 0, 18
                    !byte 11, 25, 2, 14, 21, 20, 26, 38
                    !byte 13, 7, 34, 33, 31, 4, 30, 27
                    !byte 16, 28, 8, 3, 6, 1, 36, 32
                    !byte 19, 23, 12, 5, 37, 22, 39, 15
                    FADE_IN_INITIAL_DELAY = 20
fade_in:            lda #FADE_IN_INITIAL_DELAY
                    beq +
                    dec fade_in+1
                    rts
+                   lda #DISABLE
                    sta enable_fade_in
                    lda #ENABLE
                    sta enable_anim_sync
                    sta ml_enable0
                    rts
; ==============================================================================
                    !zone LIB
library:            !if LIB_INCLUDE = 1 {
                         !source"../../lib/library.asm"
                    }
; ==============================================================================
                    !zone SYNC
sync0:              +sync 0x000688 + ( 2 * SAFETY_OFFSET )
                    rts
sync1:              +sync 0x0009FB + ( 4 * SAFETY_OFFSET )
                    rts
sync2:              +sync 0x000DA0 + ( 6 * SAFETY_OFFSET )
                    rts
sync3:              +sync 0x0010F4 + ( 8 * SAFETY_OFFSET )
                    rts
sync4:              +sync 0x00148F + ( 10 * SAFETY_OFFSET )
                    rts
sync5:              +sync 0x0017FC + ( 12 * SAFETY_OFFSET )
                    rts
sync6:              +sync 0x00461D + ( 20 * SAFETY_OFFSET )
                    rts
sync7:              +sync 0x005575 + ( 22 * SAFETY_OFFSET )
                    rts
; ==============================================================================
                    !zone DATA
digit0:
!byte $00+$00, $00+$00, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $00+$00, $00+$00 ; 0
!byte $40+$01, $40+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 1
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 2
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 3
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 4
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 5
!byte $40+$04, $40+$05, $00+$01, $00+$02, $00+$02, $00+$02, $00+$03, $40+$04, $40+$05 ; 6
!byte $40+$01, $40+$03, $00+$04, $00+$02, $00+$02, $00+$02, $00+$05, $40+$01, $40+$03 ; 7
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 8
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 9
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 10
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 11
!byte $40+$04, $40+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $40+$04, $40+$05 ; 12
!byte $00+$00, $00+$00, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $00+$00, $00+$00 ; 13

digit1:
!byte $00+$00, $00+$00, $00+$01, $00+$02, $00+$02, $00+$02, $00+$03, $00+$00, $00+$00 ; 0
!byte $00+$01, $00+$03, $00+$04, $00+$02, $00+$02, $00+$02, $00+$05, $40+$01, $40+$03 ; 1
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 2
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 3
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 4
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 5
!byte $00+$04, $00+$05, $00+$01, $00+$02, $00+$02, $00+$02, $00+$03, $40+$04, $40+$05 ; 6
!byte $00+$01, $00+$03, $00+$04, $00+$02, $00+$02, $00+$02, $00+$05, $40+$01, $40+$03 ; 7
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 8
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 9
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 10
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 11
!byte $00+$04, $00+$05, $00+$01, $00+$02, $00+$02, $00+$02, $00+$03, $40+$04, $40+$05 ; 12
!byte $00+$00, $00+$00, $00+$04, $00+$02, $00+$02, $00+$02, $00+$05, $00+$00, $00+$00 ; 13

digit2:
!byte $00+$00, $00+$00, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $00+$00, $00+$00 ; 0
!byte $00+$01, $00+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 1
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 2
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 3
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 4
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 5
!byte $00+$04, $00+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $40+$04, $40+$05 ; 6
!byte $40+$01, $40+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $00+$01, $00+$03 ; 7
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 8
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 9
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 10
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 11
!byte $40+$04, $40+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $00+$04, $00+$05 ; 12
!byte $00+$00, $00+$00, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $00+$00, $00+$00 ; 13

digit3:
!byte $00+$00, $00+$00, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $00+$00, $00+$00 ; 0
!byte $00+$01, $00+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 1
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 2
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 3
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 4
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 5
!byte $00+$04, $00+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $40+$04, $40+$05 ; 6
!byte $00+$01, $00+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 7
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 8
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 9
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 10
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 11
!byte $00+$04, $00+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $40+$04, $40+$05 ; 12
!byte $00+$00, $00+$00, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $00+$00, $00+$00 ; 13

digit4:
!byte $00+$00, $00+$00, $00+$01, $00+$02, $00+$02, $00+$02, $00+$03, $00+$00, $00+$00 ; 0
!byte $40+$01, $40+$03, $00+$04, $00+$02, $00+$02, $00+$02, $00+$05, $40+$01, $40+$03 ; 1
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 2
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 3
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 4
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 5
!byte $40+$04, $40+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $40+$04, $40+$05 ; 6
!byte $00+$01, $00+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 7
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 8
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 9
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 10
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 11
!byte $00+$04, $00+$05, $00+$01, $00+$02, $00+$02, $00+$02, $00+$03, $40+$04, $40+$05 ; 12
!byte $00+$00, $00+$00, $00+$04, $00+$02, $00+$02, $00+$02, $00+$05, $00+$00, $00+$00 ; 13

digit5:
!byte $00+$00, $00+$00, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $00+$00, $00+$00 ; 0
!byte $40+$01, $40+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $00+$01, $00+$03 ; 1
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 2
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 3
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 4
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 5
!byte $40+$04, $40+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $00+$04, $00+$05 ; 6
!byte $00+$01, $00+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 7
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 8
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 9
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 10
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 11
!byte $00+$04, $00+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $40+$04, $40+$05 ; 12
!byte $00+$00, $00+$00, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $00+$00, $00+$00 ; 13

digit6:
!byte $00+$00, $00+$00, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $00+$00, $00+$00 ; 0
!byte $40+$01, $40+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $00+$01, $00+$03 ; 1
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 2
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 3
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 4
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $00+$02, $00+$02 ; 5
!byte $40+$04, $40+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $00+$04, $00+$05 ; 6
!byte $40+$01, $40+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 7
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 8
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 9
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 10
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 11
!byte $40+$04, $40+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $40+$04, $40+$05 ; 12
!byte $00+$00, $00+$00, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $00+$00, $00+$00 ; 13

digit7:
!byte $00+$00, $00+$00, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $00+$00, $00+$00 ; 0
!byte $00+$01, $00+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 1
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 2
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 3
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 4
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 5
!byte $00+$04, $00+$05, $00+$01, $00+$02, $00+$02, $00+$02, $00+$03, $40+$04, $40+$05 ; 6
!byte $00+$01, $00+$03, $00+$04, $00+$02, $00+$02, $00+$02, $00+$05, $40+$01, $40+$03 ; 7
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 8
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 9
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 10
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 11
!byte $00+$04, $00+$05, $00+$01, $00+$02, $00+$02, $00+$02, $00+$03, $40+$04, $40+$05 ; 12
!byte $00+$00, $00+$00, $00+$04, $00+$02, $00+$02, $00+$02, $00+$05, $00+$00, $00+$00 ; 13

digit8:
!byte $00+$00, $00+$00, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $00+$00, $00+$00 ; 0
!byte $40+$01, $40+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 1
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 2
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 3
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 4
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 5
!byte $40+$04, $40+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $40+$04, $40+$05 ; 6
!byte $40+$01, $40+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 7
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 8
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 9
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 10
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 11
!byte $40+$04, $40+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $40+$04, $40+$05 ; 12
!byte $00+$00, $00+$00, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $00+$00, $00+$00 ; 13

digit9:
!byte $00+$00, $00+$00, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $00+$00, $00+$00 ; 0
!byte $40+$01, $40+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 1
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 2
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 3
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 4
!byte $40+$02, $40+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 5
!byte $40+$04, $40+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $40+$04, $40+$05 ; 6
!byte $00+$01, $00+$03, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $40+$01, $40+$03 ; 7
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 8
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 9
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 10
!byte $00+$02, $00+$02, $00+$00, $00+$00, $00+$00, $00+$00, $00+$00, $40+$02, $40+$02 ; 11
!byte $00+$04, $00+$05, $40+$01, $40+$02, $40+$02, $40+$02, $40+$03, $40+$04, $40+$05 ; 12
!byte $00+$00, $00+$00, $40+$04, $40+$02, $40+$02, $40+$02, $40+$05, $00+$00, $00+$00 ; 13

digit_tab_lo:       !byte <digit0
                    !byte <digit1
                    !byte <digit2
                    !byte <digit3
                    !byte <digit4
                    !byte <digit5
                    !byte <digit6
                    !byte <digit7
                    !byte <digit8
                    !byte <digit9

digit_tab_hi:       !byte >digit0
                    !byte >digit1
                    !byte >digit2
                    !byte >digit3
                    !byte >digit4
                    !byte >digit5
                    !byte >digit6
                    !byte >digit7
                    !byte >digit8
                    !byte >digit9

digit_col_tab_lo:   !byte <vidmem0+(1*40)+00
                    !byte <vidmem0+(1*40)+10
                    !byte <vidmem0+(1*40)+21
                    !byte <vidmem0+(1*40)+31

digit_col_tab_hi:   !byte >vidmem0+(1*40)+00
                    !byte >vidmem0+(1*40)+10
                    !byte >vidmem0+(1*40)+21
                    !byte >vidmem0+(1*40)+31
; ==============================================================================
data_day0:          !byte sprite_base + 18, sprite_base + 16, sprite_base + 14
                    !byte sprite_base + 12, sprite_base - 1, sprite_base - 1
                    !byte sprite_base + 19, sprite_base + 17, sprite_base + 15
                    !byte sprite_base + 13, sprite_base - 1, sprite_base - 1
                    !byte 0, 7, 1, 8

data_day1:          !byte sprite_base + 28, sprite_base + 26, sprite_base + 24
                    !byte sprite_base + 22, sprite_base - 1, sprite_base - 1
                    !byte sprite_base + 29, sprite_base + 27, sprite_base + 25
                    !byte sprite_base + 23, sprite_base - 1, sprite_base - 1
                    !byte 0, 4, 2, 4

data_day2:          !byte sprite_base + 38, sprite_base + 36, sprite_base + 34
                    !byte sprite_base + 32, sprite_base - 1, sprite_base - 1
                    !byte sprite_base + 39, sprite_base + 37, sprite_base + 35
                    !byte sprite_base + 33, sprite_base - 1, sprite_base - 1
                    !byte 0, 6, 0, 2

data_day3:          !byte sprite_base + 48, sprite_base + 46, sprite_base + 44
                    !byte sprite_base + 42, sprite_base - 1, sprite_base - 1
                    !byte sprite_base + 49, sprite_base + 47, sprite_base + 45
                    !byte sprite_base + 43, sprite_base - 1, sprite_base - 1
                    !byte 1, 7, 4, 6

data_day4:          !byte sprite_base + 58, sprite_base + 56, sprite_base + 54
                    !byte sprite_base + 52, sprite_base - 1, sprite_base - 1
                    !byte sprite_base + 59, sprite_base + 57, sprite_base + 55
                    !byte sprite_base + 53, sprite_base - 1, sprite_base - 1
                    !byte 2, 3, 1, 5

data_day5:          !byte sprite_base + 68, sprite_base + 66, sprite_base + 64
                    !byte sprite_base + 62, sprite_base - 1, sprite_base - 1
                    !byte sprite_base + 69, sprite_base + 67, sprite_base + 65
                    !byte sprite_base + 63, sprite_base - 1, sprite_base - 1
                    !byte 1, 3, 3, 4

data_day6:          !byte sprite_base + 78, sprite_base + 76, sprite_base + 74
                    !byte sprite_base + 72, sprite_base - 1, sprite_base - 1
                    !byte sprite_base + 79, sprite_base + 77, sprite_base + 75
                    !byte sprite_base + 73, sprite_base - 1, sprite_base - 1
                    !byte 1, 2, 0, 6

day_tab_lo:         !byte <data_day0        ; montag
                    !byte <data_day1        ; dienstag
                    !byte <data_day2        ; mittwoch
                    !byte <data_day3        ; donnerstag
                    !byte <data_day4        ; freitag
                    !byte <data_day5        ; samstag
                    !byte <data_day6
                    !byte <data_day0

day_tab_hi:         !byte >data_day0
                    !byte >data_day1
                    !byte >data_day2
                    !byte >data_day3
                    !byte >data_day4
                    !byte >data_day5
                    !byte >data_day6
                    !byte >data_day0

rand_tab_lo:        !byte <rand_tab40_0
                    !byte <rand_tab40_1
                    !byte <rand_tab40_0
                    !byte <rand_tab40_2
                    !byte <rand_tab40_0
                    !byte <rand_tab40_2
                    !byte <rand_tab40_1
                    !byte <rand_tab40_0

rand_tab_hi:        !byte >rand_tab40_0
                    !byte >rand_tab40_1
                    !byte >rand_tab40_0
                    !byte >rand_tab40_2
                    !byte >rand_tab40_0
                    !byte >rand_tab40_2
                    !byte >rand_tab40_1
                    !byte >rand_tab40_0

sync_tab_lo:        !byte <sync0
                    !byte <sync1
                    !byte <sync2
                    !byte <sync3
                    !byte <sync4
                    !byte <sync5
                    !byte <sync6
                    !byte <sync7

sync_tab_hi:        !byte >sync0
                    !byte >sync1
                    !byte >sync2
                    !byte >sync3
                    !byte >sync4
                    !byte >sync5
                    !byte >sync6
                    !byte >sync7
data_end:
; ==============================================================================
