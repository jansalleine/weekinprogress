SHELL=/bin/bash

RELEASE?=$(shell ([[ -f .RELEASE ]] && echo $$(<.RELEASE)) || echo -n 1)
ONCE:=$(shell [[ -f .RELEASE ]] || echo -n $(RELEASE) > .RELEASE)
ALWAYS:=$(shell [[ "$(RELEASE)" == 1 ]] && [[ "$$(<.RELEASE)" == 0 ]] && echo -n 1 > .RELEASE)
ALWAYS:=$(shell [[ "$(RELEASE)" == 0 ]] && [[ "$$(<.RELEASE)" == 1 ]] && echo -n 0 > .RELEASE)

CBMDISK=c1541

ASS=acme
ASSFLAGS=-v4 -f cbm -DRELEASE=$(RELEASE)
PACKER=exomizer
RM=rm -f
CP=cp -v

EMU?=x64sc
EMUFLAGS?=-VICIIborders 0

FRAMEWORK=framework.asm \
	install-c64.prg \
	loader-c64.prg \
	loadersymbols-c64.inc \
	music/0c00.sid
