DEBUG = 0
LIB_INCLUDE = 0
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 0
LIB_HEX2SCREEN = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 0
;RELEASE = 0
col= $d800
scr= $4000
vic= $d000

                    !cpu 6510
                    !source "../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !source "../../lib/sync.asm"
                    *= $1ffe
                    !byte <code_start
                    !byte >code_start
; ==============================================================================
code_start:
  !if RELEASE=1 { +sync 0x0026AC + ( 16 * SAFETY_OFFSET ) }
  lda #%00000010
  sta $dd00
  lda #$d8
  sta $d016


  ldx #$00
  stx $d011
  lda #$fb
- sta col-1,x
  sta col-1+$100,x
  sta col-1+$200,x
  sta col-1+$2e9,x
  inx
  bne -

  stx $d020
  stx $d021

  lda #$fe
  sta $d022
  lda #$f4
  sta $d023



  lda #$1b
  sta $d011
  lda #$1e
  sta $d018

intro
  jsr fw_wait_irq
  ;~ bit $d011
  ;~ bpl *-3
  ;~ bit $d011
  ;~ bmi *-3




buildup
  ldx #10;12;10
  beq quitbuildup
  dec buildupdelay+1
buildupdelay;
  ldy #50
  bne builddone
targetline
  ldx #$00
  lda filllines,x
  sta uppercontent+1
  inx
  lda filllines,x
  sta lowercontent+1
  inx
  stx targetline+1
  ldy #40
uppercontent
  lda #$00
uppertarget
  sta $4400-1,y
lowercontent
  lda #$00
lowertarget
  sta $4400+960-1,y
  dey
  bne uppercontent
  lda uppertarget+1
  clc
  adc #40
  sta uppertarget+1
  bcc +
  inc uppertarget+2
+ lda lowertarget+1
  sec
  sbc #40
  sta lowertarget+1
  bcs +
  dec lowertarget+2
+

  lda #13;TWEAK THIS, currently little more than 1/4 sec 12 times lines till cheap o raster screen is done
      ; higher value -> higher delay
  sta buildupdelay+1
  dec buildup+1
  jmp builddone



quitbuildup
; ldx #$00
  stx $d020
  txa
restorecol
  lda #$00;mcrasterfont+40-8
- sta $7838,x;20,x;38,x
  inx
  cpx #08
  bne -

  lda #$e8
  cmp $d012
  bne *-3
  jmp helixpart

builddone
  dec delay+1

delay
  lda #$00
  and #%00000011
  bne skip
  inc bordercol+1
;jmp skip
  lda mcrasterfont+40-8
  sta erasedcol+1
  ldx #$00
- lda mcrasterfont+48-8,x
  sta mcrasterfont+40-8,x
  lda mcrasterfont+56-8,x
  sta mcrasterfont+48-8,x
  lda mcrasterfont+64-8,x
  sta mcrasterfont+56-8,x

erasedcol
  lda #$00
  sta mcrasterfont+64-8,x
  inx
  cpx #08
  bne -
skip
; lda #$ff
; cmp $d012
; bne *-3

bordercol
  lda #$00
  and #%00000011
  tax
  lda bordercoltab,x
; sta $d020
;shift some chars of the cheap-o-raster-font
  lda mcrasterfont_pt2
  pha
  ldx #00
- lda mcrasterfont_pt2+1,x
  sta mcrasterfont_pt2,x
  inx
  cpx #15
  bne -
  pla
  sta mcrasterfont_pt2+15

  lda mcrasterfont+15
  pha
; ldx #15
- lda mcrasterfont-1,x
  sta mcrasterfont,x
  dex
  bne -
  pla
  sta mcrasterfont



  jmp intro





helixpart
; lda #$0e
; sta $d018
; sta $d020
; sta $d021

  ldx #200
- ;lda #$fe
  ;sta col-1,x
  ;sta col-1+800,x
  lda #$fc
  sta col-1,x
  sta col-1+800,x
  lda #$fb
  sta col-1+400,x
  sta col-1+200,x
  sta col-1+600,x


  dex
  bne -



start
  lda #$ff
  cmp $d012
  bne *-3

;shift some chars of the cheap-o-raster-font
  lda mcrasterfont
  pha
  ldx #00
- lda mcrasterfont+1,x
  sta mcrasterfont,x
  inx
  cpx #15
  bne -
  pla
  sta mcrasterfont+15

  lda mcrasterfont_pt2+15
  pha
; ldx #15
- lda mcrasterfont_pt2-1,x
  sta mcrasterfont_pt2,x
  dex
  bne -
  pla
  sta mcrasterfont_pt2


split0
  lda #$fe
  sta $d022
  lda #$f6
  sta $d023

scrollval1
  lda #$d0
  sta $d016

modifyscroll5
  lda scrollval5+1
  clc;sec
speed5
  adc #01;sbc #$08
  pha
  ora #$d0
  and #%11110111
  sta scrollval5+1
  pla
  cmp #$d8;#$d0
cmp5
  bmi +;bpl +
;inc $d021
target5
  jsr scroll5a
;dec $d021
+


  lda #$5a
  cmp $d012
  bne *-3
  jsr waste48cycles
switch1
  lda #$1e
  sta $d018
split1
scrollval2
  lda #$d0
  sta $d016

  lda #$fe
  sta $d022
  lda #$f6
  sta $d023

; lda #$02
; sta $d021


modifyscroll1
  lda scrollval1+1
  sec
speed1
  sbc #$01
  pha
  ora #$d0
  and #%11110111
  sta scrollval1+1
  pla
  cmp #$d0
cmp1
  bpl +
;inc $d021
target1
  jsr scroll1
;dec $d021
+




  lda #$5a+40
  cmp $d012
  bne *-3
  jsr waste48cycles
split2
scrollval3
  lda #$d0
  sta $d016

  lda #$0e
  sta $d018



  lda #$fe
  sta $d022
  lda #$f4
  sta $d023

; lda #$00
; sta $d021

modifyscroll2
  lda scrollval2+1
  clc;sec
speed2
  adc #01;sbc #$08
  pha
  ora #$d0
  and #%11110111
  sta scrollval2+1
  pla
  cmp #$d8;#$d0
cmp2
  bmi +;bpl +
;inc $d021
target2
  jsr scroll2a
;dec $d021
+

  lda #$5a+80
  cmp $d012
  bne *-3
  jsr waste24cycles
  jsr waste12cycles
  nop
  nop
  nop
switch1b
  lda #$1e
  sta $d018
split3
scrollval4
  lda #$d0
  sta $d016




  lda #$fe
  sta $d022
  lda #$f6
  sta $d023
; lda #$02
; sta $d021

modifyscroll3
  lda scrollval3+1
  clc;sec
speed3
  adc #01;sbc #$08
  pha
  ora #$d0
  and #%11110111
  sta scrollval3+1
  pla
  cmp #$d8;#$d0
cmp3
  bmi +;bpl +
;inc $d021
target3
  jsr scroll3a
;dec $d021
+
  lda #$5a+40+80
  cmp $d012
  bne *-3
  jsr waste24cycles
  jsr waste12cycles
  nop
  nop
  nop
switch2
  lda #$1e
  sta $d018
; jsr waste48cycles
split4

scrollval5
  lda #$d0
  sta $d016
; lda #$00
; sta $d021


modifyscroll4
  lda scrollval4+1
  sec
speed4
  sbc #$01
  pha
  ora #$d0
  and #%11110111
  sta scrollval4+1
  pla
  cmp #$d0
cmp4
  bpl +
;inc $d021
target4
  jsr scroll4
;dec $d021
+

  lda scr+480
  cmp #$07
  bne +
  jsr pullswitch_phase2

+
  lda scr+280
  cmp #$07
  bne +
  jsr pullswitch_phase3

+ lda scr+80
  cmp #$07
  beq loadnextpart

  jmp start

loadnextpart
  lda #$0b
  sta $d011
  !if RELEASE = 0 {
      inc $d020 ; Ryks special :)
      jmp *-3
  } else {
    jmp code_exit
  }

scroll1
over1a
  lda scr-400+560
  pha
over1b
  lda scr-400+520
  pha
over1c
  lda scr-400+480
  pha
over1d
  lda scr-400+440
  pha
over1e
  lda scr-400+400
  pha
  ldx #00
- lda scr-400+401,x; necessary to unroll due to cpx #39 eats too many cycles
  sta scr-400+400,x
  lda scr-400+441,x
  sta scr-400+440,x
  lda scr-400+481,x
  sta scr-400+480,x
  lda scr-400+521,x
  sta scr-400+520,x
  lda scr-400+561,x
  sta scr-400+560,x
  inx
  lda scr-400+401,x
  sta scr-400+400,x
  lda scr-400+441,x
  sta scr-400+440,x
  lda scr-400+481,x
  sta scr-400+480,x
  lda scr-400+521,x
  sta scr-400+520,x
  lda scr-400+561,x
  sta scr-400+560,x
  inx
  lda scr-400+401,x
  sta scr-400+400,x
  lda scr-400+441,x
  sta scr-400+440,x
  lda scr-400+481,x
  sta scr-400+480,x
  lda scr-400+521,x
  sta scr-400+520,x
  lda scr-400+561,x
  sta scr-400+560,x
  inx
  cpx #39
  bne -
  pla
  sta scr-400+439
  pla
  sta scr-400+479
  pla
  sta scr-400+519
  pla
  sta scr-400+559
  pla
  sta scr-400+599
  dec scrolledcount1+1
scrolledcount1
  ldx #4
  bne +++
scrollmodcounter1
  asl counterdef1+1
; asl counterdef1+1
counterdef1
  lda #4
  sta scrolledcount1+1
scrollmodspeed1
+ asl speed1+1
  bne +
  inc speed1+1
  lda cmp1-1
  eor #%00001000
  sta cmp1-1
  lda cmp1
  eor #%00100000
  sta cmp1
  lda speed1
  eor #%10000000
  sta speed1
  lda speed1-1
  eor #$20
  sta speed1-1
  lda #<scroll1a
  sta target1+1
  lda #>scroll1a
  sta target1+2
  lda #$4
  sta scrolledcount1a+1
  sta counterdef1a+1
  bne ++
+ lda speed1+1
  cmp #$08
  bne +++
++  lda scrollmodspeed1
  eor #$40
  sta scrollmodspeed1
  lda scrollmodspeed1
  eor #$40
  lda scrollmodcounter1
  eor #$40
  sta scrollmodcounter1
; sta scrollmodcounter1+3
+++ rts

scroll1a
  lda scr-400+599
  pha
  lda scr-400+559
  pha
  lda scr-400+519
  pha
  lda scr-400+479
  pha
  lda scr-400+439
  pha
  ldx #39
- lda scr-400+400-1,x
  sta scr-400+400,x
  lda scr-400+440-1,x
  sta scr-400+440,x
  lda scr-400+480-1,x
  sta scr-400+480,x
  lda scr-400+520-1,x
  sta scr-400+520,x
  lda scr-400+560-1,x
  sta scr-400+560,x
  dex
  bne -
  pla
  sta scr-400+400
  pla
  sta scr-400+440
  pla
  sta scr-400+480
  pla
  sta scr-400+520
  pla
  sta scr-400+560
  dec scrolledcount1a+1
scrolledcount1a
  ldx #4
  bne +++
scrollmodcounter1a
  asl counterdef1a+1
; asl counterdef1a+1
counterdef1a
  lda #4
  sta scrolledcount1a+1
scrollmodspeed1a
+ asl speed1+1
  bne +
  inc speed1+1
  lda cmp1-1
  eor #%00001000
  sta cmp1-1
  lda cmp1
  eor #%00100000
  sta cmp1
  lda speed1
  eor #%10000000
  sta speed1
  lda speed1-1
  eor #$20
  sta speed1-1
  lda #<scroll1
  sta target1+1
  lda #>scroll1
  sta target1+2
  lda #$04
  sta scrolledcount1+1
  sta counterdef1+1
  jmp ++
+ lda speed1+1
  cmp #$08
  bne +++
++  lda scrollmodspeed1a
  eor #$40
  sta scrollmodspeed1a
  lda scrollmodspeed1a
  eor #$40
  lda scrollmodcounter1a
  eor #$40
  sta scrollmodcounter1a
; sta scrollmodcounter1a+3
+++ rts

scroll2
over2a
  lda scr-200+560
  pha
over2b
  lda scr-200+520
  pha
over2c
  lda scr-200+480
  pha
over2d
  lda scr-200+440
  pha
over2e
  lda scr-200+400
  pha
  ldx #00
- lda scr-200+401,x; necessary to unroll due to cpx #39 eats too many cycles
  sta scr-200+400,x
  lda scr-200+441,x
  sta scr-200+440,x
  lda scr-200+481,x
  sta scr-200+480,x
  lda scr-200+521,x
  sta scr-200+520,x
  lda scr-200+561,x
  sta scr-200+560,x
  inx
  lda scr-200+401,x
  sta scr-200+400,x
  lda scr-200+441,x
  sta scr-200+440,x
  lda scr-200+481,x
  sta scr-200+480,x
  lda scr-200+521,x
  sta scr-200+520,x
  lda scr-200+561,x
  sta scr-200+560,x
  inx
  lda scr-200+401,x
  sta scr-200+400,x
  lda scr-200+441,x
  sta scr-200+440,x
  lda scr-200+481,x
  sta scr-200+480,x
  lda scr-200+521,x
  sta scr-200+520,x
  lda scr-200+561,x
  sta scr-200+560,x
  inx
  cpx #39
  bne -
  pla
  sta scr-200+439
  pla
  sta scr-200+479
  pla
  sta scr-200+519
  pla
  sta scr-200+559
  pla
  sta scr-200+599
  dec scrolledcount2+1
scrolledcount2
  ldx #1;2
  bne +++
scrollmodcounter2
  asl counterdef2+1
  asl counterdef2+1
counterdef2
  lda #1;2
  sta scrolledcount2+1
scrollmodspeed2
+ asl speed2+1
  bne +
  inc speed2+1
  lda cmp2-1
  eor #%00001000
  sta cmp2-1
  lda cmp2
  eor #%00100000
  sta cmp2
  lda speed2
  eor #%10000000
  sta speed2
  lda speed2-1
  eor #$20
  sta speed2-1
  lda #<scroll2a
  sta target2+1
  lda #>scroll2a
  sta target2+2
  lda #$01;2
  sta scrolledcount2a+1
  sta counterdef2a+1
  bne ++
+ lda speed2+1
  cmp #$08
  bne +++
++  lda scrollmodspeed2
  eor #$40
  sta scrollmodspeed2
  lda scrollmodspeed2
  eor #$40
  lda scrollmodcounter2
  eor #$40
  sta scrollmodcounter2
  sta scrollmodcounter2+3
+++ rts

scroll2a
;over2a
  lda scr-200+599
  pha
;over2b
  lda scr-200+559
  pha
;over2c
  lda scr-200+519
  pha
;over2d
  lda scr-200+479
  pha
;over2e
  lda scr-200+439
  pha
  ldx #39
- lda scr-200+400-1,x
  sta scr-200+400,x
  lda scr-200+440-1,x
  sta scr-200+440,x
  lda scr-200+480-1,x
  sta scr-200+480,x
  lda scr-200+520-1,x
  sta scr-200+520,x
  lda scr-200+560-1,x
  sta scr-200+560,x
  dex
  bne -
  pla
  sta scr-200+400
  pla
  sta scr-200+440
  pla
  sta scr-200+480
  pla
  sta scr-200+520
  pla
  sta scr-200+560
  dec scrolledcount2a+1
scrolledcount2a
  ldx #1;2
  bne +++
scrollmodcounter2a
  asl counterdef2a+1
  asl counterdef2a+1
counterdef2a
  lda #1;2
  sta scrolledcount2a+1
scrollmodspeed2a
+ asl speed2+1
  bne +
  inc speed2+1
  lda cmp2-1
  eor #%00001000
  sta cmp2-1
  lda cmp2
  eor #%00100000
  sta cmp2
  lda speed2
  eor #%10000000
  sta speed2
  lda speed2-1
  eor #$20
  sta speed2-1
  lda #<scroll2
  sta target2+1
  lda #>scroll2
  sta target2+2
  lda #$01;2
  sta scrolledcount2+1
  sta counterdef2+1
  jmp ++
+ lda speed2+1
  cmp #$08
  bne +++
++  lda scrollmodspeed2a
  eor #$40
  sta scrollmodspeed2a
  lda scrollmodspeed2a
  eor #$40
  lda scrollmodcounter2a
  eor #$40
  sta scrollmodcounter2a
  sta scrollmodcounter2a+3
+++ rts


scroll3
over3a
  lda scr+560
  pha
over3b
  lda scr+520
  pha
over3c
  lda scr+480
  pha
over3d
  lda scr+440
  pha
over3e
  lda scr+400
  pha
  ldx #00
- lda scr+401,x; necessary to unroll due to cpx #39 eats too many cycles
  sta scr+400,x
  lda scr+441,x
  sta scr+440,x
  lda scr+481,x
  sta scr+480,x
  lda scr+521,x
  sta scr+520,x
  lda scr+561,x
  sta scr+560,x
  inx
  lda scr+401,x
  sta scr+400,x
  lda scr+441,x
  sta scr+440,x
  lda scr+481,x
  sta scr+480,x
  lda scr+521,x
  sta scr+520,x
  lda scr+561,x
  sta scr+560,x
  inx
  lda scr+401,x
  sta scr+400,x
  lda scr+441,x
  sta scr+440,x
  lda scr+481,x
  sta scr+480,x
  lda scr+521,x
  sta scr+520,x
  lda scr+561,x
  sta scr+560,x
  inx
  cpx #39
  bne -
  pla
  sta scr+439
  pla
  sta scr+479
  pla
  sta scr+519
  pla
  sta scr+559
  pla
  sta scr+599
  dec scrolledcount3+1
scrolledcount3
  ldx #2
  bne +++
scrollmodcounter3
  asl counterdef3+1
  asl counterdef3+1
counterdef3
  lda #2
  sta scrolledcount3+1
scrollmodspeed3
+ asl speed3+1
  bne +
  inc speed3+1
  lda cmp3-1
  eor #%00001000
  sta cmp3-1
  lda cmp3
  eor #%00100000
  sta cmp3
  lda speed3
  eor #%10000000
  sta speed3
  lda speed3-1
  eor #$20
  sta speed3-1
  lda #<scroll3a
  sta target3+1
  lda #>scroll3a
  sta target3+2

  jsr pullswitch

  lda #$02
  sta scrolledcount3a+1
  sta counterdef3a+1
  bne ++
+ lda speed3+1
  cmp #$08
  bne +++
++  lda scrollmodspeed3
  eor #$40
  sta scrollmodspeed3
  lda scrollmodspeed3
  eor #$40
  lda scrollmodcounter3
  eor #$40
  sta scrollmodcounter3
  sta scrollmodcounter3+3
+++ rts

scroll3a
  lda scr+599
  pha
  lda scr+559
  pha
  lda scr+519
  pha
  lda scr+479
  pha
  lda scr+439
  pha
  ldx #39
- lda scr+400-1,x
  sta scr+400,x
  lda scr+440-1,x
  sta scr+440,x
  lda scr+480-1,x
  sta scr+480,x
  lda scr+520-1,x
  sta scr+520,x
  lda scr+560-1,x
  sta scr+560,x
  dex
  bne -
  pla
  sta scr+400
  pla
  sta scr+440
  pla
  sta scr+480
  pla
  sta scr+520
  pla
  sta scr+560
  dec scrolledcount3a+1
scrolledcount3a
  ldx #2
  bne +++
scrollmodcounter3a
  asl counterdef3a+1
  asl counterdef3a+1
counterdef3a
  lda #2
  sta scrolledcount3a+1
scrollmodspeed3a
+ asl speed3+1
  bne +
  inc speed3+1
  lda cmp3-1
  eor #%00001000
  sta cmp3-1
  lda cmp3
  eor #%00100000
  sta cmp3
  lda speed3
  eor #%10000000
  sta speed3
  lda speed3-1
  eor #$20
  sta speed3-1
  lda #<scroll3
  sta target3+1
  lda #>scroll3
  sta target3+2
  lda #$0e
  sta switch1+1
  sta switch1b+1

  lda #$02
  sta scrolledcount3+1
  sta counterdef3+1
  jmp ++
+ lda speed3+1
  cmp #$08
  bne +++
++  lda scrollmodspeed3a
  eor #$40
  sta scrollmodspeed3a
  lda scrollmodspeed3a
  eor #$40
  lda scrollmodcounter3a
  eor #$40
  sta scrollmodcounter3a
  sta scrollmodcounter3a+3
+++ rts

scroll4
;over4a
  lda scr+200+560
  pha
;over4b
  lda scr+200+520
  pha
;over4c
  lda scr+200+480
  pha
;over4d
  lda scr+200+440
  pha
;over4e
  lda scr+200+400
  pha
  ldx #00
- lda scr+200+401,x; necessary to unroll due to cpx #39 eats too many cycles
  sta scr+200+400,x
  lda scr+200+441,x
  sta scr+200+440,x
  lda scr+200+481,x
  sta scr+200+480,x
  lda scr+200+521,x
  sta scr+200+520,x
  lda scr+200+561,x
  sta scr+200+560,x
  inx
  lda scr+200+401,x
  sta scr+200+400,x
  lda scr+200+441,x
  sta scr+200+440,x
  lda scr+200+481,x
  sta scr+200+480,x
  lda scr+200+521,x
  sta scr+200+520,x
  lda scr+200+561,x
  sta scr+200+560,x
  inx
  lda scr+200+401,x
  sta scr+200+400,x
  lda scr+200+441,x
  sta scr+200+440,x
  lda scr+200+481,x
  sta scr+200+480,x
  lda scr+200+521,x
  sta scr+200+520,x
  lda scr+200+561,x
  sta scr+200+560,x
  inx
  cpx #39
  bne -
  pla
  sta scr+200+439
  pla
  sta scr+200+479
  pla
  sta scr+200+519
  pla
  sta scr+200+559
  pla
  sta scr+200+599
  dec scrolledcount4+1
scrolledcount4
  ldx #1
  bne +++
scrollmodcounter4
  asl counterdef4+1
  asl counterdef4+1
counterdef4
  lda #1
  sta scrolledcount4+1
scrollmodspeed4
+ asl speed4+1
  bne +
  inc speed4+1
  lda cmp4-1
  eor #%00001000
  sta cmp4-1
  lda cmp4
  eor #%00100000
  sta cmp4
  lda speed4
  eor #%10000000
  sta speed4
  lda speed4-1
  eor #$20
  sta speed4-1
  lda #<scroll4a
  sta target4+1
  lda #>scroll4a
  sta target4+2
  lda #$1
  sta scrolledcount4a+1
  sta counterdef4a+1
  bne ++
+ lda speed4+1
  cmp #$08
  bne +++
++  lda scrollmodspeed4
  eor #$40
  sta scrollmodspeed4
  lda scrollmodspeed4
  eor #$40
  lda scrollmodcounter4
  eor #$40
  sta scrollmodcounter4
  sta scrollmodcounter4+3
+++ rts

scroll4a
over4a
  lda scr+200+599
  pha
over4b
  lda scr+200+559
  pha
over4c
  lda scr+200+519
  pha
over4d
  lda scr+200+479
  pha
over4e
  lda scr+200+439
  pha
  ldx #39
- lda scr+200+400-1,x
  sta scr+200+400,x
  lda scr+200+440-1,x
  sta scr+200+440,x
  lda scr+200+480-1,x
  sta scr+200+480,x
  lda scr+200+520-1,x
  sta scr+200+520,x
  lda scr+200+560-1,x
  sta scr+200+560,x
  dex
  bne -
  pla
  sta scr+200+400
  pla
  sta scr+200+440
  pla
  sta scr+200+480
  pla
  sta scr+200+520
  pla
  sta scr+200+560
  dec scrolledcount4a+1
scrolledcount4a
  ldx #1
  bne +++
scrollmodcounter4a
  asl counterdef4a+1
  asl counterdef4a+1
counterdef4a
  lda #1
  sta scrolledcount4a+1
scrollmodspeed4a
+ asl speed4+1
  bne +
  inc speed4+1
  lda cmp4-1
  eor #%00001000
  sta cmp4-1
  lda cmp4
  eor #%00100000
  sta cmp4
  lda speed4
  eor #%10000000
  sta speed4
  lda speed4-1
  eor #$20
  sta speed4-1
  lda #<scroll4
  sta target4+1
  lda #>scroll4
  sta target4+2
  lda #$01
  sta scrolledcount4+1
  sta counterdef4+1
  jmp ++
+ lda speed4+1
  cmp #$08
  bne +++
++  lda scrollmodspeed4a
  eor #$40
  sta scrollmodspeed4a
  lda scrollmodspeed4a
  eor #$40
  lda scrollmodcounter4a
  eor #$40
  sta scrollmodcounter4a
  sta scrollmodcounter4a+3
+++ rts

scroll5
  lda scr+400+560
  pha
  lda scr+400+520
  pha
  lda scr+400+480
  pha
  lda scr+400+440
  pha
  lda scr+400+400
  pha
  ldx #00
- lda scr+400+401,x; necessary to unroll due to cpx #39 eats too many cycles
  sta scr+400+400,x
  lda scr+400+441,x
  sta scr+400+440,x
  lda scr+400+481,x
  sta scr+400+480,x
  lda scr+400+521,x
  sta scr+400+520,x
  lda scr+400+561,x
  sta scr+400+560,x
  inx
  lda scr+400+401,x
  sta scr+400+400,x
  lda scr+400+441,x
  sta scr+400+440,x
  lda scr+400+481,x
  sta scr+400+480,x
  lda scr+400+521,x
  sta scr+400+520,x
  lda scr+400+561,x
  sta scr+400+560,x
  inx
  lda scr+400+401,x
  sta scr+400+400,x
  lda scr+400+441,x
  sta scr+400+440,x
  lda scr+400+481,x
  sta scr+400+480,x
  lda scr+400+521,x
  sta scr+400+520,x
  lda scr+400+561,x
  sta scr+400+560,x
  inx
  cpx #39
  bne -
  pla
  sta scr+400+439
  pla
  sta scr+400+479
  pla
  sta scr+400+519
  pla
  sta scr+400+559
  pla
  sta scr+400+599
  dec scrolledcount5+1
scrolledcount5
  ldx #4
  bne +++
scrollmodcounter5
  asl counterdef5+1
; asl counterdef5+1
counterdef5
  lda #4
  sta scrolledcount5+1
scrollmodspeed5
+ asl speed5+1
  bne +
  inc speed5+1
  lda cmp5-1
  eor #%00001000
  sta cmp5-1
  lda cmp5
  eor #%00100000
  sta cmp5
  lda speed5
  eor #%10000000
  sta speed5
  lda speed5-1
  eor #$20
  sta speed5-1
  lda #<scroll5a
  sta target5+1
  lda #>scroll5a
  sta target5+2
  lda #$4
  sta scrolledcount5a+1
  sta counterdef5a+1
  bne ++
+ lda speed5+1
  cmp #$08
  bne +++
++  lda scrollmodspeed5
  eor #$40
  sta scrollmodspeed5
  lda scrollmodspeed5
  eor #$40
  lda scrollmodcounter5
  eor #$40
  sta scrollmodcounter5
; sta scrollmodcounter5+3
+++ rts

scroll5a
over5a
  lda scr+400+599
  pha
over5b
  lda scr+400+559
  pha
over5c
  lda scr+400+519
  pha
over5d
  lda scr+400+479
  pha
over5e
  lda scr+400+439
  pha
  ldx #39
- lda scr+400+400-1,x
  sta scr+400+400,x
  lda scr+400+440-1,x
  sta scr+400+440,x
  lda scr+400+480-1,x
  sta scr+400+480,x
  lda scr+400+520-1,x
  sta scr+400+520,x
  lda scr+400+560-1,x
  sta scr+400+560,x
  dex
  bne -
  pla
  sta scr+400+400
  pla
  sta scr+400+440
  pla
  sta scr+400+480
  pla
  sta scr+400+520
  pla
  sta scr+400+560
  dec scrolledcount5a+1
scrolledcount5a
  ldx #4
  bne +++
scrollmodcounter5a
  asl counterdef5a+1
; asl counterdef5a+1
counterdef5a
  lda #4
  sta scrolledcount5a+1
scrollmodspeed5a
+ asl speed5+1
  bne +
  inc speed5+1
  lda cmp5-1
  eor #%00001000
  sta cmp5-1
  lda cmp5
  eor #%00100000
  sta cmp5
  lda speed5
  eor #%10000000
  sta speed5
  lda speed5-1
  eor #$20
  sta speed5-1
  lda #<scroll5
  sta target5+1
  lda #>scroll5
  sta target5+2
  lda #$04
  sta scrolledcount5+1
  sta counterdef5+1
  jmp ++
+ lda speed5+1
  cmp #$08
  bne +++
++  lda scrollmodspeed5a
  eor #$40
  sta scrollmodspeed5a
  lda scrollmodspeed5a
  eor #$40
  lda scrollmodcounter5a
  eor #$40
  sta scrollmodcounter5a
; sta scrollmodcounter5a+3
+++ rts


waste48cycles
  jsr waste24cycles
waste24cycles
  jsr waste12cycles
waste12cycles
  rts

pullswitch
  lda #$0e
  sta switch2+1
  lda #$a9; opcode LDA #$00
  sta over3a
  sta over3b
  sta over3c
  sta over3d
  sta over3e
  lda #$07; value
  sta over3a+1
  sta over3b+1
  sta over3c+1
  sta over3d+1
  sta over3e+1
  lda #$ea; opcode NOP
  sta over3a+2
  sta over3b+2
  sta over3c+2
  sta over3d+2
  sta over3e+2
  rts

pullswitch_phase2
  lda #$0e
  sta switch2+1
  lda #$a9; opcode LDA #$00
  sta over2a
  sta over2b
  sta over2c
  sta over2d
  sta over2e
  sta over4a
  sta over4b
  sta over4c
  sta over4d
  sta over4e
  lda #$07; value
  sta over2a+1
  sta over2b+1
  sta over2c+1
  sta over2d+1
  sta over2e+1
  sta over4a+1
  sta over4b+1
  sta over4c+1
  sta over4d+1
  sta over4e+1
  lda #$ea; opcode NOP
  sta over2a+2
  sta over2b+2
  sta over2c+2
  sta over2d+2
  sta over2e+2
  sta over4a+2
  sta over4b+2
  sta over4c+2
  sta over4d+2
  sta over4e+2
  rts

pullswitch_phase3
  lda #$0e
  sta switch2+1
  lda #$a9; opcode LDA #$00
  sta over1a
  sta over1b
  sta over1c
  sta over1d
  sta over1e
  sta over5a
  sta over5b
  sta over5c
  sta over5d
  sta over5e
  lda #$07; value
  sta over1a+1
  sta over1b+1
  sta over1c+1
  sta over1d+1
  sta over1e+1
  sta over5a+1
  sta over5b+1
  sta over5c+1
  sta over5d+1
  sta over5e+1
  lda #$ea; opcode NOP
  sta over1a+2
  sta over1b+2
  sta over1c+2
  sta over1d+2
  sta over1e+2
  sta over5a+2
  sta over5b+2
  sta over5c+2
  sta over5d+2
  sta over5e+2
  rts


bordercoltab
  !byte $03,$0e,$04,$00

eightletterwords
  !ct scr
  !tx "codepron"
  !tx "proncode"
  !tx "weeak in"
  !tx "progress"

filllines
  !byte 00,(mcrasterfont_pt2-$7800)/8
  !byte 00,(mcrasterfont_pt2-$7800)/8
  !byte 00,(mcrasterfont_pt2-$7800)/8
  !byte 00,(mcrasterfont_pt2-$7800)/8
  !byte 01,(mcrasterfont_pt2-$7800)/8+1
  !byte 00,(mcrasterfont_pt2-$7800)/8
  !byte 01,(mcrasterfont_pt2-$7800)/8+1
  !byte 00,(mcrasterfont_pt2-$7800)/8
  !byte 01,(mcrasterfont_pt2-$7800)/8+1
  !byte 00,(mcrasterfont_pt2-$7800)/8
  !byte 01,(mcrasterfont_pt2-$7800)/8+1
  !byte 00,(mcrasterfont_pt2-$7800)/8
;
; !byte 7,7
; !byte 6,6
; !byte 5,5








;*= $7000; meant for sprite jokes that never became true
; !bin "maydaystarfieldalphabet.spd",,3

*= $7800
mcrasterfont
  !bin "rasterkram3-font.bin"
mcrasterfont_pt2
  !bin "rasterkram3-font.bin",16


*= $4000
; !fill 200,07
  !bin "rasterkram3-screendata.bin",200,40
; !fill 200,07
  !bin "rasterkram3-screendata.bin",200,40
; !fill 200,07
  !bin "rasterkram3-screendata.bin",200,40
; !byte $07
  !bin "rasterkram3-screendata.bin",200,40
; !byte $07
  !bin "rasterkram3-screendata.bin",200,40



; !byte 00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,12,12,12,12,13,13,13,13,14,14,14,14,15,15,15,15
; !fill 1000-16,01

*= $4400
  !fill 1000,4




*= $8000
code_exit:          sei
                    lda #<fw_irq
                    sta $fffe
                    lda #>fw_irq
                    sta $ffff
                    lda #$0b
                    sta $d011
                    lda #$00
                    sta $d012
                    dec $d019
                    cli
                    !if RELEASE = 0 {
                         jmp *
                    } else {
                         jmp fw_back2framework
                    }
; ==============================================================================
library:            !if LIB_INCLUDE = 1 {
                         !source"../../lib/library.asm"
                    }
