DEBUG = 0
LIB_INCLUDE = 0
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 0
LIB_HEX2SCREEN = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 0

                    !cpu 6510
                    !source "../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !source "../../lib/sync.asm"


textpointer   = $04
bitcounter    = $06
animationdelay  = $08
animationcounter= $0a
tmp0      = $10
tmp1      = tmp0+1
tmp2      = tmp0+2
tmp3      = tmp0+3
tmp4      = tmp0+4

scr       = $c0
col       = $c2
tmps      = $c4
tmpc      = $c6
counter     = $c8
counter2    = $c9
counter3    = $ca
timer       = $d0

fontaddress   = $3800
spriteaddress   = $4400
screen      = $4800


*= $1ffe
                !byte <code_start
                !byte >code_start
; ==============================================================================
; init
; ==============================================================================
code_start:
        lda #$00
        sta $d020
        sta $d021
        sta $d011
        !if RELEASE=1 { +sync 0x001CD0 + ( 14 * SAFETY_OFFSET ) }
        jsr fw_wait_irq

        lda #0
        tax
-       sta $d800,x
        sta $d900,x
        sta $da00,x
        sta $dae8,x
        inx
        bne -
        lda #$d8
        sta $d016
        lda #$2b
        sta $d018

        lda #<!(0x4000/0x4000) & 3
        sta $dd00

        lda #$f0
        sta $d01c
        lda #0
        sta $d017
        sta $d01d
        sta $d010
        sta $d015
        sta timer
        sta timer+1

        lda #8
        sta $d025
        lda #9
        sta $d026

        lda #$00
        ldx #0
llpp1:      sta spriteaddress,x     ;blank sprites 4stk
        inx
        bne llpp1



        ldx #$10
        stx screen+$3f8+0
        inx
        stx screen+$3f8+1
        inx
        stx screen+$3f8+2
        inx
        stx screen+$3f8+3

        lda #$40            ;smoke sprites
        sta screen+$3f8+4
        lda #$43
        sta screen+$3f8+5
        lda #$46
        sta screen+$3f8+6
        lda #$49
        sta screen+$3f8+7

        ldx #3
llpp2:      lda #1
        sta $d027,x
        lda #7
        sta $d02b,x
        dex
        bpl llpp2


        ldx #$0f
llpp3:      lda spritespos,x
        sta $d000,x
        dex
        bpl llpp3

        lda #6
        sta animationdelay
        lda #3
        sta animationcounter

        lda #<scrolltext
        sta textpointer
        lda #>scrolltext
        sta textpointer+1
        jsr getcharbits

; ==============================================================================
; fade in
; ==============================================================================
        lda #$3b
        sta $d011
        lda #$04
        sta counter3
        ldx #$ff
        stx counter
        inx
        stx counter2
        jsr setup
        jsr fade

        lda #$ff
        sta $d015
        jsr fw_wait_irq
; ==============================================================================
; scroll text
; ==============================================================================

mainloop:   jsr fw_wait_irq
        jsr shiftbits
        jsr shiftbits
        jsr smoke

        inc timer
        lda timer
        bne skip1
        inc timer+1

skip1:      lda timer
        cmp #$98
        bne mainloop
        lda timer+1
        cmp #$04
        bne mainloop

        lda #0
        sta $d015
; ==============================================================================
; fade out
; ==============================================================================

        ldx #$ff
        stx counter
        inx
        stx counter2
        jsr setup
        jsr fade2

; ==============================================================================
; exit
; ==============================================================================

code_exit:      lda #$0b
        sta $d011
        lda #0
        sta 0xD015
        jsr fw_wait_irq


                !if RELEASE = 0 {
                      jmp *
                 } else {
                      jmp fw_back2framework
                  }


spritespos:   !byte $86,$a0,$9e,$a0,$b6,$a0,$ce,$a0
        !byte $be,$75,$d6,$75,$be,$60,$d6,$60


;/*-----------------------------------
;smoke animation
;-----------------------------------*/
smoke:      dec animationdelay
        bne animexit
        lda #6
        sta animationdelay
        inc screen+$3f8+4
        inc screen+$3f8+5
        inc screen+$3f8+6
        inc screen+$3f8+7
        dec animationcounter
        bne animexit
        lda #$40            ;smoke sprites
        sta screen+$3f8+4
        lda #$43
        sta screen+$3f8+5
        lda #$46
        sta screen+$3f8+6
        lda #$49
        sta screen+$3f8+7
        lda #3
        sta animationcounter
animexit:   rts

getcharbits:  lda #<fontaddress
        sta readchar+1
        lda #>fontaddress
        sta readchar+2

        ldy #0
        lda (textpointer),y
        asl
        asl
        asl
        sta readchar+1
        bcc skip3
        inc readchar+2

skip3:      ldx #7
readchar:   lda fontaddress,x
        sta chartmp,x
        dex
        bpl readchar

        lda #8
        sta bitcounter
        inc textpointer
        bne skip4
        inc textpointer+1
skip4:      ldy #0
        lda (textpointer),y
        cmp #$ff
        bne skip5
        lda #<scrolltext
        sta textpointer
        lda #>scrolltext
        sta textpointer+1
skip5:      rts



shiftbits:    ldx #0
        stx tmp0
        stx tmp1

llpp4:      ldx tmp0
        rol chartmp,x
        ldx tmp1
        rol spriteaddress+3*64+2,x
        rol spriteaddress+3*64+1,x
        rol spriteaddress+3*64+0,x

        rol spriteaddress+2*64+2,x
        rol spriteaddress+2*64+1,x
        rol spriteaddress+2*64+0,x

        rol spriteaddress+1*64+2,x
        rol spriteaddress+1*64+1,x
        rol spriteaddress+1*64+0,x

        rol spriteaddress+0*64+2,x
        rol spriteaddress+0*64+1,x
        rol spriteaddress+0*64+0,x


        txa
        clc
        adc #3
        sta tmp1
        inc tmp0
        lda tmp0
        cmp #8
        bcc llpp4

        dec bitcounter
        bne skip6
        jmp getcharbits
skip6:      rts


chartmp:  !fill 64,0




fade:     jsr fw_wait_irq
        jsr fw_wait_irq

        inc counter
        ldy counter
fade1:      lda (tmps),y
        sta (scr),y
        lda (tmpc),y
        sta (col),y
        dey
        bmi next
        jsr skiprow
        lda scr
        cmp #$e8      ;schon letzte zeile+1?
        beq next
        jmp fade1
next:     jsr setup
        ldy counter
        cpy #$27
        bne skip7
        dec counter
        ldy counter2
alignrow:   jsr skiprow
        dey
        bpl alignrow
        lda scr
        cmp #$e8
        beq fadeexit
        inc counter2
skip7:      jmp fade
fadeexit:   rts

fade2:      jsr fw_wait_irq
        jsr fw_wait_irq

        inc counter
        ldy counter
fade3:      lda #00
        sta (scr),y
        sta (col),y
        dey
        bmi next2
        jsr skiprow
        lda scr
        cmp #$e8      ;schon letzte zeile+1?
        beq next2
        jmp fade3
next2:      jsr setup
        ldy counter
        cpy #$27
        bne skip8
        dec counter
        ldy counter2
alignrow2:    jsr skiprow
        dey
        bpl alignrow2
        lda scr
        cmp #$e8
        beq fadeexit2
        inc counter2
skip8:      jmp fade2
fadeexit2:    rts




skiprow:    ldx #$00
skiprow1:   lda scr,x
        clc
        adc #$28
        sta scr,x
        bcc skip9
        inc scr+1,x
skip9:      inx
        inx
        cpx #$08
        bne skiprow1
        rts

setup:      lda #$00
        sta scr
        sta col
        sta tmps
        sta tmpc

        lda #$d8      ;hibyte write
        sta col+1
        lda #>screen
        sta scr+1

        lda #$4c      ;hibyte get
        sta tmps+1
        lda #$40
        sta tmpc+1
        rts


scrolltext:

!scr "                  a new challenge for every day. the whole week long. then release the stress. tgif has finally come. "
!scr "get a beer (not an oettinger but a faxe police) and drown it over to fill the atlantis. "
!scr "cheers with beers, no more fears. mayday, mayday, we are hitting the ground. "
;!scr "landing area 'goldregen', the only we've found! whoe to you people at bcc#13! "
;!scr "happiest folks we have ever seen. thunder.bird over and out. liked it? get loud! "
!byte $ff

* = fontaddress
!bin "gfx/font.bin"
* = $4000
!bin "gfx/colors.bin"
* = $4c00
!bin "gfx/screen.bin"
* = $5000
!bin "gfx/sprites.bin"
* = $6000
!bin "gfx/bitmap.bin"

