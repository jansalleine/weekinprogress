DEBUG = 0
LIB_INCLUDE = 0
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 0
LIB_HEX2SCREEN = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 0

                    !cpu 6510
                    !source "../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !source "../../lib/sync.asm"

screen    = $4000
screen1   = $4400
screen2   = screen1+1*$800
screen3   = screen1+2*$800
screen4   = screen1+3*$800
screen5   = screen1+4*$800

sync = $10
flipointer = $13
sinuspointer = $14
tmp0 = $20
tmp1 = tmp0+1
tmp2 = tmp0+2
tmp3 = tmp0+3
tmp4 = tmp0+4

                *= $1ffe
                !byte <code_start
                !byte >code_start
; ==============================================================================
code_start:     !if RELEASE=1 { +sync 0x002D00 + ( 17 * SAFETY_OFFSET ) }
                jsr fw_wait_irq
                sei
                lda #<irq1
                sta $fffe
                lda #>irq1
                sta $ffff

                lda #$2f
                sta $d012
                lda #$0b        ;screen off
                sta $d011
                asl $d019
                cli

                ldy #40
                ldx #0
loop1:          sty tmp0,x
                iny
                inx
                cpx #26
                bne loop1

deleteloop:     lda #<screen1
                sta delpos+1
                lda #>screen1
                sta delpos+2

                ldy #0
delloop:        ldx tmp0,y
                cpx #39
                bcs skip1
                lda #$20
delpos:         sta screen1+0*40,x
loop2:          lda delpos+1
                clc
                adc #40
                sta delpos+1
                bcc loop2
                inc delpos+2
skip1:          iny
                cpy #25
                bne delloop

                ldx #25
nexttm:         lda tmp0,x
                beq skp2
                dec tmp0,x
skp2:           dex
                bpl nexttm

                lda tmp0+25
                bne deleteloop

                lda #4
                sta shiftdelay

                lda #0
                sta $d020
                sta $d015
                sta sinuspointer
                sta flipointer

                lda #$18
                sta $d016
                lda #2
                sta $dd00
; ==============================================================================
; charset, tabellen, screens vorbereiten
; ==============================================================================

                lda #$08        ;mc black
                ldx #0
lp3:            sta $d800,x
                sta $d900,x
                sta $da00,x
                sta $db00,x
                inx
                bne lp3

                jsr clear18tab
                jsr setupcharset
                jsr setupscreens
                jsr tabsetup
                jsr plainlogo

                lda #$1b        ;screen on
                sta 0xD011
                jsr wait     ;start...
; ==============================================================================
; display logo
; ==============================================================================
                ;lda #4
                ;sta tmp1
                lda #0xA8
                sta tmp0

logoloop:       jsr wait
                jsr colorshift
                jsr setlogocolors

                lda tmp0
                beq +
                dec tmp0
                jmp logoloop
+
; ==============================================================================
; floating logo
; ==============================================================================
                lda #2
                sta tmp4
                lda #0
                sta tmp3

waveloop:       jsr wait
                jsr clear18tab
                jsr movelogo
                jsr colorshift

                lda tmp3
                bne skp4
                dec tmp4
skp4:           dec tmp3
                bne waveloop
                lda tmp4
                bne waveloop

; ==============================================================================
; logo fade 1
; ==============================================================================
                ;lda #2
                ;sta tmp4
                lda #0xC0
                sta tmp3
                lda #$ea
                sta fadeout1        ;fade out
                sta fadeout2

fadeoutloop:    jsr wait
                jsr clear18tab
                jsr movelogo
                jsr colorshift

                lda tmp3
                beq +
                dec tmp3
                jmp fadeoutloop
+
; ==============================================================================
; logo fade 2
; ==============================================================================
                lda #0
                sta tmp4

fadeoutloop2:   jsr wait
                jsr clear18tab
                jsr movelogo

                ldx tmp4
                lda #0
                sta color1values,x
                inx
                stx tmp4
                cpx #80
                bne fadeoutloop2
; ==============================================================================
; exit
; ==============================================================================

code_exit:      jsr wait
                sei
                lda #<fw_irq
                sta $fffe
                lda #>fw_irq
                sta $ffff
                lda #$0b
                sta $d011
                lda #$00
                sta $d012
                dec $d019
                cli

                !if RELEASE = 0 {
                  jmp *
                } else {
                  jmp fw_back2framework
                }


movelogo:       ldx sinuspointer
                stx tmp1
                ldy #0
                sty tmp0

lp5:            lda screenvalues,y
                sty tmp0
                ldx tmp1
                ldy sinus1,x
                sta tab18,y
                sta tab18-1,y
                sta tab18-2,y
                sta tab18,y

                ldx tmp0
                lda color1values,x
                sta color21,y
                sta color21-1,y
                sta color21-2,y
                sta color21-3,y
                lda color2values,x
                sta color22,y
                sta color22-1,y
                sta color22-2,y
                sta color22-3,y
                lda color3values,x
                sta color23,y
                sta color23-1,y
                sta color23-2,y
                sta color23-3,y

                inc tmp1
                ldy tmp0
                iny
                cpy #32
                bne lp5
                inc sinuspointer
                rts
; ==============================================================================
;subs
; ==============================================================================
setlogocolors:  ldx #33
lp6:            lda color2values,x
                sta color22+8,x
                lda color3values,x
                sta color23+8,x
                dex
                bpl lp6
                rts

wait:           lda #0
                sta sync
lp7:            lda sync
                beq lp7
                rts

plainlogo:      ldx #0
                ldy #32*8-1
lp19:           lda screenvalues+8,x
                sta tab18,y
                inx
                dey
                bpl lp19
                rts


clear18tab:     ldx #80
lp8:            lda #$90    ;=blank screen
                sta tab18-1,x
                lda #$00
                sta color21-1,x
                sta color22-1,x
                sta color23-1,x
                dex
                bne lp8
                rts

colorshift:     dec shiftdelay
                bne colorshiftex

                lda #2
                sta shiftdelay
                ldy color3values+39
                ldx #38
lp9:            lda color3values,x
                sta color3values+1,x
                dex
                bpl lp9
fadeout1:       !byte $2c
                ldy #$00
                sty color3values

                ldy color2values
                ldx #00
lp10:           lda color2values+1,x
                sta color2values,x
                inx
                cpx #40
                bne lp10
fadeout2:       !byte $2c
                ldy #$00
                sty color2values+39
colorshiftex:   rts

shiftdelay:     !byte 0

; ==============================================================================
; tabellen setup
; ==============================================================================
tabsetup:       ldx #$00      ;badline werte
lp11:           txa
                asl
                ora #$01
                and #$07
                ora #$10
                sta tab11,x
                inx
                cpx #80
                bne lp11

                ldy #4*8-1
                ldx #0

lp11a:          lda color1values,x
                sta color21+8,y
                lda color2values,x
                sta color22+8,y
                lda color3values,x
                sta color23+8,y
                lda screenvalues,x
                sta tab18+8,y
                dey
                inx
                cpx #4*8
                bne lp11a

                rts
; ==============================================================================
;code & graphics generators
; ==============================================================================
setupcharset:   ldy #0
lp13:           lda #$00
                ldx #0
fillchrst1:     sta screen+0*$100,x
fillchrst2:     sta screen+1*$100,x
fillchrst3:     sta screen+2*$100,x
fillchrst4:     sta screen+3*$100,x
                inx
                bne fillchrst1

                lda fillchrst1+2
                clc
                adc #8
                sta fillchrst1+2
                adc #1
                sta fillchrst2+2
                adc #1
                sta fillchrst3+2
                adc #1
                sta fillchrst4+2
                iny
                cpy #8
                bne lp13

                lda #0
                sta tmp4
                lda #$40
                jsr prepchars
                inc tmp4
                lda #$48
                jsr prepchars
                inc tmp4
                lda #$50
                jsr prepchars
                inc tmp4
                lda #$58
                jsr prepchars
                inc tmp4
                lda #$60
                jsr prepchars
                inc tmp4
                lda #$68
                jsr prepchars
                inc tmp4
                lda #$70
                jsr prepchars
                inc tmp4
                lda #$78
                jmp prepchars

prepchars:      sta tmp3
                lda #0          ; 1charset/128chars ---> 8charsets/128chars (first 2 lines only)
                sta tmp0
                sta tmp2
                lda #$27        ;hibyte charset
                sta tmp1

                ldx #0
splitchars:     ldy tmp4
                lda (tmp0),y
                ldy #0
                sta (tmp2),y
                iny
                sta (tmp2),y
                lda tmp0
                clc
                adc #8
                sta tmp0
                sta tmp2
                bcc skp6
                inc tmp1
                inc tmp3
skp6:           inx
                cpx #128
                bne splitchars
                rts


setupscreens:   ldx #0          ;4 screens = 4 char rows to fetch 8 char lines
lp15:           txa
                sta screen1+4,x
                inx
                cpx #32
                bne lp15

                ldy #0
lp16:           txa
                sta screen2+4,y
                inx
                iny
                cpy #32
                bne lp16

                ldy #0
lp17:           txa
                sta screen3+4,y
                inx
                iny
                cpy #32
                bne lp17

                ldy #0
lp18:           txa
                sta screen4+4,y
                inx
                iny
                cpy #32
                bne lp18

                lda #$00          ;fill gaps, $00=blank char
                ldx #3
lp19q:          sta screen1,x
                sta screen1+36,x
                sta screen2,x
                sta screen2+36,x
                sta screen3,x
                sta screen3+36,x
                sta screen4,x
                sta screen4+36,x
                dex
                bpl lp19q

                ldx #39
lp20:           sta screen5,x       ;blank screen
                dex
                bpl lp20
                rts

* = $2500               ;diverse tabellen
sinus1:
!byte 4,4,4,4,4,5,5,6,6,7,8,8,9,10,11,12
!byte 14,15,16,17,19,20,22,23,25,26,28,29,31,33,34,36
!byte 38,39,41,43,44,46,48,49,51,52,54,55,57,58,59,61
!byte 62,63,64,65,66,67,68,68,69,70,70,71,71,71,71,71
!byte 71,71,71,71,71,70,70,69,69,68,67,66,65,65,63,62
!byte 61,60,59,57,56,55,53,52,50,48,47,45,44,42,40,39
!byte 37,35,34,32,30,29,27,25,24,22,21,19,18,17,15,14
!byte 13,12,11,10,9,8,7,6,6,5,5,4,4,4,4,4
!byte 4,4,4,4,4,5,5,6,6,7,8,9,10,11,12,13
!byte 14,15,17,18,19,21,22,24,25,27,29,30,32,34,35,37
!byte 39,40,42,44,45,47,48,50,52,53,54,56,57,59,60,61
!byte 62,63,65,65,66,67,68,69,69,70,70,71,71,71,71,71
!byte 71,71,71,71,71,70,70,69,68,68,67,66,65,64,63,62
!byte 61,59,58,57,55,54,52,51,49,48,46,44,43,41,39,38
!byte 36,34,33,31,29,28,26,25,23,22,20,19,17,16,15,14
!byte 12,11,10,9,8,8,7,6,6,5,5,4,4,4,4,4

screenvalues:
!byte $7e,$7c,$7a,$78,$76,$74,$72,$70
!byte $5e,$5c,$5a,$58,$56,$54,$52,$50
!byte $3e,$3c,$3a,$38,$36,$34,$32,$30
!byte $1e,$1c,$1a,$18,$16,$14,$12,$10

color1values:
!byte $06,$04,$0e,$03,$01,$03,$0e,$06
!byte $06,$04,$0e,$03,$01,$03,$0e,$06
!byte $06,$04,$0e,$03,$01,$03,$0e,$06
!byte $06,$04,$0e,$03,$01,$03,$0e,$06

color2values:
!byte $06,$06,$06,$06,$06,$06,$06,$06
!byte $04,$04,$04,$04,$04,$04,$04,$04
!byte $0e,$0e,$0e,$0e,$0e,$0e,$0e,$0e
!byte $03,$03,$03,$03,$03,$03,$03,$03
!byte $01,$01,$01,$01,$01,$01,$01,$01

color3values:
!byte $01,$0f,$0c,$0b,$00,$0b,$0c,$0f
!byte $01,$0f,$0c,$0b,$00,$0b,$0c,$0f
!byte $01,$0f,$0c,$0b,$00,$0b,$0c,$0f
!byte $01,$0f,$0c,$0b,$00,$0b,$0c,$0f
!byte $01,$0f,$0c,$0b,$00,$0b,$0c,$0f


* = $2700
!bin"chars.raw"     ; charset

* = $2b00
color21:  !fill 256,$00   ;d021
* = $2c00
color22:  !fill 256,$00   ;d022
* = $2d00
color23:  !fill 256,$00   ;d023
* = $2e00
tab18:    !fill 256,$00   ;d018 tabelle
* = $2f00
tab11:    !fill 160,0     ;d011 tabelle

* = $3000
; ==============================================================================
;   irq  -   flexible line interlace
; ==============================================================================


irq1:           pha
                txa
                pha
                tya
                pha
                lda $dd04
                and #7
                eor #7
                sta *+4
                bpl *+2
                lda #$a9
                lda #$a9
                lda $eaa5
                cmp #$c9
                cmp #$c9
                cmp #$c9
                cmp #$c9
                cmp #$c9
                cmp #$c9
colorstart:     ldy flipointer
nextbadline:    lda color21,y
                sta $d021
readcolor1:     lda color22,y
                sta $d022
readcolor2:     lda color23,y
                sta $d023
                cmp #$c9
                cmp #$c9

tab18fetch:     lda #$00
                sta $d018
tab11fetch:     lda #$00
                sta $d011

                iny
                cmp #$c9
                cmp #$c9
                dec $dfff
                cmp #$c9

                ldx flipointer
                lda tab18,x
                sta tab18fetch+1
                lda tab11,x
                sta tab11fetch+1

                inx
                stx flipointer
                cpx #80
                bne nextbadline

                lda #0
                sta flipointer

                lda #$90
                sta $d018
                lda #$7b
                sta $d011
                asl $d019
                inc sync          ;eigener sync, da d012=00 zu spät wäre

                pla
                tay
                pla
                tax
                pla
                rti
; ==============================================================================
library:            !if LIB_INCLUDE = 1 {
                         !source"../../lib/library.asm"
                    }
