DEBUG = 0
LIB_INCLUDE = 0
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 0
LIB_HEX2SCREEN = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 0


counter             = $20   ; zählt die animationen

vicbank0            = 0x4000
bitmap0             = vicbank0+0x0000
vidmem0             = vicbank0+0x2000
dd00_val0           = <!(vicbank0/0x4000) & 3
d018_val0           = <(((vidmem0-vicbank0)/0x400) << 4)+ <(((bitmap0-vicbank0)/0x800) << 1)


                    !cpu 6510
                    !source"../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !source "../../lib/sync.asm"
                    * = bitmap0
                    ;!bin "gfx/boring-1.kla.bmp"
                    !bin "includes/bitmap1.bin"

                    * = vidmem0
                    !bin "includes/boring-1.kla.scr"

                    *= $1ffe
                    !byte <code_start
                    !byte >code_start
; ==============================================================================
code_start:         lda #0x0B
                    sta 0xD011
                    !if RELEASE=1 { +sync 0x00130E + ( 9 * SAFETY_OFFSET ) }
                    jsr fw_wait_irq
                    lda #0x18
                    sta 0xd016
                    lda #d018_val0
                    sta $d018
                    lda #0
                    sta $d020
                    lda #1
                    sta $d021

                    lda #dd00_val0
                    sta $dd00

                    jsr setcolor
                    jsr fw_wait_irq
                    lda #$3b
                    sta $d011

                    lda #0
                    sta counter
; ==============================================================================
; 16 animationen
; ==============================================================================

mainloop:           ldx #$20            ;zeitverzögerung
loop3:              jsr fw_wait_irq
                    dex
                    bne loop3

                    jsr switch

                    inc counter
                    lda counter
                    cmp #10             ; = anzahl der animationen
                    bne mainloop

; ==============================================================================
; exit
; ==============================================================================
code_exit:          jsr fw_wait_irq
                    lda #$0b
                    sta $d011
                    !if RELEASE = 0 {
                        jmp *
                    } else {
                        jmp fw_back2framework
                    }
; ==============================================================================
; color ram kopieren
; ==============================================================================
setcolor:
                    ldx #0
-                   lda colram_src+0x000,x
                    sta 0xD800+0x000,x
                    lda colram_src+0x100,x
                    sta 0xD800+0x100,x
                    lda colram_src+0x200,x
                    sta 0xD800+0x200,x
                    lda colram_src+0x2e8,x
                    sta 0xD800+0x2e8,x
                    inx
                    bne -
                    rts
switch:             lda #0
                    beq +
                    dec switch+1
                    jmp switch1
+                   lda #1
                    sta switch+1
switch0:            jsr boring1_kla_bmp_to_boring2_koa_bmp
                    jsr boring1_kla_scr_to_boring2_koa_scr
                    jsr boring1_kla_col_to_boring2_koa_col
                    rts
switch1:            jsr boring_2_koa_bmp_to_boring_1_kla_bmp
                    jsr boring_2_koa_scr_to_boring_1_kla_scr
                    jsr boring_2_koa_col_to_boring_1_kla_col
                    rts
; ==============================================================================
library:            !if LIB_INCLUDE = 1 {
                         !source"../../lib/library.asm"
                    }
; ==============================================================================
colram_src:         !bin "includes/boring-1.kla.col"
; ==============================================================================
                    !source "includes/sc_bmp.asm"
                    !source "includes/sc_col.asm"
                    !source "includes/sc_scr.asm"
