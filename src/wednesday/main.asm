DEBUG = 0
LIB_INCLUDE = 0
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 0
LIB_HEX2SCREEN = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 0

ENABLE              = 0x20
DISABLE             = 0x2C

BLACK               = 0x00
WHITE               = 0x01
RED                 = 0x02
CYAN                = 0x03
PURPLE              = 0x04
GREEN               = 0x05
BLUE                = 0x06
YELLOW              = 0x07
ORANGE              = 0x08
BROWN               = 0x09
PINK                = 0x0A
DARK_GREY           = 0x0B
GREY                = 0x0C
LIGHT_GREEN         = 0x0D
LIGHT_BLUE          = 0x0E
LIGHT_GREY          = 0x0F
; ==============================================================================
vicbank0            = 0x4000
bitmap0             = vicbank0+0x0000
vidmem0             = vicbank0+0x2000
;vidmem1             = vicbank0+0x2400
data_area           = vicbank0+0x2400
dd00_val0           = <!(vicbank0/0x4000) & 3
d018_val0           = <(((vidmem0-vicbank0)/0x400) << 4)+ <(((bitmap0-vicbank0)/0x800) << 1)
;d018_val1           = <(((vidmem1-vicbank0)/0x400) << 4)+ <(((bitmap0-vicbank0)/0x800) << 1)
; ==============================================================================
                    !cpu 6510
                    !source"../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !source "../../lib/sync.asm"
                    *= bitmap0
                    !bin "includes/pic_kack.prg",8000,2
                    *= vidmem0
                    !bin "includes/pic_kack.prg",1000,8000+2
                    *= $1ffe
                    !byte <code_start
                    !byte >code_start
; ==============================================================================
code_start:         lda #0x0B
                    sta 0xD011
                    !if RELEASE=1 { +sync 0x000F6F + ( 7 * SAFETY_OFFSET ) }
                    jsr init_vic
mainloop:           jsr fw_wait_irq
enable_fade_in:     jsr fade_in
enable_wait:        bit wait
enable_fade_out:    bit fade_out
                    lda d011_value
                    sta 0xD011
                    lda exit_flag
                    bne code_exit
                    jmp mainloop
exit_flag:          !byte 0x00
code_exit:          ;sei
                    ;~ lda #<fw_irq
                    ;~ sta $fffe
                    ;~ lda #>fw_irq
                    ;~ sta $ffff
                    lda #$0b
                    sta $d011
                    ;~ lda #$00
                    ;~ sta $d012
                    ;~ dec $d019
                    ;~ cli
                    !if RELEASE = 0 {
                         jmp *
                    } else {
                         jmp fw_back2framework
                    }
wait:               ldx #0xB7
-                   jsr fw_wait_irq
                    dex
                    bne -
                    lda #DISABLE
                    sta enable_wait
                    lda #ENABLE
                    sta enable_fade_out
                    rts
; ==============================================================================
                    !zone INIT
init_vic:           lda #0x00
                    sta 0xD020
                    ;lda #GREY
                    sta 0xD021
                    ldx #0
-                   lda colram_src+0x000,x
                    sta 0xD800+0x000,x
                    lda colram_src+0x100,x
                    sta 0xD800+0x100,x
                    lda colram_src+0x200,x
                    sta 0xD800+0x200,x
                    lda colram_src+0x2E8,x
                    sta 0xD800+0x2E8,x
                    inx
                    bne -
                    lda #d018_val0
                    sta 0xD018
                    lda #0x18
                    sta 0xD016
                    lda #dd00_val0
                    sta 0xDD00
                    rts
; ==============================================================================
                    !zone FADES
fade_in:            lda .fade_wait
                    beq +
                    dec .fade_wait
                    rts
+                   ldx .fade_in_pt
                    lda .fade_in_tab,x
                    sta .fade_wait
                    lda d011_value
                    eor #(0x0B XOR 0x3B)
                    sta d011_value
                    inx
                    stx .fade_in_pt
                    cpx #16
                    bne +
                    lda #DISABLE
                    sta enable_fade_in
                    lda #ENABLE
                    sta enable_wait
+
                    rts
.fade_wait:         !byte 0x00
.fade_in_pt:        !byte 0x00
.fade_in_tab:       !byte 1, 9, 1, 7, 1, 5, 2, 2
                    !byte 3, 1, 5, 1, 7, 1, 9, 1
                    !byte 1
fade_out:           lda .fade_wait
                    beq +
                    dec .fade_wait
                    rts
+                   ldx .fade_out_pt
                    lda .fade_out_tab,x
                    sta .fade_wait
                    lda d011_value
                    eor #(0x0B XOR 0x3B)
                    sta d011_value
                    inx
                    stx .fade_out_pt
                    cpx #17
                    bne +
                    inc exit_flag
                    lda #DISABLE
                    sta enable_fade_out
+
                    rts
.fade_out_pt:       !byte 0x00
.fade_out_tab:      !byte 9, 1, 7, 1, 5, 2, 2
                    !byte 3, 1, 5, 1, 7, 1, 9, 1
                    !byte 1, 1

d011_value:         !byte 0x0B
; ==============================================================================
                    !zone LIB
library:            !if LIB_INCLUDE = 1 {
                         !source"../../lib/library.asm"
                    }
; ==============================================================================
                    !zone DATA
                    *= data_area
;~ bitmap_src:         !bin "includes/pic_kack.prg",8000,2
;~ vidmem_src:         !bin "includes/pic_kack.prg",1000,8000+2
colram_src:         !bin "includes/pic_kack.prg",1000,9000+2
