;~ /*-------------------------------------------------------------------
;~ aue im kopf
;~ -------------------------------------------------------------------*/
sync      = $02
textpointer   = $03
softscroll    = $05

tmp0      = $10
tmp1      = tmp0+1
tmp2      = tmp0+2
tmp3      = tmp0+3
tmp4      = tmp0+4

animationdelay  = $20
animationcounter= $21
sinuspointer  = $22
sparkledelay  = $23
sparklecounter  = $24
rotationdelay = $25

screen      = $6000


DEBUG = 0
LIB_INCLUDE = 0
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 0
LIB_HEX2SCREEN = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 0

                    !cpu 6510
                    !source "../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !source "../../lib/sync.asm"


        *= $1ffe
                !byte <code_start
                !byte >code_start
; ==============================================================================
code_start:
        !if RELEASE=1 { +sync 0x004826 + ( 21 * SAFETY_OFFSET ) }
        jsr fw_wait_irq

        lda #$00
        sta $d020
        sta $d021
        sta $d011
        jsr fw_wait_irq


        sei
        lda #<irq1
        sta $fffe
        lda #>irq1
        sta $ffff
        lda #$fc
        sta $d012
        lda #$0b        ;screen off
        sta $d011
        asl $d019
        lda #2
        sta $dd00
        cli

        ldx #0
loopp1:     lda $6400,x
        sta $d800,x
        lda $6500,x
        sta $d900,x
        lda $6600,x
        sta $da00,x
        lda $6700,x
        sta $db00,x
        inx
        bne loopp1

        lda #0
        sta sinuspointer
        lda #$ff
        sta $d015
        sta $d01c

        lda #$c0        ;tablette
        sta sparklesprites
        lda #6
        sta animationcounter
        lda #8
        sta animationdelay

        lda #$c6
        sta sparklesprites+1
        lda #$c6+6
        sta sparklesprites+2
        lda #$c6+12
        sta sparklesprites+3
        lda #$c6+18
        sta sparklesprites+4
        lda #$c6+24
        sta sparklesprites+5
        lda #$c6+18
        sta sparklesprites+5

        lda #6
        sta sparklecounter
        lda #4
        sta sparkledelay
        lda #1
        sta rotationdelay

        lda #0
        sta softscroll
        sta tmp0
        lda #<scrolltext
        sta textpointer
        lda #>scrolltext
        sta textpointer+1


        ldx #0
loopp3:     lda #$a0
        sta screen+22*40,x
        sta screen+23*40,x
        sta screen+24*40,x
        lda #0
        sta $d800+22*40,x
        sta $d800+23*40,x
        sta $d800+24*40,x
        inx
        cpx #40
        bne loopp3

        lda #$3b          ;screen on
        sta screensw1+1
        lda #$1b
        sta screensw2+1

main:     jsr wait
        jsr tabletanim
        jsr sparkleanim
        jsr scroller
        jsr bgspriteanim
        lda tmp0
        beq main
        jsr wait
code_exit:          sei
                    lda #<fw_irq
                    sta $fffe
                    lda #>fw_irq
                    sta $ffff
                    lda #$0b
                    sta $d011
                    lda #$00
                    sta $d012
                    dec $d019
                    cli
                    !if RELEASE = 0 {
                         jmp *
                    } else {
                         jmp fw_back2framework
                    }




; ==============================================================================
; exit
; ==============================================================================

;code_exit:     jsr fw_wait_irq

;       sei
;                lda #<fw_irq
;                sta $fffe
;                lda #>fw_irq
;               sta $ffff
;                lda #$0b
;                sta $d011
;                lda #$00
;                sta $d012
;                dec $d019
;                cli



;                !if RELEASE = 0 {
;                      jmp *
;                 } else {
;                      jmp fw_back2framework
;                  }

;==========================================================
; text scroller
;==========================================================
scroller:   lda softscroll
        sec
        sbc #2            ; 2px oder 1px
        and #$07
        sta softscroll
        cmp #6            ; bei 2px/frame = 6, bei 1px/frame = 7
        beq shift2left
        rts
shift2left:   ldx #0
nextcolumn:   lda screen+23*40+1,x
        sta screen+23*40,x
        lda screen+24*40+1,x
        sta screen+24*40,x
        inx
        cpx #39
        bne nextcolumn

        ldy #0
        lda (textpointer),y
        cmp #$ff
        beq resetpointer
        clc
        adc #$80
        sta screen+23*40+39
        adc #$40
        sta screen+24*40+39
        inc textpointer
        bne scrollerex
        inc textpointer+1
scrollerex:   rts

resetpointer: lda #<scrolltext
        sta textpointer
        lda #>scrolltext
        sta textpointer+1
        lda #1
        sta tmp0
        rts


;==========================================================
; scroller background bewegen
;==========================================================
bgspriteanim: inc bgsprsinus
        ldx bgsprsinus
        ldy bgspritemove,x
        lda bgsprlo,y
        sta bgspraddr+1
        lda bgsprhi,y
        sta bgspraddr+2

        ldx #63
bgspraddr:    lda $ffff,x
        sta $7900,x
        dex
        bpl bgspraddr
        rts

bgsprsinus: !byte 0

bgspritemove:
!byte 0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1
!byte 1,1,2,2,2,2,3,3,3,4,4,4,4,5,5,5
!byte 6,6,6,6,7,7,7,8,8,8,8,9,9,9,9,10
!byte 10,10,10,10,11,11,11,11,11,11,11,11,11,11,11,11
!byte 11,11,11,11,11,11,11,11,11,11,11,11,10,10,10,10
!byte 10,9,9,9,9,9,8,8,8,7,7,7,7,6,6,6
!byte 5,5,5,5,4,4,4,3,3,3,3,2,2,2,2,1
!byte 2,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0
!byte 0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1
!byte 1,2,2,2,2,3,3,3,3,4,4,4,5,5,5,5
!byte 6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10
!byte 10,10,10,10,11,11,11,11,11,11,11,11,11,11,11,11
!byte 11,11,11,11,11,11,11,11,11,11,11,11,10,10,10,10
!byte 10,9,9,9,9,8,8,8,8,7,7,7,6,6,6,6
!byte 5,5,5,4,4,4,4,3,3,3,2,2,2,2,1,1
!byte 1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0


bgsprlo: !byte $00,$40,$80,$c0,$00,$40,$80,$c0,$00,$40,$80,$c0
bgsprhi: !byte $80,$80,$80,$80,$81,$81,$81,$81,$82,$82,$82,$82




;==========================================================
; geblubber
;==========================================================
sparkleanim:  dec sparkledelay
        bne sparkleanimex
        lda #4
        sta sparkledelay

        inc sparklesprites+1
        inc sparklesprites+2
        inc sparklesprites+3
        inc sparklesprites+4
        inc sparklesprites+5
        inc sparklesprites+6

        dec sparklecounter
        bne sparkleanimex

        lda #$c6
        sta sparklesprites+1
        lda #$c6+6
        sta sparklesprites+2
        lda #$c6+12
        sta sparklesprites+3
        lda #$c6+18
        sta sparklesprites+4
        lda #$c6+24
        sta sparklesprites+5
        lda #$c6+18
        sta sparklesprites+6
        lda #6
        sta sparklecounter
sparkleanimex:  rts


sparklesprites: !fill 7,0
;==========================================================
; pille
;==========================================================
tabletanim:   dec animationdelay
        bne animtabex
        lda #8            ;drehen
        sta animationdelay


        lda sinuspointer      ;auf&ab
        and #$3f
        tax
        lda #$98          ;tiefster punkt für pille
        sec
        sbc tabsinus,x
        sta pilleypos+1
        inc sinuspointer

        lda rotationdelay
        bne rotationwait
rotateoffset: ldx #0
        lda rotationtab,x
        sta rotationdelay
        inx
        txa
        and #$1f
        sta rotateoffset+1

        inc sparklesprites
        dec animationcounter
        bne animtabex
        lda #$c0
        sta sparklesprites
        lda #6
        sta animationcounter
animtabex:    rts
rotationwait: dec rotationdelay
        rts

tabsinus:
!byte 0,0,0,0,1,1,2,3,4,4,5,6,6,7,7,7
!byte 7,7,7,7,6,6,5,4,3,2,2,1,0,0,0,0
!byte 0,0,0,0,1,2,2,3,4,5,5,6,7,7,7,7
!byte 7,7,7,6,6,5,4,4,3,2,1,1,0,0,0,0

rotationtab:
!byte 0,0,0,0,0,1,0,1,0,1,0,1,0,0,1,0
!byte 0,0,1,0,0,0,1,0,0,2,0,1,0,0,1,0



;==========================================================
; irqs
;==========================================================
irq1:     pha
        lda #$d8
        sta $d016
        lda #$80      ;bitmap = $4000, screen = $6000
        sta $d018
        lda #0
        sta $d021

        lda sparklesprites
        sta screen+$3f8
        lda sparklesprites+1
        sta screen+$3f9
        lda sparklesprites+2
        sta screen+$3fa
        lda sparklesprites+3
        sta screen+$3fb
        lda sparklesprites+4
        sta screen+$3fc
        lda sparklesprites+5
        sta screen+$3fd
        lda sparklesprites+6
        sta screen+$3fe

        lda #$0f
        sta $d027
        sta $d028
        sta $d029
        sta $d02a
        sta $d02b
        sta $d02c
        sta $d02d

        lda #$01
        sta $d025
        lda #$0f
        sta $d026
        lda #$00
        sta $d01b
        sta $d017
        sta $d01d
        sta $d010

        lda #$98
        sta $d000
        sta $d002
        sta $d004
pilleypos:    lda #$98
        sta $d001
        lda #$88-20
        sta $d003
        lda #$5c
        sta $d005

        lda #$88
        sta $d006
        sta $d007
        sta $d008
        lda #$74
        sta $d009

        lda #$88
        sta $d00b
        lda #$a6
        sta $d00a
        sta $d00c
        lda #$70
        sta $d00d

screensw1:    lda #$0b
        sta $d011
        lda #$32+22*8
        sta $d012
        lda #<irq2
        sta $fffe
        lda #>irq2
        sta $ffff
        asl $d019
        inc sync
        pla
        rti


irq2:     pha
        lda #4
        sec
wait4ras:   sbc #1
        bne wait4ras
screensw2:    lda #$0b
        sta $d011


        lda #$ff
        sta $d01b
        sta $d01d
        lda #$c0
        sta $d010
        lda #$e4
        sta screen+$3f8
        sta screen+$3f9
        sta screen+$3fa
        sta screen+$3fb
        sta screen+$3fc
        sta screen+$3fd
        sta screen+$3fe
        sta screen+$3ff
        lda #$0d
        sta $d027
        sta $d028
        sta $d029
        sta $d02a
        sta $d02b
        sta $d02c
        sta $d02d
        sta $d02e
        lda #$32+23*8
        sta $d001
        sta $d003
        sta $d005
        sta $d007
        sta $d009
        sta $d00b
        sta $d00d
        sta $d00f
        lda #$18
        sta $d000
        lda #$42
        sta $d002
        lda #$6c
        sta $d004
        lda #$96
        sta $d006
        lda #$c0
        sta $d008
        lda #$ea
        sta $d00a
        lda #$14
        sta $d00c
        lda #$3e
        sta $d00e
        lda #$03
        sta $d025
        lda #$05
        sta $d026

        lda #$10
        ora softscroll
        sta $d016
        lda #$8e
        sta $d018
        lda #1
        sta $d021
        lda #$fc
        sta $d012
        lda #<irq1
        sta $fffe
        lda #>irq1
        sta $ffff
        asl $d019
        pla
        rti


wait:     lda #0
        sta sync
waitr:      lda sync
        beq waitr
        rts

scrolltext:
!scr  "                        "
!scr  "dang... what a night...     ...but the party isn#t over yet!     "
!scr  "thank you for watching "
!byte 34
!scr  "week in progress"
!byte 34
!scr  " by mayday! as always, "
!scr  "we started late and as always we swore to begin earlier next time, "
!scr  "because the demo could#ve been soooo much better if we only had more time!!!"
!scr  "     yeah, sounds familiar to you, too, eh?     life gets in the way, we "
!scr  "grew from kids to parents and the week gets filled up with work and family duties... "
!scr  "but that#s what we chose - and that#s what we ultimately love...     "
!scr  "it#s a gift to have a hobby that brings back good memories of easy times and connects "
!scr  "us with brothers from other mothers to enjoy this wonderful mix of fun, creativity ... "
!scr  "and beers! mayday loves you all! we hope you enjoyed our little demo, let#s party like it#s 1986!!!"
!scr  "                                        "
!byte $ff

* = $4000
!bin "gfx/bitmap.bin"
* = $6000
!bin "gfx/screen.bin"
* = $6400
!bin "gfx/colors.bin"
* = $7000
!bin "gfx/sprites.bin"
* = $7c00
!bin "gfx/chars_inverted.raw"
* = $8000
!bin "gfx/bgsprites.bin"

