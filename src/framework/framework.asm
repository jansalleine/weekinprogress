DEBUG = 0
                    !cpu 6510
; ==============================================================================
PAL_CYCLES          = 19655
; ==============================================================================
; ZEROPAGE ADRESSES
; -----------------
; ==============================================================================
;         $02 - $DF free to use!
; ==============================================================================
; $E0 - (max.) $F$  KRILL LOADER
; ==============================================================================
fw_irq_ready        = $f8
fw_irq_lo           = $f9
fw_irq_hi           = $fa
fw_global_ct        = $fb             ; $fb, $fc, $fd
; music player uses:  $fe, $ff
; ==============================================================================
; RAM ADRESSES
; ------------
fw_code_start       = $0200
fw_nmi_addr         = $0300
fw_music_init       = $0c00
fw_music_play       = fw_music_init+3
fw_music_volume     = fw_music_init+$135
fw_part_start_lo    = $1ffe
fw_part_start_hi    = $1fff
; ==============================================================================
; KRILL
; -----
                    !source "loadersymbols-c64.inc"
; ==============================================================================
                    *= fw_music_init
                    !if RELEASE = 1 {
                        !bin "music/0c00.sid",,$7e
                    }
                    !if RELEASE = 0 {
                        !bin "music/0c00-nointro.sid",,$7e
                    }
fw_music_end:

                    !if RELEASE = 1 {
                        *= loadcompd
                        !bin "loader-c64.prg",,2
                        *= $2000
                        !bin "install-c64.prg",,2
                    }
                    !if RELEASE = 0 {
                        *= $0400
                    }
fw_krill_end:
; ==============================================================================
                    jmp fw_init
fw_reloc_code_01_start:
!pseudopc fw_code_start {
; ==============================================================================
!zone MAINLOOP
fw_mainloop:
fw_load_flag:       lda #$00
                    bne fw_skip_load
                    lda #$00
                    sta loadaddrlo
                    sta loadaddrhi
                    ldx #<fw_filename
                    ldy #>fw_filename
                    jsr loadcompd
                    bcc +
                    jmp *
+                   ldx fw_part_pt
                    lda fw_part_list,x
                    sta fw_partnum
                    inc fw_part_pt
fw_skip_load:       lda fw_part_start_lo
                    sta fw_exit+1
                    lda fw_part_start_hi
                    sta fw_exit+2
fw_exit:            jmp $0000
fw_back2framework:  lda fw_partnum
                    jsr fw_hex2text
                    sta fw_filename
                    stx fw_filename+1
                    jmp fw_mainloop
fw_filename:        !tx "00"
fw_partnum:         !byte $00
fw_part_pt:         !byte 0x00
                    TITLE           = 0x01
                    SPRITEWECKER    = 0x02
                    MONDAY          = 0x03
                    WEDNESDAY       = 0x04
                    SATURDAY        = 0x05
                    WECKER          = 0x06
                    SIDEBORDERFLD   = 0x07
                    LOORBEERKRANZ   = 0x08
                    ARROWS          = 0x09
                    FLOATING_LOGO   = 0x0A
                    THURSDAY        = 0x0B
                    TUESDAY         = 0x0C
                    FRIDAY          = 0x0D
                    BLUHELIX        = 0x0E
                    SUNDAY          = 0x0F
fw_part_list:       !byte TITLE
                    !byte SPRITEWECKER
                    !byte MONDAY
                    !byte SPRITEWECKER
                    !byte TUESDAY
                    !byte SPRITEWECKER
                    !byte WEDNESDAY
                    !byte SPRITEWECKER
                    !byte THURSDAY
                    !byte SPRITEWECKER
                    !byte FRIDAY
                    !byte SPRITEWECKER
                    !byte SATURDAY
                    !byte LOORBEERKRANZ
                    !byte ARROWS
                    !byte BLUHELIX
                    !byte FLOATING_LOGO
                    !byte SIDEBORDERFLD
                    !byte WECKER
                    !byte SPRITEWECKER
                    !byte SUNDAY
                    !byte SPRITEWECKER
; ==============================================================================
fw_wait_irq:        lda #$00
                    sta fw_irq_ready
-                   lda fw_irq_ready
                    beq -
                    rts
; ==============================================================================
!zone HEX2TEXT
fw_hex2text:        sta .savea+1
                    and #%00001111
                    tax
                    lda .hextab,x
                    sta .low_nibble+1
.savea:             lda #0
                    lsr
                    lsr
                    lsr
                    lsr
                    tax
                    lda .hextab,x
.low_nibble:        ldx #0
                    rts
.hextab:            !tx "0123456789ABCDEF"
; ==============================================================================
!zone INIT_VIC
fw_init_vic:        lxa #$00
-                   sta $d000,x
                    inx
                    cpx #$2f
                    bne -
                    rts
}
fw_reloc_code_01_end:
; ==============================================================================
!zone IRQ
fw_reloc_code_02_start:
!pseudopc fw_nmi_addr {
fw_nmi:             pha
                    lda $01
                    sta fw_save_01+1
                    lda #$35
                    sta $01
                    lda $dd04
                    eor #7
                    sta *+4
                    bpl *+2
                    cmp #$c9
                    cmp #$c9
                    bit $ea24
                    bit $ea24
                    jmp *+8
                    nop
                    nop
                    jmp *+3
                    txa
                    pha
                    tya
                    pha
                    !if DEBUG = 1 {
                         inc $d020
                    }
                    jsr fw_music_play
                    clc
                    lda fw_global_ct
                    adc #$01
                    sta fw_global_ct
                    lda fw_global_ct+1
                    adc #$00
                    sta fw_global_ct+1
                    lda fw_global_ct+2
                    adc #$00
                    sta fw_global_ct+2
                    !if DEBUG = 1 {
                         dec $d020
                    }
                    lda $dd0d
fw_save_01:         lda #$00
                    sta $01
                    pla
                    tay
                    pla
                    tax
                    pla
                    rti
fw_irq:             pha
                    !if DEBUG = 1 {
                         inc $d020
                    }
                    lda $d012
                    cmp $d012
                    beq *-3
                    lda #$01
                    sta fw_irq_ready
                    lda #$00
                    sta $d012
                    lda $d011
                    and #$7f
                    sta $d011
                    !if DEBUG = 1 {
                         dec $d020
                    }
                    dec $d019
                    pla
                    rti
}
fw_reloc_code_02_end:
; ==============================================================================
!zone FW_INIT
fw_init:            sei
                    lda #$7f
                    sta $dc0d
                    sta $dd0d
                    lda $dc0d
                    lda $dd0d
                    !if RELEASE = 1 {
                        lda #$36
                        sta $01
                        jsr install
                    }
                    lda #$35
                    sta $01
                    lda #$00
                    sta $dc03
                    lda #$ff
                    sta $dc02
                    lda #%11111101
                    sta $dc00
                    lda $dc01
                    and #%10000000
                    sta $dc01
                    beq fw_model_8580
                    lda #$ff
                    cmp $d012
                    bne *-3
                    lda #$ff
                    sta $d412
                    sta $d40e
                    sta $d40f
                    lda #$20
                    sta $d412
                    lda $d41b
                    lsr
                    bcc fw_model_8580
fw_model_6581:      jsr fw_gen_old_sid
fw_model_8580:
                    ldx #(fw_reloc_code_01_end-fw_reloc_code_01_start)
-                   lda fw_reloc_code_01_start,x
                    sta fw_code_start,x
                    dex
                    cpx #$ff
                    bne -
                    ldx #(fw_reloc_code_02_end-fw_reloc_code_02_start)
-                   lda fw_reloc_code_02_start,x
                    sta fw_nmi_addr,x
                    dex
                    cpx #$ff
                    bne -
                    !if RELEASE = 1 {
                        lda #$00
                    }
                    !if RELEASE = 0 {
                        lda #$01
                    }
                    sta fw_load_flag+1
                    lda #$00
                    sta fw_global_ct
                    sta fw_global_ct+1
                    sta fw_global_ct+2
                    jsr fw_music_init
fw_init_cia2_a:     cmp $d012
                    bne fw_init_cia2_a
                    ldy #$08
                    sty $dd04
-                   dey
                    bne -
                    sty $dd05
                    sta $dd0e,y
                    lda #$11
                    cmp $d012
                    sty $d015
                    bne fw_init_cia2_a
fw_init_cia2_b:     lda #<PAL_CYCLES
                    sta $dd06
                    lda #>PAL_CYCLES
                    sta $dd07
                    bit $d011
                    bmi *-3
                    bit $d011
                    bpl *-3
                    lda #$20
-                   cmp $d012
                    bne -
                    lda $dd04
                    eor #$07
                    sta *+4
                    bpl *+2
                    cmp #$c9
                    cmp #$c9
                    bit $ea24
                    bit $ea24
                    lda #$01
                    sta $dd0f
                    lda #<fw_nmi
                    sta $fffa
                    lda #>fw_nmi
                    sta $fffb
                    lda #$1b
                    sta $d011
                    lda #$00
                    sta $d012
                    lda #$01
                    sta $d01a
                    lda #<fw_irq
                    sta $fffe
                    lda #>fw_irq
                    sta $ffff
                    lda #$82
                    sta $dd0d
                    dec $d019
                    cli
                    jmp fw_code_start
fw_gen_old_sid:     rts
fw_endmarker:
