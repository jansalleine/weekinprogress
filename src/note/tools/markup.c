#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

typedef unsigned char uchar;

typedef struct {
  char *name;
  uchar code;
  bool require_newline;
  bool (*parse_arguments) (void *tag, char *arg, char *error);
} Tag;

char *colors[16] = { "black", "white", "red", "cyan",
		     "purple", "green", "blue", "yellow",
		     "orange", "brown", "lightred", "darkgrey",
		     "grey", "lightgreen", "lightblue", "lightgrey" };

#define NEWLINE 0x0d
#define HYPHEN  0x01

void read(char **input, int *len) {
  
  (*len) = 0;
  char c;

  while((c = fgetc(stdin)) != EOF) {
    (*input) = (char*) realloc((*input), ((*len)+2) * sizeof(char));
    (*input)[(*len)] = c;
    (*len)++;
  }
  (*input)[(*len)] = '\0';
}

void parse(char *input, uchar **output, int *len) {

  int s = (*len);
  char c, n;
  (*len) = 0;

  bool newline = true;
  int line = 1;
  bool tagging = false;

  void out(uchar c) {
    (*output) = (uchar*) realloc((*output), ((*len)+2) * sizeof(uchar));
    (*output)[(*len)] = c;
    (*len)++;
    if(!tagging) {
      newline = c == NEWLINE;
    }
  }

  bool last() { return n == '\0'; }

  bool none(void *tag, char *arg, char *error) {
    out(((Tag*)tag)->code);
    return true;
  }

  bool nl(void *tag, char *arg, char *error) {
    out(((Tag*)tag)->code);
    newline = true;
    return true;
  }

  bool isColor(char *str) {
    char color = -1;

    for(int i=0; i<sizeof(colors)/sizeof(char*); i++) {
      if(strcmp(str, colors[i]) == 0) {
        color = i;
      }
    }
    return color > -1;
  }
  
  bool color(void *tag, char *arg, char *error) {

    char none = -1;
    char color = none;
    char *failed;
    
    for(int i=0; i<sizeof(colors)/sizeof(char*); i++) {
      if(strcmp(arg, colors[i]) == 0) {
        color = i;
      }
    }
    if(color == none) {
      color = strtol(arg, &failed, 0);
      if(arg == failed) {
        snprintf(error, 256, "in <%s>: '%s' is not a color name or value", ((Tag*)tag)->name, arg);
        return false;
      }
    }
      
    out(((Tag*)tag)->code);
    out(((uchar)color) << 4);
    return true;
  }
  
  bool skip(void *tag, char *arg, char *error) {
    bool result = false;
    char *failed;
    
    int w = strtol(arg, &failed, 0);
    
    if(arg != failed) {
      out(((Tag*)tag)->code);
      out(w/8);
      out(w%8);
      result = true;
    }
    else {
      snprintf(error, 256, "in <%s>: '%s' is not a number", ((Tag*)tag)->name, arg); 
    }
    return result;
  }

  Tag tags[9] = {
    { "n",       0x0d, false, &nl    },
    { "skip",    0x02, false, &skip  },
    { "color",   0x03, false, &color },    
    { "left",    0x04, true,  &none  },
    { "right",   0x05, true,  &none  },
    { "center",  0x06, true,  &none  },
    { "block",   0x07, true,  &none  },
    { "...",     0xa9, false, &none  },
    { "unknown", 0xff, true,  &none  },
  };

  int parse_tag(char *str) {
    Tag tag;
    char error[256] = "";
    int i = 0;    

    tagging = true;
    
    while(str[i] != '>') {
      if(str[i] == '\0' || str[i] == '<') {
        fprintf(stderr, "error: line %d: unclosed tag around '<%.35s...'\n", line, str);
        exit(EXIT_FAILURE);
      }
      i++;
    }
    str[i] = '\0';

    for(int i=0; i<sizeof(tags)/sizeof(tag); i++) {
      tag = tags[i];
      if(strncmp(str, tag.name, strlen(tag.name)) == 0) {
        break;
      }
    }
    
    if(strcmp(tag.name, "unknown") == 0) {
      if(isColor(str)) {
	
	tag = tags[2];
	if(!tag.parse_arguments(&tag, str, error)) {
	  fprintf(stderr, "error: line %d: %s\n", line, error);
	  exit(EXIT_FAILURE);
	}
	goto done;
	
      }
      else {
	fprintf(stderr, "warning: line %d: unknown tag [%s], skipping...\n", line, str);
	goto done;
      }
    }

    if(tag.require_newline && !newline) {
      fprintf(stderr, "error: line %d: tag [%s] not at start of line\n", line, tag.name);
      exit(EXIT_FAILURE);
    }
    
    while(!isspace(str[0]) && str[0] != '\0') {
      str++;
    }
    
    if(!tag.parse_arguments(&tag, ++str, error)) {
      fprintf(stderr, "error: line %d: %s\n", line, error);
      exit(EXIT_FAILURE);
    }

  done:
    tagging = false;
    return i+1;
  }  

  for(int i=0; i<s; i++) {
    c = input[i];
    n = input[i+1];

    switch((uchar)c) {

    /* escape markup characters and backslash */
      
    case '\\':
      if (n == '\\') { out('\\'); i++; }
      if (n == '<')  { out('<'); i++; }
      if (n == '>')  { out('>'); i++; }
      if (n == '|')  { out('|'); i++; }
      break;

    /* handle newlines */
      
    case '\n':
      if(n == '\n') {
        out(NEWLINE);
        out(NEWLINE);
        i++;
        line+=2;
      }
      else {
        if(n != '\0' && !newline) out(' ');
        line++;
      }
      break;

    /* handle hyphens */

    case '|':
      out(HYPHEN);
      break;
	
    /* handle tags */

    case '<':
      i += parse_tag(input+i+1);
      break;
      
    /* convert lower case to upper case */
      
    case 0x41 ... 0x5a:
      out(c + 0x20);
      break;

    /* convert upper case to lower case */
      
    case 0x61 ... 0x7a:
      out(c - 0x20);
      break;    

    /* pass through values > 0x7f (for entering umlauts in 8-bit encodings) */

    case 0xc3:
      if(!last()) { out(n); i++; }
      break;

    /* output character unchanged */
      
    default:
      out(input[i]);
    }
  }
  out(NEWLINE);
  out(0x00);   
}

void write(uchar *output, int len) {
  for(int i=0; i<len; i++) {
    putchar((char) (output[i]));
  }
}

int main(int argc, char **argv) {

  int len;
  char *input  = (char*) calloc(1, sizeof(char));
  uchar *output = (uchar*) calloc(1, sizeof(uchar));
  
  read(&input, &len);
  parse(input, &output, &len);
  write(output, len);
  
  free(input);
  free(output);

  return EXIT_SUCCESS;
}
