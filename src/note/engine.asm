//------------------------------------------------------------------------------
// Global variables
//------------------------------------------------------------------------------

// Generic zeropage pointers

/* These need to be defined by the client code	
.var p1 = $20
.var p2 = $22
.var p3 = $50
.var p4 = $fc
.var p5 = $fe
.var p6 = $52
.var p7 = $54
.var z1 = $56
*/
	
.var nextByte = z1

// Cursor position, offset, line, color

.var cursor = $fc
.var offset = $fe
.var next   = $52
.var line   = $54
	
// Memory locations
	
.var vicbase = $4000
.var bitmap  = $4000
.var charset = $6000
.var screen  = $7c00
.var scratch = $c000

// Tag codes

.namespace tag {
.label newline = $0d
.label hyphen  = $01
.label space   = $20
.label skip    = $02
.label color   = $03
.label left    = $04
.label right   = $05
.label center  = $06
.label block   = $07
.label justify = $08
}

//------------------------------------------------------------------------------
// Global Variables
//------------------------------------------------------------------------------	

foreground: .byte $00
background: .byte $00
margins:    .byte $00
maxwlow:    .byte $41
	
//------------------------------------------------------------------------------
// Code
//------------------------------------------------------------------------------	

init: {	
	jsr engine.measureFont
	jsr engine.alignFont	
	jsr engine.clearScreen
        jsr engine.highres
	rts
}

//------------------------------------------------------------------------------	

setMargins: {
        sta margins
        lda #$41
        sec sbc margins
        sec sbc margins
        sta maxwlow
        rts
}        
        
//------------------------------------------------------------------------------	
	
highres: {		
	// vicbase = $4000
        lda #$dd00
        and #%11111100
        ora #$2
        sta $dd00

        // screen ram at vicbase+$3c00 = $7c00
        lda $d018
        ora #$f0
        sta $d018

	// tell kernal about new screen ram position
	lda #>screen  
	sta $0288                         

        // hires mode on
        lda $d011
        ora #$20
        sta $d011
	
        rts
}

//------------------------------------------------------------------------------
		
clearScreen: {

	lda foreground
	ora background
	ldx #$00
!loop:
	sta screen+$0000,x
	sta screen+$0100,x
	sta screen+$0200,x
	sta screen+$0300,x
	inx
	bne !loop-
	
        lda #$00
        ldx #$00
!loop:        
        sta bitmap+$0000,x
        sta bitmap+$0100,x
        sta bitmap+$0200,x
        sta bitmap+$0300,x
        sta bitmap+$0400,x
        sta bitmap+$0500,x
        sta bitmap+$0600,x
        sta bitmap+$0700,x
        sta bitmap+$0800,x
        sta bitmap+$0900,x
        sta bitmap+$0a00,x
        sta bitmap+$0b00,x                                
        sta bitmap+$0c00,x
        sta bitmap+$0d00,x
        sta bitmap+$0e00,x
        sta bitmap+$0f00,x                                
        sta bitmap+$1000,x
        sta bitmap+$1100,x
        sta bitmap+$1200,x
        sta bitmap+$1300,x
        sta bitmap+$1400,x
        sta bitmap+$1500,x
        sta bitmap+$1600,x
        sta bitmap+$1700,x
        sta bitmap+$1800,x
        sta bitmap+$1900,x
        sta bitmap+$1a00,x
        sta bitmap+$1b00,x                                
        sta bitmap+$1c00,x
        sta bitmap+$1d00,x
        sta bitmap+$1e00,x
        sta bitmap+$1f00,x                                
        inx
        bne !loop-

        rts
}

//------------------------------------------------------------------------------
	
measureFont: {        

.var chr = p1
.var wdt = p2
.var mrg = p3
	
        lda #<charset sta chr
        lda #>charset sta chr+1

        lda #<width sta wdt
        lda #>width sta wdt+1        

        lda #<leftmargin sta mrg
        lda #>leftmargin sta mrg+1        
	
!loop:          
        jsr measureChar

        lda wdt+1
        cmp #>width.end
        bne !loop-
        
        lda wdt
        cmp #<width.end
        bne !loop-

        // define space width
        lda #$04
        sta width+$20
        rts
}

//------------------------------------------------------------------------------

measureChar: {
	
.var chr = p1
.var wdt = p2
.var mrg = p3

	// assume 8px initial width of char
	lda #$08
	sta wchar

	// assume 0px inital left margin of char
	lda #$00
	sta mchar	

fromLeft:	
	lda #$80
	sta column

!eachColumn:
	ldy #$07
	
!eachRow:
	lda (chr),y
	and column
	cmp column
	beq fromRight
	dey
	bpl !eachRow-

!emptyColumn:
	dec wchar
	inc mchar
	lsr column
	bne !eachColumn-

fromRight:
	lda #$01
	sta column
	
!eachColumn:
	ldy #$07
	
!eachRow:
	lda (chr),y
	and column
	cmp column
	beq store
	dey
	bpl !eachRow-

!emptyColumn:
	dec wchar
	asl column
	bne !eachColumn-

store:  ldy #$00
        lda wchar
	clc adc #1
        sta (wdt),y
        :incw(wdt)
	
	lda mchar
	sta (mrg),y
	:incw(mrg)
	
advanceCharPointer:        
	:addw(chr, $08)
done:   
        rts
	
	
column:	.byte $00
wchar:	.byte $00
mchar:	.byte $00	
}	

//------------------------------------------------------------------------------
	
alignFont: {

.var chr = p1
	
	lda #<charset sta chr
        lda #>charset sta chr+1

	lda #$00
	sta char
	
!eachChar:
	ldx char
	lda leftmargin,x
	sta pixels

	ldx #$08	

!eachByte:	
	ldy #$00
	lda (chr),y

	ldy pixels
	beq !nextByte+
	
!eachPixel:
	asl
	
!nextPixel:
	dey
	bne !eachPixel-

!nextByte:
	ldy #$00
	sta (chr),y
	:incw(chr)
	dex
	bne !eachByte-
	
!nextChar:
	inc char
	bne !eachChar-

done:	rts

char:	.byte $00
pixels:	.byte $00
}

//------------------------------------------------------------------------------
	
homeCursor: {
	lda #<bitmap
	sta cursor
	lda #>bitmap
	sta cursor+1

	lda #<[bitmap+8]
	sta next
	lda #>[bitmap+8]
	sta next+1
	
	lda #$00
	sta line

	lda margins
	sta offset

	rts
}
	
//------------------------------------------------------------------------------
	
print: {

.macro next() {
	:incw(text) jmp loop
}
	
.var text = p1
.var char = p2
.var col =  p3
	
	stx text
	sty text+1

	jsr layout

	lda #<screen sta col
	lda #>screen sta col+1
	
	lda #<scratch sta text
	lda #>scratch sta text+1
	
	ldy #$00
	
loop:	lax (text),y
	bne !skip+
	rts
	
!skip:	cmp #tag.newline
	bne !skip+

newline: {	
	inc line
	ldx line

	lda lines.low,x
	sta cursor
	sta next
	lda lines.high,x
	sta cursor+1
	sta next+1

	lda #<screen
	clc adc low,x
	sta col
	lda #>screen
	adc high,x
	sta col+1

	:addw(next, $08)

	lda margins
	sta offset
	
	:next()
}
	
!skip:	cmp #tag.skip
	bne !skip+

skip: {
	:incw(text)

	lax (text),y
	beq offsetOnly

eachChar:
	:addw(cursor, $08)
	:addw(next, $08)
	:incw(col)
	dex bne eachChar
	
offsetOnly:
	:incw(text)
	lda (text),y
	beq done
	clc adc offset
	sta offset
	cmp #$08
	bcc done

offsetOverflow:	
	sec sbc #$08
	sta offset

	:addw(cursor, $08)
	:addw(next, $08)
	:incw(col)	
done:	
}		
	:next()
	
!skip:	cmp #tag.color
	bne !skip+

	:incw(text)
	lda (text),y
	sta foreground
	:next()

!skip:

plot: {

getCharPosition:
	lda petscii2screencode,x
	pha

checkSpace: 
        cmp #tag.space
        bne lookupChar
space:
        :addw(cursor, $08)
        :addw(next, $08)
        jmp addOffset
	
lookupChar:
	tax
	lda charlow,x
	sta char
	lda charhigh,x
	sta char+1

	lda offset
	bne withOffset	
	
withoutOffset:	
	ldy #$07	

!eachByte:
	lda (char),y
	sta (cursor),y        
	dey bpl !eachByte-	
        iny
        
        pla tax
        lda width,x
        sta offset

	lda foreground
	ora background
	sta (col),y
	iny sta (col),y dey
        
	:next()

withOffset:
	ldx #$08

!eachByte:	
        sty nextByte

	lda (char),y
        beq !skip+

	ldy offset
	
!loop:	lsr ror nextByte
	dey bne !loop-

	ora (cursor),y
	sta (cursor),y

	lda nextByte
	sta (next),y

!skip:	:incw(char)
	:incw(cursor)
	:incw(next)

	dex bne !eachByte-	
	
addOffset:
	lda foreground
	ora background
	sta (col),y
	:incw(col)
	sta (col),y
	
	pla tax
	lda offset	
	clc adc width,x
	cmp #$08
	beq none
	bcc !skip+

	sec sbc #$08  
	sta offset
	:next()

none:	sty offset    
	:next()	
	
!skip:  sta offset 
	:subw(cursor, $08)
	:subw(next, $08)
	:decw(col)

	:next()
}	
}
	
//------------------------------------------------------------------------------

layout:	{
.var text = p1
.var out  = p2
.var sol  = p3
	
	lda #<scratch sta out
	lda #>scratch sta out+1

	lda #tag.left
	sta mode

nextLine:
	// reset the line width
	lda #$00
	sta wline
	sta wline+1

	// remember start of line in output
	lda out sta sol
	lda out+1 sta sol+1

	ldy #$00

checkDone:
	lda (text),y
	bne checkMode
	jmp done
	
checkMode:
	lda (text),y
	cmp #tag.left
	bne !skip+

	sta mode
	jmp next

!skip:  cmp #tag.block
	bne !skip+

	sta mode
	jmp next
	
!skip:	cmp #tag.right
	bne !skip+
	
	jsr insertJustification
	jmp next
		
!skip:  cmp #tag.center
	bne !skip+

	jsr insertJustification
	jmp next
	
!skip:	lda mode
	cmp #tag.right
	bne !skip+

	jsr insertJustification
	jmp loop

!skip:  lda mode
	cmp #tag.center
	bne !skip+

	jsr insertJustification
	jmp loop

!skip:	
loop:	lda (text),y
	bne continue
	jmp done

continue:
!skip:	cmp #tag.newline
	bne !skip+

	sta (out),y
	:incw(out)
	
	:incw(text)
	jmp justify

!skip:	cmp #tag.hyphen
	bne !skip+

	jmp next

!skip:	cmp #tag.color
	bne !skip+

	sta (out),y
	:incw(out)
	
	:incw(text)
	lda (text),y
	
	sta (out),y
	:incw(out)
	
	jmp next
	
!skip:	cmp #tag.skip
	bne !skip+

	sta (out),y
	:incw(out)
	
	:incw(text)
	lda (text),y

	sta (out),y
	:incw(out)
	
	tax
	beq offsetOnly

!loop:  :addw(wline, $08)
	dex
	bne !loop-

offsetOnly:
	:incw(text)
	lda (text),y
	
	sta (out),y
	:incw(out)

	clc adc wline
	sta wline

	lda wline+1
	adc #$0
	sta wline+1
	
	jmp next
	
!skip:  cmp #tag.space
	bne !skip+

	lda mode
	cmp #tag.block
	beq skipForSpace

	lda #tag.space
	jmp addWidth

skipForSpace:	
	lda #tag.skip
	sta (out),y
	:incw(out)

	lda #$00
	sta (out),y
	:incw(out)

	lda width+tag.space
	sta (out),y
	:incw(out)

	lda #tag.space
	
!skip:
addWidth:
	tax lda petscii2screencode,x 
	tax lda width,x
	clc adc wline
	sta wline
	
	lda wline+1
	adc #$0
	sta wline+1

checkWidth:
	lda wline+1
	cmp #$01
	bcc !skip+

	lda wline
	cmp maxwlow
	beq !skip+
	bcs rewind
	
!skip:	jmp put
        
rewind: sty pass

!loop:	lda (text),y
	cmp #tag.hyphen
	beq !skip+

        tax lda petscii2screencode,x 
	tax lda width,x
	sta tmp
        
        lda wline
        sec sbc tmp
	sta wline
	
	lda wline+1
	sbc #$0
	sta wline+1

!skip:  lda (text),y
	cmp #tag.space
	bne !skip+

	lda mode
	cmp #tag.block
	beq !block+
	jmp break
	
!block:	:decw(out)
	:decw(out)

	lda pass
	bne !block+

	:decw(out)
	
!block:	jmp break
	
!skip:	cmp #tag.hyphen
	bne !skip+
	
	lda pass
	cmp #$01	
	bcs softbreak

	:decw(text)

!skip:  cmp #$2d 
	bne !next+

	lda pass
	cmp #$01	
	bcs hardHyphen

!next:  :decw(text)
	:decw(out)
        inc pass

        jmp !loop-

hardHyphen:
	jmp hard

softbreak:
	:decw(text)
	lda (text),y
	:incw(text)
	
	sta(out),y
	:incw(out)

hard:	lda #$2d
	sta (out),y
	:incw(out)

	ldx petscii2screencode+$2d	
	
	lda wline
	clc adc width,x
	sta wline

	lda wline+1
	adc #0
	sta wline+1

break:  lda #tag.newline
	sta (out),y
	:incw(text)
	:incw(out)
	jmp justify

put:	lda (text),y
	cmp #tag.space
	bne !skip+

	lda mode
	cmp #tag.block
	beq next

	lda #tag.space
	
!skip:	sta (out),y
	:incw(out)
	
next:	:incw(text)
	jmp loop

justify:
	lda (sol),y
	cmp #tag.justify
	beq compute

	lda mode
	cmp #tag.block
	beq compute
	
	jmp nextLine
	
compute:
	lda maxwlow        // margin = $141 = 321px
	sta margin
	lda #$01
	sta margin+1

	lda margin      // margin = margin - wline
	sec sbc wline
	sta margin
	lda margin+1
	sbc wline+1
	sta margin+1

	lda mode
	cmp #tag.center
	bne !skip+
	
	lsr margin+1    // margin = margin/2
	ror margin

!skip:  lda mode
	cmp #tag.block
	beq blockJustify
	
	lda margin      // chars = margin
        sta chars       
	lda margin+1
	sta chars+1 

	lda #$00        // pixels = 0
	sta pixels

	ldx #$03        // chars = margin / 8
!loop:	lsr chars+1     // pixels = margin % 8
	ror chars
	ror pixels
	dex
	bne !loop-

	lsr pixels
	lsr pixels
	lsr pixels
	lsr pixels
	lsr pixels

store:	lda #tag.skip
	sta (sol),y
	:incw(sol)

	lda chars
	sta (sol),y
	:incw(sol)

	lda pixels
	sta (sol),y
	
	jmp nextLine
	
done:   sta (out),y
	rts

blockJustify: {
	lda (text),y
	cmp #tag.newline
	bne !skip+

	jmp done
	
!skip:	cmp #$00
	bne notLastLine
	
	jmp done

notLastLine:	
	sty spaces
	
checkEmptyLine:	
	lda (sol),y
	cmp #tag.newline
	bne checkMargin

	jmp nextLine

checkMargin:
	lda margin
	bne prepare

	jmp nextLine
	
prepare:
	lda sol sta start
	lda sol+1 sta start+1

	ldx margin
	
restart:
	lda start sta sol
	lda start+1 sta sol+1

!loop:	lda (sol),y
	cmp #tag.skip
	bne next

	inc spaces
	
	:addw(sol, $02)
	lda (sol),y
	clc adc #1
	cmp #$08
	bcs offsetOverflow
	
offsetOnly:
	sta (sol),y
	jmp decm

offsetOverflow:
	sec sbc #$08
	sta(sol),y

	:decw(sol)
	lda (sol),y
	clc adc #1
	sta (sol),y
	:incw(sol)

decm:   dex
	beq done
	
next:   lda (sol),y
	cmp #tag.newline
	bne !skip+

	lda spaces
	beq done
	
	jmp restart

!skip:	:incw(sol)	
	jmp !loop-
	
done:   jmp nextLine

spaces:	.byte $00
}
	
wline:  .word $0000
margin:	.word $0000
chars:	.word $0000
pixels:	.byte $00
mode:   .byte $00
tmp:    .byte $00
pass:	.byte $00
start:	.word $0000
}

//------------------------------------------------------------------------------
	
insertJustification: {
.var text = p1
.var out = p2

	sta layout.mode
	
	lda #tag.justify
	sta (out),y
	:incw(out)

	lda #00
	sta (out),y
	:incw(out)

	sta (out),y
	:incw(out)
	
	rts
}

//------------------------------------------------------------------------------

	
//------------------------------------------------------------------------------	

//------------------------------------------------------------------------------	
	
	
//------------------------------------------------------------------------------
// Tables
//------------------------------------------------------------------------------	

.pc = * "Character width table"
	
width: {
.pc = *+256 "Character margin table"
end:    
}

leftmargin: {
.pc = *+256 "Petscii to screencode conversion table"
end:    
}
	
petscii2screencode:	
/*            0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F   /*
/* 0 */ .byte $80, $81, $82, $83, $84, $85, $86, $87, $88, $89, $8a, $8b, $8c, $8d, $8e, $8f
/* 1 */ .byte $90, $91, $92, $93, $94, $95, $96, $97, $98, $99, $9a, $9b, $9c, $9d, $9e, $9f	
/* 2 */ .byte $20, $21, $22, $23, $24, $25, $26, $27, $28, $29, $2a, $2b, $2c, $2d, $2e, $2f
/* 3 */ .byte $30, $31, $32, $33, $34, $35, $36, $37, $38, $39, $3a, $3b, $3c, $3d, $3e, $3f
/* 4 */ .byte $00, $01, $02, $03, $04, $05, $06, $07, $08, $09, $0a, $0b, $0c, $0d, $0e, $0f	
/* 5 */ .byte $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $1a, $1b, $1c, $1d, $1e, $1f	
/* 6 */ .byte $40, $41, $42, $43, $44, $45, $46, $47, $48, $49, $4a, $4b, $4c, $4d, $4e, $4f
/* 7 */ .byte $50, $51, $52, $53, $54, $55, $56, $57, $58, $59, $5a, $5b, $5c, $5d, $5e, $5f
/* 8 */ .byte $c0, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $ca, $cb, $cc, $cd, $ce, $cf
/* 9 */ .byte $d0, $d1, $d2, $d3, $d4, $d5, $d6, $d7, $d8, $d9, $da, $db, $dc, $dd, $de, $df
/* A */ .byte $60, $61, $62, $63, $64, $65, $66, $67, $68, $69, $6a, $6b, $6c, $6d, $6e, $6f
/* B */ .byte $70, $71, $72, $73, $74, $75, $76, $77, $78, $79, $7a, $7b, $7c, $7d, $7e, $7f	
/* C */ .byte $40, $41, $42, $43, $44, $45, $46, $47, $48, $49, $4a, $4b, $4c, $4d, $4e, $4f
/* D */ .byte $50, $51, $52, $53, $54, $55, $56, $57, $58, $59, $5a, $5b, $5c, $5d, $5e, $5f
/* E */ .byte $60, $61, $62, $63, $64, $65, $66, $67, $68, $69, $6a, $6b, $6c, $6d, $6e, $6f
/* F */ .byte $70, $71, $72, $73, $74, $75, $76, $77, $78, $79, $7a, $7b, $7c, $7d, $7e, $5e

.pc = * "Char data adress table (lowbyte)"
	
charlow: {    
  .var i = 0
  .for(i=0; i<256; i++) {
    .byte [charset+i*8] & $ff
  }
}
.pc = * "Char data adress table (highbyte)"
	
charhigh: {	
  .var i = 0
  .for(i=0; i<256; i++) {
    .byte [charset+i*8] >> 8
  }
}

.pc = * "Line Address Table"

lines: {
  .var i=0
low:	
  .for(i=0; i<25; i++) {
    .byte [bitmap+i*40*8] & $ff
  }	
high:	
  .for(i=0; i<25; i++) {
    .byte [bitmap+i*40*8] >> 8
  }	
}

high:
.byte $00, $00, $00, $00, $00, $00, $00
.byte $01, $01, $01, $01, $01, $01
.byte $02, $02, $02, $02, $02, $02, $02
.byte $03, $03, $03, $03, $03
	
low:
.byte $00, $28, $50, $78, $A0, $C8, $F0
.byte $18, $40, $68, $90, $B8, $E0
.byte $08, $30, $58, $80, $A8, $D0, $F8
.byte $20, $48, $70, $98, $C0

.pc = * "End of Font Rendering Engine"
	
//------------------------------------------------------------------------------
// Macros
//------------------------------------------------------------------------------	
	
.macro incw(addr) {
        inc addr
        bne done
        inc addr+1
done:
}

.macro decw(addr) {
        dec addr
	lda addr
	cmp #$ff
        bne done
        dec addr+1
done:
}

.macro addw(addr, val) {
	lda addr
	clc adc #val
	sta addr
        bcc done
        inc addr+1
done:   
}
	
.macro subw(addr, val) {
	lda addr	
	sec sbc #val
	sta addr
        bcs done
        dec addr+1
done:   
}
	
//------------------------------------------------------------------------------
// End of file
//------------------------------------------------------------------------------
	
