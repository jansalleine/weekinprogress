// -*- mode: kasm *-*
//------------------------------------------------------------------------------
// Entry Point
//------------------------------------------------------------------------------	

:BasicUpstart2(main)

//------------------------------------------------------------------------------
// Zeropage locations required by Font Rendering Engine
//------------------------------------------------------------------------------	
	
.var p1 = $24
.var p2 = $22
.var p3 = $50
.var p4 = $10
.var p5 = $12
.var p6 = $52
.var p7 = $54        
.var z1 = $56
.var z2 = $57

//------------------------------------------------------------------------------
// Music
//------------------------------------------------------------------------------

.var sid = LoadSid("music.sid")	

.pc = sid.location "Music"
.fill sid.size, sid.getData(i)
        
//------------------------------------------------------------------------------
// Font Rendering Engine
//------------------------------------------------------------------------------	

.pc = $2b00 "Engine"
        
engine:	{ .import source "engine.asm" }
        
//------------------------------------------------------------------------------
// Text
//------------------------------------------------------------------------------

.pc = * "Text"
	
page1: .import binary "page1.pet"
page2: .import binary "page2.pet"
page3: .import binary "page3.pet"
	        
//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------
        
.pc = * "Code"

main: {
	sei

	lda #$35              // Basic & Kernal ROM aus
	sta $01
        
	lda #$7f              // System-Interrupt aus
	sta $dc0d             
	sta $dd0d

        lda #<irq             // IRQ-Vector auf irq-routine biegen            
	sta $fffe
	lda #>irq
	sta $ffff

	lda #<nmi             // NMI-Vector auf nmi-routine biegen
	sta $fffa 
	lda #>nmi
	sta $fffb

        asl $d019       // Anstehende raster-interrupts canceln
        
        lda #$01        // Raster-Interrupts aktivieren
	sta $d01a
        
	lda $d011       // höchstes BIT der Rasterzeile löschen
	and #%01111111
	sta $d011

	lda $80         // gewünschte Rasterzeile für ersten IRQ
	sta $d012
        
	ldx #$00
	ldy #$00	
	lda #sid.startSong-1
	jsr sid.init
                
        cli
        
        :screenOff()
	lda #$00 sta $d020 sta $d021
	lda #$f0 sta engine.foreground
	lda #$00 sta engine.background

        jsr clearColorRAM
        
	jsr engine.init

        lda #$00
        jsr engine.setMargins
        
!loop:
        :print(page1) :waitKey()
        :print(page2) :waitKey()
        :print(page3) :waitKey()
	jmp !loop-
}

//------------------------------------------------------------------------------
        
irq:    {
	pha
	txa pha
	tya pha
        
        jsr sid.play

        lda #$80
        sta $d012
        
        lda #$ff
        sta $d019
        
	pla tay                  // Prozessor-Register wiederherstellen, RTI
	pla tax
	pla
	rti         
}

//------------------------------------------------------------------------------
        
nmi:    { rti }
                
//------------------------------------------------------------------------------
        
clearColorRAM:  { 
	lda #$00 tax
!loop:	sta $d800,x
	sta $d900,x
	sta $da00,x
	sta $db00,x
	dex bne !loop-
        rts
}

//------------------------------------------------------------------------------
// Macros
//------------------------------------------------------------------------------
	
.macro waitKey() {

!wait:	lda #$00
	sta $dc00
	lda $dc01 
	cmp #$ff
	beq !wait-

!wait:	lda #$00
	sta $dc00
	lda $dc01 
	cmp #$ff
	bne !wait-
}

//------------------------------------------------------------------------------

.macro print(text) {

	:screenOff()
	jsr engine.clearScreen
	:screenOn()

	jsr engine.homeCursor
		
	ldx #<text
	ldy #>text
	jsr engine.print
}

//------------------------------------------------------------------------------	
	
.macro sync()      { wait: lda $d012 bne wait }
.macro screenOff() { :sync() lda #$0b sta $d011 }
.macro screenOn()  { :sync() lda #$3b sta $d011 }	
	
//------------------------------------------------------------------------------		
