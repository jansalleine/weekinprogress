colram0_src_to_colram1_src:
    ldy #$0c
    sty colram+$011b
    sty colram+$013c
    sty colram+$0143
    sty colram+$0164
    sty colram+$016b
    sty colram+$018c
    rts
colram1_src_to_colram2_src:
    ldy #$0b
    sty colram+$0193
    sty colram+$01b4
    rts
colram2_src_to_colram3_src:
    ldy #$0f
    sty colram+$016b
    sty colram+$018c
    rts
colram3_src_to_colram4_src:
    ldy #$0f
    sty colram+$0193
    sty colram+$01b4
    rts
colram4_src_to_colram5_src:
    ldy #$0c
    sty colram+$0193
    sty colram+$01b4
    sty colram+$01bb
    sty colram+$01dc
    ldy #$0f
    sty colram+$01e3
    sty colram+$0204
    rts
colram5_src_to_colram6_src:
    ldy #$0b
    sty colram+$01bb
    sty colram+$01dc
    ldy #$0f
    sty colram+$020b
    sty colram+$022c
    rts
colram6_src_to_colram7_src:
    ldy #$0f
    sty colram+$0233
    sty colram+$0254
    rts
colram7_src_to_colram8_src:
    ldy #$0b
    sty colram+$01e3
    sty colram+$0204
    ldy #$0f
    sty colram+$025b
    sty colram+$027c
    rts
colram8_src_to_colram9_src:
    rts
colram9_src_to_colram10_src:
    ldy #$0c
    sty colram+$011b
    sty colram+$013c
    sty colram+$0143
    sty colram+$0164
    sty colram+$016b
    sty colram+$018c
    rts
colram10_src_to_colram11_src:
    ldy #$0b
    sty colram+$0193
    sty colram+$01b4
    rts
colram11_src_to_colram12_src:
    ldy #$0f
    sty colram+$016b
    sty colram+$018c
    rts
colram12_src_to_colram13_src:
    ldy #$0f
    sty colram+$0193
    sty colram+$01b4
    rts
colram13_src_to_colram14_src:
    ldy #$0c
    sty colram+$0193
    sty colram+$01b4
    sty colram+$01bb
    sty colram+$01dc
    ldy #$0f
    sty colram+$01e3
    sty colram+$0204
    rts
colram14_src_to_colram15_src:
    ldy #$0b
    sty colram+$01bb
    sty colram+$01dc
    ldy #$0f
    sty colram+$020b
    sty colram+$022c
    rts
colram15_src_to_colram16_src:
    ldy #$0f
    sty colram+$0233
    sty colram+$0254
    rts
colram16_src_to_colram17_src:
    ldy #$0b
    sty colram+$01e3
    sty colram+$0204
    ldy #$0f
    sty colram+$025b
    sty colram+$027c
    rts
colram17_src_to_colram0_src:
    rts
