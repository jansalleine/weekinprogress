DEBUG = 0
LIB_INCLUDE = 1
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 1
LIB_HEX2SCREEN = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 1

ENABLE              = 0x20
DISABLE             = 0x2C

BLACK               = 0x00
WHITE               = 0x01
RED                 = 0x02
CYAN                = 0x03
PURPLE              = 0x04
GREEN               = 0x05
BLUE                = 0x06
YELLOW              = 0x07
ORANGE              = 0x08
BROWN               = 0x09
PINK                = 0x0A
DARK_GREY           = 0x0B
GREY                = 0x0C
LIGHT_GREEN         = 0x0D
LIGHT_BLUE          = 0x0E
LIGHT_GREY          = 0x0F
; ==============================================================================
zp_start            = 0x10
irq_ready           = zp_start
zp_temp0            = irq_ready+1
zp_temp0_lo         = zp_temp0
zp_temp0_hi         = zp_temp0_lo+1
; ==============================================================================
vicbank0            = 0x0000
charset0            = vicbank0+0x1000
vidmem0             = vicbank0+0x0400
colram              = 0xD800
dd00_val0           = <!(vicbank0/0x4000) & 3
d018_val0           = <(((vidmem0-vicbank0)/0x400) << 4)+ <(((charset0-vicbank0)/0x800) << 1)

decrunch_area_01    = 0x9E00
decrunch_area_02    = 0xB700
; ==============================================================================
                    !cpu 6510
                    !source "../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !source "../../lib/sync.asm"
                    *= $1ffe
                    !byte <code_start
                    !byte >code_start
; ==============================================================================
copy_area:          !fi 352, 0
; ==============================================================================
code_start:         lda #0x0B
                    sta 0xD011
                    !if RELEASE=1 { +sync 0x004088 + ( 19 * SAFETY_OFFSET ) }
                    jsr decrunch_next
                    jsr decrunch_next
                    jsr init_vic
                    jsr init_irq
                    jsr fade_in
                    clc
                    lda fw_global_ct
                    adc #0xA0
                    sta end_time_a+1
                    lda fw_global_ct+1
                    adc #0x02
                    sta end_time_b+1
                    lda fw_global_ct+2
                    adc #0x00
                    sta end_time_c+1
                    lda #ENABLE
                    sta enable_anim_frame
mainloop:           jsr wait_irq
                    !if DEBUG = 1 {
                        lda #4
                        sta 0xd020
                    }
                    lda decrunch_flag
                    beq +
                    lda #0
                    sta decrunch_flag
                    jsr decrunch_next
+
                    !if DEBUG = 1 {
                        lda #0
                        sta 0xd020
                    }
                    lda fw_global_ct
end_time_a:         cmp #0
                    bne +
                    lda fw_global_ct+1
end_time_b:         cmp #0
                    bne +
                    lda fw_global_ct+2
end_time_c:         cmp #0
                    bne +
                    lda #0x01
                    sta stop_anim_flag
                    lda #0x09
                    sta anim_counter+1
                    jmp mainloop_fade_out
+
                    jmp mainloop
decrunch_flag:      !byte 0x00
fade_out_ready_flag:!byte 0x00
mainloop_fade_out:  jsr wait_irq
                    lda fade_out_ready_flag
                    bne +
                    jmp mainloop_fade_out
+                   jsr fade_out_bars
code_exit:          sei
                    lda #<fw_irq
                    sta $fffe
                    lda #>fw_irq
                    sta $ffff
                    lda #$0b
                    sta $d011
                    lda #$00
                    sta $d012
                    dec $d019
                    cli
                    !if RELEASE = 0 {
                         jmp *
                    } else {
                         jmp fw_back2framework
                    }
; ==============================================================================
                    !zone INIT
init_vic:           lda #dd00_val0
                    sta 0xDD00
                    lda #d018_val0
                    sta 0xD018
                    lda #0x08
                    sta 0xD016
                    lda #0x00
                    sta 0xD020
                    sta 0xD021
                    jsr lib_colramfill
                    lda #0x20
                    ldx #>vidmem0
                    jsr lib_vidmemfill
                    rts
init_irq:           sei
                    lda #<irq_color
                    sta 0xFFFE
                    lda #>irq_color
                    sta 0xFFFF
                    lda #FIRST_COL_IRQ_LINE
                    sta 0xD012
                    lda #0x1B
                    sta 0xD011
                    asl 0xD019
                    cli
                    rts
; ==============================================================================
                    !zone FADE
fade_in:            jsr .fade_in_bars
                    ldy #24
--                  jsr .delay
                    ldx #39
-
.fade_mod0:         lda screen0,x
.fade_mod1:         sta vidmem0,x
.fade_mod2:         lda colram0,x
.fade_mod3:         sta colram,x
                    dex
                    bpl -
                    clc
                    lda .fade_mod0+1
                    adc #40
                    sta .fade_mod0+1
                    lda .fade_mod0+2
                    adc #0
                    sta .fade_mod0+2
                    clc
                    lda .fade_mod1+1
                    adc #40
                    sta .fade_mod1+1
                    lda .fade_mod1+2
                    adc #0
                    sta .fade_mod1+2
                    clc
                    lda .fade_mod2+1
                    adc #40
                    sta .fade_mod2+1
                    lda .fade_mod2+2
                    adc #0
                    sta .fade_mod2+2
                    clc
                    lda .fade_mod3+1
                    adc #40
                    sta .fade_mod3+1
                    lda .fade_mod3+2
                    adc #0
                    sta .fade_mod3+2
                    dey
                    bpl --
                    rts
                    FADE_IN_SPEED = 4
.delay:             lda #FADE_IN_SPEED
                    beq +
                    dec .delay+1
                    jsr wait_irq
                    jmp .delay
+                   lda #FADE_IN_SPEED
                    sta .delay+1
                    rts
.fade_in_bars:      jsr .fade_bar_loop
                    clc
                    lda .fade_bar_loop+1
                    adc #3
                    sta .fade_bar_loop+1
                    clc
                    lda .cpx+1
                    adc #3
                    sta .cpx+1
                    jsr .fade_bar_loop
                    clc
                    lda .fade_bar_loop+1
                    adc #3
                    sta .fade_bar_loop+1
                    clc
                    lda .cpx+1
                    adc #1
                    sta .cpx+1
                    jsr .fade_bar_loop
                    rts
.fade_bar_loop:     ldx #0
-                   jsr .delay_bars
                    lda coltab_src,x
                    sta coltab,x
                    inx
                    inx
.cpx:               cpx #6
                    bne -
                    rts
                    FADE_IN_BARS_SPEED = 20
.delay_bars:        lda #FADE_IN_BARS_SPEED
                    beq +
                    dec .delay_bars+1
                    jsr wait_irq
                    jmp .delay_bars
+                   lda #FADE_IN_BARS_SPEED
                    sta .delay_bars+1
                    rts
fade_out:           FADE_OUT_SPEED = 4
.delay_out:         lda #0x2F ; FADE_OUT_SPEED
                    beq +
                    dec .delay_out+1
                    rts
+                   lda #FADE_OUT_SPEED
                    sta .delay_out+1
.fade_out_pt:       ldy #0
                    lda .rand_tab40,y
                    tax
                    jsr .clear_line
                    iny
                    cpy #40
                    bne +
                    lda #DISABLE
                    sta enable_fade_out
                    lda #0x01
                    sta fade_out_ready_flag
                    ldy #0
+                   sty .fade_out_pt+1
                    rts
.clear_line:        lda #0x20
                    !for i, 0, 24 {
                        sta vidmem0+(i*40),x
                    }
                    rts
.rand_tab40:        !byte 31, 6, 15, 26, 25, 8, 9, 16
                    !byte 32, 10, 22, 5, 4, 29, 36, 34
                    !byte 19, 18, 3, 33, 2, 23, 14, 11
                    !byte 20, 24, 7, 13, 39, 27, 37, 30
                    !byte 0, 38, 17, 35, 21, 28, 1, 12
fade_out_bars:      jsr .fade_out_bar_loop
                    sec
                    lda .fade_out_bar_loop+1
                    sbc #1
                    sta .fade_out_bar_loop+1
                    sec
                    lda .cpx_out+1
                    sbc #3
                    sta .cpx_out+1
                    jsr .fade_out_bar_loop
                    sec
                    lda .fade_out_bar_loop+1
                    sbc #3
                    sta .fade_out_bar_loop+1
                    sec
                    lda .cpx_out+1
                    sbc #3
                    sta .cpx_out+1
                    jsr .fade_out_bar_loop
                    rts

.fade_out_bar_loop: ldx #8
-                   jsr .delay_bars
                    lda #0
                    sta coltab,x
                    dex
                    dex
.cpx_out:           cpx #4
                    bne -
                    rts
; ==============================================================================
                    !zone IRQ
irq0:               pha
                    txa
                    pha
                    tya
                    pha
                    lda #0
                    sta 0xD020
                    sta 0xD021
                    !if DEBUG=1 { dec 0xD020 }
enable_anim_frame:  bit anim_frame
enable_copy_frame:  bit copy_frame
enable_fade_out:    bit fade_out
                    !if DEBUG=1 { inc 0xD020 }
                    lda #FIRST_COL_IRQ_LINE
                    sta 0xD012
                    lda #<irq_color
                    sta 0xFFFE
                    lda #>irq_color
                    sta 0xFFFF
                    lda #0x01
                    sta irq_ready
irq_end:            asl 0xD019
                    pla
                    tay
                    pla
                    tax
                    pla
                    rti
irq_color:          pha
                    txa
                    pha
                    tya
                    pha
                    ldx #6
-                   dex
                    bne -
.color_pt:          ldx #0
                    lda coltab,x
                    sta 0xD020
                    sta 0xD021
                    lda .linetab,x
                    sta 0xD012
                    inc .color_pt+1
                    lda .color_pt+1
                    cmp #0x09
                    beq +
                    jmp irq_end
+                   lda #0
                    sta .color_pt+1
                    lda #<irq0
                    sta 0xFFFE
                    lda #>irq0
                    sta 0xFFFF
                    lda #0x00
                    sta 0xD012
                    jmp irq_end

coltab:             !byte BLACK
                    !byte BLACK
                    !byte BLACK
                    !byte BLACK
                    !byte BLACK
                    !byte BLACK
                    !byte BLACK
                    !byte BLACK
                    !byte BLACK

coltab_src:         !byte DARK_GREY       ; 0
                    !byte BLACK           ; 1
                    !byte DARK_GREY       ; 2
                    !byte GREY            ; 3
                    !byte DARK_GREY       ; 4
                    !byte GREY            ; 5
                    !byte LIGHT_GREY      ; 6
                    !byte GREY            ; 7
                    !byte LIGHT_GREY      ; 8

                    FIRST_COL_IRQ_LINE = 0xDA
.linetab:
                    !byte FIRST_COL_IRQ_LINE+3
                    !byte FIRST_COL_IRQ_LINE+3+3
                    !byte FIRST_COL_IRQ_LINE+3+3+9
                    !byte FIRST_COL_IRQ_LINE+3+3+9+3
                    !byte FIRST_COL_IRQ_LINE+3+3+9+3+3
                    !byte FIRST_COL_IRQ_LINE+3+3+9+3+3+9
                    !byte FIRST_COL_IRQ_LINE+3+3+9+3+3+9+3
                    !byte FIRST_COL_IRQ_LINE+3+3+9+3+3+9+3+3
                    !byte FIRST_COL_IRQ_LINE

wait_irq:           lda #0x00
                    sta irq_ready
-                   lda irq_ready
                    beq -
                    rts
; ==============================================================================
                    !zone ANIMATION
                    ANIM_SPEED = 3
anim_frame:         lda #ANIM_SPEED
                    beq +
                    dec anim_frame+1
                    lda anim_frame+1
                    cmp #(ANIM_SPEED-1)
                    bne .fake
                    lda #ENABLE
                    sta enable_copy_frame
.fake:              rts
+                   lda #ANIM_SPEED
                    sta anim_frame+1
                    ldx .current_anim
                    lda jumptable0_lo,x
                    sta .update_screen+1
                    lda jumptable0_hi,x
                    sta .update_screen+2
                    lda jumptable1_lo,x
                    sta .update_colram+1
                    lda jumptable1_hi,x
                    sta .update_colram+2
                    inx
anim_counter:       cpx #18
                    bne ++
                    lda stop_anim_flag
                    beq +
                    lda #DISABLE
                    sta enable_anim_frame
                    lda #ENABLE
                    sta enable_fade_out
+                   ldx #0
++                  stx .current_anim
.update_screen:     jsr .fake
.update_colram:     jsr .fake
                    ldx #0
.update_colram_s:   lda copy_area+(0*32),x
                    sta 0xD800+4+(6*40),x
                    lda copy_area+(1*32),x
                    sta 0xD800+4+(7*40),x
                    lda copy_area+(2*32),x
                    sta 0xD800+4+(8*40),x
                    lda copy_area+(3*32),x
                    sta 0xD800+4+(9*40),x
                    lda copy_area+(4*32),x
                    sta 0xD800+4+(10*40),x
                    lda copy_area+(5*32),x
                    sta 0xD800+4+(11*40),x
                    lda copy_area+(6*32),x
                    sta 0xD800+4+(12*40),x
                    lda copy_area+(7*32),x
                    sta 0xD800+4+(13*40),x
                    lda copy_area+(8*32),x
                    sta 0xD800+4+(14*40),x
                    lda copy_area+(9*32),x
                    sta 0xD800+4+(15*40),x
                    lda copy_area+(10*32),x
                    sta 0xD800+4+(16*40),x
                    inx
                    cpx #32
                    bne .update_colram_s
                    rts

.current_anim:      !byte 0x00
stop_anim_flag:     !byte 0x00

copy_frame:         clc
                    lda .current_anim
                    adc #1
                    cmp #18
                    bne +
                    lda #<decrunch_area_01
                    eor #<( decrunch_area_01 XOR decrunch_area_02 )
                    sta *-3
                    sta .mod01+1
                    lda #>decrunch_area_01
                    eor #>( decrunch_area_01 XOR decrunch_area_02 )
                    sta *-3
                    sta .mod01+2
                    lda #<(decrunch_area_01+0xB0)
                    eor #<( decrunch_area_01+0xB0 XOR decrunch_area_02+0xB0 )
                    sta *-3
                    sta .mod02+1
                    lda #>(decrunch_area_01+0xB0)
                    eor #>( decrunch_area_01+0xB0 XOR decrunch_area_02+0xB0 )
                    sta *-3
                    sta .mod02+2
                    lda #1
                    sta decrunch_flag
+                   ldx #0x00
.mod01:             lda decrunch_area_01+352,x
                    sta copy_area,x
.mod02:             lda decrunch_area_01+0xB0+352,x
                    sta copy_area+0xB0,x
                    inx
                    cpx #0xB0
                    bne .mod01
                    clc
                    lda .mod01+1
                    adc #<(352)
                    sta .mod01+1
                    lda .mod01+2
                    adc #>(352)
                    sta .mod01+2
                    clc
                    lda .mod02+1
                    adc #<(352)
                    sta .mod02+1
                    lda .mod02+2
                    adc #>(352)
                    sta .mod02+2
                    lda #DISABLE
                    sta enable_copy_frame
                    rts
; ==============================================================================
                    !zone DECRUNCH
exod_addr:          !src "includes/wrap.asm"
                    !src "includes/exodecrunch.asm"
                    NUM_SCREENS = 10
decrunch_next:      ldx #0
                    lda decrunch_table_lo,x
                    sta opbase+1
                    lda decrunch_table_hi,x
                    sta opbase+2
                    inx
                    cpx #NUM_SCREENS
                    bne +
                    ldx #0
+                   stx decrunch_next+1
                    jsr exod_addr
                    rts
; ==============================================================================
                    !zone LIB
library:            !if LIB_INCLUDE = 1 {
                         !source "../../lib/library.asm"
                    }
; ==============================================================================
                    !zone DATA
data_start:
screen0:            !bin "includes/screen0.src"
colram0:            !bin "includes/colram0.src"
                    !src "includes/speedcode_screen.asm"
                    !src "includes/speedcode_colram.asm"
; ==============================================================================
jumptable0_lo:      !byte <screen0_src_to_screen1_src
                    !byte <screen1_src_to_screen2_src
                    !byte <screen2_src_to_screen3_src
                    !byte <screen3_src_to_screen4_src
                    !byte <screen4_src_to_screen5_src
                    !byte <screen5_src_to_screen6_src
                    !byte <screen6_src_to_screen7_src
                    !byte <screen7_src_to_screen8_src
                    !byte <screen8_src_to_screen9_src
                    !byte <screen9_src_to_screen10_src
                    !byte <screen10_src_to_screen11_src
                    !byte <screen11_src_to_screen12_src
                    !byte <screen12_src_to_screen13_src
                    !byte <screen13_src_to_screen14_src
                    !byte <screen14_src_to_screen15_src
                    !byte <screen15_src_to_screen16_src
                    !byte <screen16_src_to_screen17_src
                    !byte <screen17_src_to_screen0_src
jumptable0_hi:      !byte >screen0_src_to_screen1_src
                    !byte >screen1_src_to_screen2_src
                    !byte >screen2_src_to_screen3_src
                    !byte >screen3_src_to_screen4_src
                    !byte >screen4_src_to_screen5_src
                    !byte >screen5_src_to_screen6_src
                    !byte >screen6_src_to_screen7_src
                    !byte >screen7_src_to_screen8_src
                    !byte >screen8_src_to_screen9_src
                    !byte >screen9_src_to_screen10_src
                    !byte >screen10_src_to_screen11_src
                    !byte >screen11_src_to_screen12_src
                    !byte >screen12_src_to_screen13_src
                    !byte >screen13_src_to_screen14_src
                    !byte >screen14_src_to_screen15_src
                    !byte >screen15_src_to_screen16_src
                    !byte >screen16_src_to_screen17_src
                    !byte >screen17_src_to_screen0_src

jumptable1_lo:      !byte <colram0_src_to_colram1_src
                    !byte <colram1_src_to_colram2_src
                    !byte <colram2_src_to_colram3_src
                    !byte <colram3_src_to_colram4_src
                    !byte <colram4_src_to_colram5_src
                    !byte <colram5_src_to_colram6_src
                    !byte <colram6_src_to_colram7_src
                    !byte <colram7_src_to_colram8_src
                    !byte <colram8_src_to_colram9_src
                    !byte <colram9_src_to_colram10_src
                    !byte <colram10_src_to_colram11_src
                    !byte <colram11_src_to_colram12_src
                    !byte <colram12_src_to_colram13_src
                    !byte <colram13_src_to_colram14_src
                    !byte <colram14_src_to_colram15_src
                    !byte <colram15_src_to_colram16_src
                    !byte <colram16_src_to_colram17_src
                    !byte <colram17_src_to_colram0_src
jumptable1_hi:      !byte >colram0_src_to_colram1_src
                    !byte >colram1_src_to_colram2_src
                    !byte >colram2_src_to_colram3_src
                    !byte >colram3_src_to_colram4_src
                    !byte >colram4_src_to_colram5_src
                    !byte >colram5_src_to_colram6_src
                    !byte >colram6_src_to_colram7_src
                    !byte >colram7_src_to_colram8_src
                    !byte >colram8_src_to_colram9_src
                    !byte >colram9_src_to_colram10_src
                    !byte >colram10_src_to_colram11_src
                    !byte >colram11_src_to_colram12_src
                    !byte >colram12_src_to_colram13_src
                    !byte >colram13_src_to_colram14_src
                    !byte >colram14_src_to_colram15_src
                    !byte >colram15_src_to_colram16_src
                    !byte >colram16_src_to_colram17_src
                    !byte >colram17_src_to_colram0_src
; ==============================================================================
decrunch_table_lo:  !byte <decrunch_data_01
                    !byte <decrunch_data_02
                    !byte <decrunch_data_03
                    !byte <decrunch_data_04
                    !byte <decrunch_data_05
                    !byte <decrunch_data_06
                    !byte <decrunch_data_07
                    !byte <decrunch_data_08
                    !byte <decrunch_data_09
                    !byte <decrunch_data_10

decrunch_table_hi:  !byte >decrunch_data_01
                    !byte >decrunch_data_02
                    !byte >decrunch_data_03
                    !byte >decrunch_data_04
                    !byte >decrunch_data_05
                    !byte >decrunch_data_06
                    !byte >decrunch_data_07
                    !byte >decrunch_data_08
                    !byte >decrunch_data_09
                    !byte >decrunch_data_10
; ==============================================================================
                    !bin "includes/achim-exo.prg"
decrunch_data_01:   !bin "includes/awsm-exo.prg"
decrunch_data_02:   !bin "includes/gijoe-exo.prg"
decrunch_data_03:   !bin "includes/irata-exo.prg"
decrunch_data_04:   !bin "includes/roly-exo.prg"
decrunch_data_05:   !bin "includes/spider-exo.prg"
decrunch_data_06:   !bin "includes/theryk-exo.prg"
decrunch_data_07:   !bin "includes/theuser-exo.prg"
decrunch_data_08:   !bin "includes/thunder-exo.prg"
decrunch_data_09:   !bin "includes/mayday-exo.prg"
decrunch_data_10:
data_end:
