DEBUG = 0
LIB_INCLUDE = 0
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 0
LIB_HEX2vidmem0 = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 0

ENABLE              = 0x20
DISABLE             = 0x2C

BLACK               = 0x00
WHITE               = 0x01
RED                 = 0x02
CYAN                = 0x03
PURPLE              = 0x04
GREEN               = 0x05
BLUE                = 0x06
YELLOW              = 0x07
ORANGE              = 0x08
BROWN               = 0x09
PINK                = 0x0A
DARK_GREY           = 0x0B
GREY                = 0x0C
LIGHT_GREEN         = 0x0D
LIGHT_BLUE          = 0x0E
LIGHT_GREY          = 0x0F

irq_ready           = 0x05
; ==============================================================================
vicbank0            = 0x4000
bitmap0             = vicbank0+0x0000
vidmem0             = vicbank0+0x2000
;vidmem1             = vicbank0+0x2400
sprite_data         = vicbank0+0x2400
sprite_base         = <((sprite_data-vicbank0)/0x40)
dd00_val0           = <!(vicbank0/0x4000) & 3
d018_val0           = <(((vidmem0-vicbank0)/0x400) << 4)+ <(((bitmap0-vicbank0)/0x800) << 1)
;d018_val1           = <(((vidmem1-vicbank0)/0x400) << 4)+ <(((bitmap0-vicbank0)/0x800) << 1)
; ==============================================================================
                    !cpu 6510
                    !source"../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !source "../../lib/sync.asm"
                    *= bitmap0
                    !bin "includes/pic_grrl.prg",8000,2
                    *= vidmem0
                    !bin "includes/pic_grrl.prg",1000,8000+2
                    *= $1ffe
                    !byte <code_start
                    !byte >code_start
; ==============================================================================
                    !zone IRQ
irq:                pha
                    txa
                    pha
                    tya
                    pha
enable_sprites_anim:jsr sprites_anim
                    lda #0x01
                    sta irq_ready
irq_end:            asl 0xD019
                    pla
                    tay
                    pla
                    tax
                    pla
                    rti
; ==============================================================================
code_start:         lda #0x0B
                    sta 0xD011
                    !if RELEASE=1 { +sync 0x0019F9 + ( 13 * SAFETY_OFFSET ) }
                    jsr init_vic
                    jsr init_irq
mainloop:           jsr wait_irq
enable_fade_in:     jsr fade_in
enable_wait_spr:    bit wait_spr
enable_wait:        bit wait
enable_fade_out:    bit fade_out
                    lda d011_value
                    sta 0xD011
                    lda exit_flag
                    bne code_exit
                    jmp mainloop
exit_flag:          !byte 0x00
code_exit:          lda #DISABLE
                    sta enable_sprites_anim
                    sei
                    lda #<fw_irq
                    sta $fffe
                    lda #>fw_irq
                    sta $ffff
                    lda #$0b
                    sta $d011
                    lda #$00
                    sta $d012
                    dec $d019
                    cli
                    !if RELEASE = 0 {
                         jmp *
                    } else {
                         jmp fw_back2framework
                    }
wait:               ldy #1
--                  ldx #0xAF
-                   jsr wait_irq
                    dex
                    bne -
                    dey
                    bpl --
                    lda #DISABLE
                    sta enable_wait
                    lda #ENABLE
                    sta enable_fade_out
                    rts
wait_spr:           ldx #0x4F
-                   jsr wait_irq
                    dex
                    bne -
                    lda #DISABLE
                    sta enable_wait_spr
                    lda #ENABLE
                    sta enable_wait
                    jsr sprites_init
wait_irq:           lda #0
                    sta irq_ready
-                   lda irq_ready
                    beq -
                    rts
; ==============================================================================
                    !zone INIT
init_vic:           lda #GREY
                    sta 0xD020
                    lda #BLACK
                    sta 0xD021
                    ldx #0
-                   lda colram_src+0x000,x
                    sta 0xD800+0x000,x
                    lda colram_src+0x100,x
                    sta 0xD800+0x100,x
                    lda colram_src+0x200,x
                    sta 0xD800+0x200,x
                    lda colram_src+0x2E8,x
                    sta 0xD800+0x2E8,x
                    inx
                    bne -
                    lda #d018_val0
                    sta 0xD018
                    lda #0x18
                    sta 0xD016
                    lda #dd00_val0
                    sta 0xDD00
                    rts
init_irq:           sei
                    lda #<irq
                    sta 0xFFFE
                    lda #>irq
                    sta 0xFFFF
                    lda #0xFF
                    sta 0xD012
                    asl 0xD019
                    cli
                    rts
; ==============================================================================
                    !zone FADES
fade_in:            lda .fade_wait
                    beq +
                    dec .fade_wait
                    rts
+                   ldx .fade_in_pt
                    lda .fade_in_tab,x
                    sta .fade_wait
                    lda d011_value
                    eor #(0x0B XOR 0x3B)
                    sta d011_value
                    inx
                    stx .fade_in_pt
                    cpx #16
                    bne +
                    lda #DISABLE
                    sta enable_fade_in
                    lda #ENABLE
                    sta enable_wait_spr

+
                    rts
.fade_wait:         !byte 0x00
.fade_in_pt:        !byte 0x00
.fade_in_tab:       !byte 1, 9, 1, 7, 1, 5, 2, 2
                    !byte 3, 1, 5, 1, 7, 1, 9, 1
                    !byte 1
fade_out:           lda .fade_wait
                    beq +
                    dec .fade_wait
                    rts
+                   ldx .fade_out_pt
                    lda .fade_out_tab,x
                    sta .fade_wait
                    lda d011_value
                    eor #(0x0B XOR 0x3B)
                    sta d011_value
                    inx
                    stx .fade_out_pt
                    cpx #17
                    bne +
                    inc exit_flag
                    lda #DISABLE
                    sta enable_fade_out
+
                    rts
.fade_out_pt:       !byte 0x00
.fade_out_tab:      !byte 9, 1, 7, 1, 5, 2, 2
                    !byte 3, 1, 5, 1, 7, 1, 9, 1
                    !byte 1, 1

d011_value:         !byte 0x0B
; ==============================================================================
                    !zone LIB
library:            !if LIB_INCLUDE = 1 {
                         !source"../../lib/library.asm"
                    }
; ==============================================================================
sprites_init:       lda #%11111000
                    sta 0xd015   ; enable sprites
                    sta 0xd01c   ; multicolor

                    lda #0
                    sta 0xD017
                    sta 0xD01D

                    lda #sprite_base + 0
                    sta vidmem0 + 0x3f8

                    lda #sprite_base + 1
                    sta vidmem0 + 0x3f9

                    lda #sprite_base + 2
                    sta vidmem0 + 0x3fa

                    lda #sprite_base + 3
                    sta vidmem0 + 0x3fb

                    lda #sprite_base + 4
                    sta vidmem0 + 0x3fc

                    lda #sprite_base + 0
                    sta vidmem0 + 0x3fd

                    lda #sprite_base + 1
                    sta vidmem0 + 0x3fe

                    lda #sprite_base + 2
                    sta vidmem0 + 0x3ff

                    lda #0x0a             ; sprite color
                    sta 0xd027
                    sta 0xd028
                    sta 0xd029
                    sta 0xd02a

                    lda #0x02
                    sta 0xd02b
                    sta 0xd02c

                    lda #0x04
                    sta 0xd02d
                    sta 0xd02e

                    lda #0x00
                    sta 0xd025           ; multicolor 1

                    lda #0x01
                    sta 0xd026           ; multicolor 2

                    ; sprite y pos
                    lda #0x0
                    sta 0xd001
                    sta 0xd003
                    sta 0xd005
                    sta 0xd007
                    sta 0xd009
                    sta 0xd00b
                    sta 0xd00d
                    sta 0xd00f
                    sta 0xd000
                    sta 0xd002
                    sta 0xd004
                    sta 0xd006
                    sta 0xd008
                    sta 0xd00a
                    sta 0xd00c
                    sta 0xd00e
                    rts

CONST_ANIMATION_STEPS = 9
NUM_SPRITES           = 4
ANIM_DELAY            = 5

var_delay           !byte $00
var_phase_sprite_0  !byte $00
var_phase_sprite_1  !byte $01
var_phase_sprite_2  !byte $02
var_phase_sprite_3  !byte $03
var_phase_sprite_4  !byte $04
var_phase_sprite_5  !byte $05
var_phase_sprite_6  !byte $06
var_phase_sprite_7  !byte $07

sprites_anim:       lda #ANIM_DELAY
                    beq +
                    dec sprites_anim+1
                    rts
+                   lda #ANIM_DELAY
                    sta sprites_anim+1

                    !for i, 7, (7-NUM_SPRITES) {
                        inc var_phase_sprite_0+i
                    }

                    !for i, 7, (7-NUM_SPRITES) {
                        lda var_phase_sprite_0 + i
                        cmp #CONST_ANIMATION_STEPS
                        bne +
                        lda #0                          ; reset counter for sprite
                        sta var_phase_sprite_0 + i
                        lda #( 2*i ) + 0
                        sta mod_new_x + 1
                        lda #( 2*i ) + 1
                        sta mod_new_y + 1
                        jsr get_new_pos
+                       lda #<vidmem0 + $3f8 + i
                        sta sprite_add + 1
                        lda #> vidmem0 + $3f8 + i
                        sta sprite_add + 2
                        lda var_phase_sprite_0 + i
                        jsr display_hearts
                    }
                    rts

;==========================================================
; display hearts
;==========================================================
display_hearts
                    tay
                    lda animationtable,y
sprite_add
                    sta $1000
                    rts



animationtable
                    !byte sprite_base + 1
                    !byte sprite_base + 2
                    !byte sprite_base + 3
                    !byte sprite_base + 4
                    !byte sprite_base + 5
                    !byte sprite_base + 4
                    !byte sprite_base + 3
                    !byte sprite_base + 1
                    !byte sprite_base + 0
;==========================================================
; get new position
;==========================================================

var_table_pos       !byte $00

get_new_pos
                    inc var_table_pos
                    ldx var_table_pos

                    cpx #100
                    bne +
                    ldx #0
                    stx var_table_pos

+
                    lda random_table_x,x
mod_new_x
                    sta $d000
                    lda random_table_y,x
mod_new_y
                    sta $d001
                    rts

random_table_x
!byte 097,149,188,071,122,073,218,125,209,204,114,240,110,253,072,170,115,092,161,150,236,215,185,171,083,138,219,173,208,102,082,193,200,127,202,223,254,214,137,167,186,077,103,130,244,094,155,169,179,148,242,104,076,212,143,152,230,141,159,211,198,133,184,084,187,070,176,250,172,067,134,232,135,177,154,160,246,225,247,233,081,145,229,235,074,164,144,255,066,189,089,120,080,199,181,183,196,139,165,192

random_table_y
!byte 117,161,104,158,114,127,207,170,162,143,200,191,100,168,083,102,090,126,136,181,087,091,120,151,135,065,172,073,133,115,082,180,208,210,205,220,174,152,107,148,145,195,206,215,189,106,110,199,150,214,141,155,198,069,134,176,094,178,086,112,166,122,171,109,067,216,144,095,218,159,079,081,098,123,188,213,139,160,183,116,156,153,203,062,197,138,089,194,201,192,088,077,185,066,068,217,131,186,105,187
; ==============================================================================
                    !zone DATA
                    *= sprite_data
                    !bin "includes/mikie.spd",6*64,3
data_area:
colram_src:         !bin "includes/pic_grrl.prg",1000,9000+2
