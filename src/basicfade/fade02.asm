DEBUG = 0
                    *= $1ffe
                    !byte <code_start
                    !byte >code_start
; ==============================================================================
ENABLE              = $20
DISABLE             = $2c

BLACK               = $00
WHITE               = $01
RED                 = $02
CYAN                = $03
PURPLE              = $04
GREEN               = $05
BLUE                = $06
YELLOW              = $07
ORANGE              = $08
BROWN               = $09
PINK                = $0A
DARK_GREY           = $0B
GREY                = $0C
LIGHT_GREEN         = $0D
LIGHT_BLUE          = $0E
LIGHT_GREY          = $0F

FADECOLOR           = BLACK
; ==============================================================================
irq0_line           = $02
irq0_d011           = irq0_line+1
irq1_line           = irq0_d011+1
irq1_d011           = irq1_line+1
d020_save           = irq1_d011+1
d021_save           = d020_save+1
irq_ready           = d021_save+1
; ==============================================================================
code_start          = $2000
vicbank             = $0000
charset0            = vicbank+$1000
charset1            = vicbank+$2800
vidmem0             = vicbank+$0400
;sprite_data         = vicbank+$0c00
;sprite_base         = <((sprite_data-vicbank)/$40)
dd00_val0           = <!(vicbank/$4000) & 3
d018_val0           = <(((vidmem0-vicbank)/$400) << 4)+ <(((charset0-vicbank)/$800) << 1)
d018_val1           = <(((vidmem0-vicbank)/$400) << 4)+ <(((charset1-vicbank)/$800) << 1)
; ==============================================================================
!zone MAIN
                    *= code_start
                    lda $d020
                    sta d020_save
                    lda $d021
                    sta d021_save
                    lda #$00
                    sta irq0_line
                    lda #$02
                    sta irq1_line
                    lda #$1b
                    sta irq0_d011
                    sta irq1_d011
                    lda #$00
                    ldx #>charset1
                    ldy #>(charset1+$800)
                    jsr lib_memfill
                    sei
                    lda #<irq0
                    sta $fffe
                    lda #>irq0
                    sta $ffff
                    lda irq0_d011
                    sta $d011
                    lda irq0_line
                    sta $d012
                    dec $d019
                    cli
mainloop:           jsr wait_irq
                    clc
                    lda irq1_line
                    adc #$02
                    sta irq1_line
                    bcc +
                    lda #$9b
                    sta irq1_d011
+                   lda irq1_line
                    cmp #$20
                    bne mainloop
                    lda irq1_d011
                    cmp #$9b
                    bne mainloop
                    lda #DISABLE
                    sta mod0
                    sta mod1
                    lda #$00
                    sta $d020
                    sta $d021
; ==============================================================================
code_exit:          sei
                    lda #<fw_irq
                    sta $fffe
                    lda #>fw_irq
                    sta $ffff
                    lda #$0b
                    sta $d011
                    lda #$00
                    sta $d012
                    dec $d019
                    cli
                    !if RELEASE = 1 {
                         jmp fw_back2framework
                    } else {
                         jmp *
                    }
; ==============================================================================
wait_irq:           lda #$00
                    sta irq_ready
-                   lda irq_ready
                    beq -
                    rts
; ==============================================================================
!zone LIB_MEMFILL
lib_memfill:        stx .from_hi+2
                    sty .to_hi+1
.loop:              ldx #0
.from_hi:           sta $0000,x
                    inx
                    bne .from_hi
                    inc .from_hi+2
                    ldy .from_hi+2
.to_hi:             cpy #0
                    bne .loop
                    rts
; ==============================================================================
!zone IRQs
irq0:               pha
                    lda $dd04
                    eor #7
                    sta *+4
                    bpl *+2
                    cmp #$c9
                    cmp #$c9
                    bit $ea24
                    bit $ea24
                    jmp *+8
                    nop
                    nop
                    jmp *+3
                    txa
                    pha
                    tya
                    pha
                    ldx #4
                    dex
                    bne *-1
                    lda #FADECOLOR
                    sta $d020
                    sta $d021
                    lda #d018_val1
                    sta $d018
                    lda irq1_line
                    sta $d012
                    lda irq1_d011
                    sta $d011
                    lda #<irq1
                    sta $fffe
                    lda #>irq1
                    sta $ffff
                    dec $d019
                    pla
                    tay
                    pla
                    tax
                    pla
                    rti

irq1:               pha
                    lda $dd04
                    eor #7
                    sta *+4
                    bpl *+2
                    cmp #$c9
                    cmp #$c9
                    bit $ea24
                    bit $ea24
                    jmp *+8
                    nop
                    nop
                    jmp *+3
                    txa
                    pha
                    tya
                    pha
                    ldx #1
                    dex
                    bne *-1
                    lda d020_save
mod0:               sta $d020
                    lda d021_save
mod1:               sta $d021
                    lda #d018_val0
                    sta $d018
                    lda irq0_line
                    sta $d012
                    lda irq0_d011
                    sta $d011
                    lda #<irq0
                    sta $fffe
                    lda #>irq0
                    sta $ffff
                    lda #$01
                    sta irq_ready
                    dec $d019
                    pla
                    tay
                    pla
                    tax
                    pla
                    rti
