DEBUG = 0
LIB_INCLUDE = 0
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 0
LIB_HEX2SCREEN = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 0
; ______________________________________________________________________________
black         = $00
white         = $01
red           = $02
cyan          = $03
purple        = $04
green         = $05
blue          = $06
yellow        = $07
orange        = $08
brown         = $09
pink          = $0a
dark_grey     = $0b
grey          = $0c
light_green   = $0d
light_blue    = $0e
light_grey    = $0f
; ______________________________________________________________________________
MEMCFG              = $35
IRQ_LINE0             = 6   ; ( $d011 = #$0b )
IRQ_LINE1             = 32    ; Wert + 256 ( $d011 = #$8b !! )
;
anzahl_rasterlines  = 4
speed1_rasterbar_moving = 5   ; größere Werte -> Ablauf langsamer !
speed2_rasterbar_moving = 1   ; größere Werte -> Ablauf langsamer !
;
charfillspeed   = $35   ; größere Werte -> Ablauf langsamer !
sourcescreenram   = $0400
destinationscreenram  = $3000
sourcecharset     = $D000
destinationcharset  = $2000
destinationcharsetreverse = $2400
destinationcharsettwo   = $2800
destinationcharsetreversetwo = $2C00
;
; ZP-Speicheradressen
fw_savea          = $02
fw_savex          = $03
fw_savey          = $04
save_d018           = $05
save_d018_charset_bits  = $06
saved_bordercolour    = $07
saved_backgroundcolour  = $08
save_01         = $09
basicfade_tmp_savea = $0a
basicfade_tmp_savex = $0b
basicfade_tmp_savey = $0c
pausenwert    = $0d
pre_ora_save    = $0e
enable_rasterbars = $0f
irq0_d012   = $10
irq1_d012   = $11
colour_between_rasterbars = $12
; ______________________________________________________________________________
                    !cpu 6510
                    !initmem $00
; ______________________________________________________________________________
; ______________________________________________ INCLUDES ______________________
                    !src "../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                        *= $0400
                        !bin "../../lib/framework/framework-noloader.prg",,2
                    }
; ______________________________________________________________________________
                    *= $1ffe
                    !byte <code_start
                    !byte >code_start
; ______________________________________________________________________________
;!fill $1000, 0    ; 4kb space for charset
;!fill $0400, 0    ; 1kb space for screenram
; ______________________________________________________________________________
; _________________________________________________________ CODE START__________
*= $3400

einschaltmeldung:   !if RELEASE = 0 {
                        !bin "includes/einschaltmeldung.prg",,2
                    }
code_start:         jmp init_code
; ______________________________________________________________________________
init_code:
                    lda #IRQ_LINE0
                    sta irq0_d012
                    lda #IRQ_LINE1
                    sta irq1_d012
                    lda #0
                    sta colour_between_rasterbars
                    sta enable_rasterbars
                    jsr init_vic
                    !if RELEASE = 1 { jsr copy_screenram }
                    jsr copy_rom_charset
                    lda #$C8    ; 1010 1000
                    ora save_d018_charset_bits
                    sta $d018 ; Screenram auf $3000, charset auf $2000
                    jsr FillColourRAMwithBorderColour
                    jmp mainloop
; ______________________________________________________________________________
                    !zone MAINLOOP
mainloop:           !if DEBUG = 1 { jmp + }
                    jsr charfill
+                   lda #$0b
                    sta $d011
                    jsr init_irq
                    sta enable_rasterbars
                    lda #speed1_rasterbar_moving
                    sta pausenwert

                    ldx #0
-                   lda UpperRasterBarPositionTable,x
                    sta irq0_d012
                    lda LowerRasterBarPositionTable,x
                    sta irq1_d012
                    lda LowerRasterBarPositionTable_d011,x
                    sta D011_value+1
                    jsr pause
                    inx
                    cpx #224
                    bne -

                    ldx #0
                    stx saved_bordercolour
                    lda #speed2_rasterbar_moving
                    sta pausenwert
                    jsr pause
                    jsr pause
                    jsr pause

-                   lda UpperRasterBarPositionTableEnd,x
                    sta irq0_d012
                    lda LowerRasterBarPositionTableEnd,x
                    sta irq1_d012
                    lda LowerRasterBarPositionTableEnd_d011,x
                    sta D011_value+1
                    jsr pause_short
                    inx
                    cpx #255
                    bne -

                    lda #0
                    sta $d020
                    lda #0
                    sta enable_rasterbars

                    !if RELEASE = 0 {
                        jmp *
                    } else  {
                        lda #0
                        sta enable_rasterbars
                        jmp code_exit
                    }
; ______________________________________________________________________________
                    !zone CODE_EXIT
code_exit:          sei     ; back to Krill-Loader
                    lda #<fw_irq
                    sta $fffe
                    lda #>fw_irq
                    sta $ffff
                    lda #$0b
                    sta $d011
                    lda #$00
                    sta $d012
                    dec $d019
                    lda save_d018
                    sta $d018
                    cli
                    !if RELEASE = 0 {
                        jmp *
                    } else {
                        jmp fw_back2framework
                    }
; ______________________________________________________________________________
; ______________________________________________________________________________
; ________________________________________________________ RASTER IRQ __________
                    !zone IRQ
                    !align 255,0,0
irq0:               sta fw_savea
                    stx fw_savex
                    sty fw_savey
                    lda enable_rasterbars
                    bne +
                    jmp irq0_end
+                   jsr fake
                    jsr fake
                    nop
                    nop
                    nop
                    ldx #0      ; 2
-                   lda rastertable1,x  ; 5
                    sta $d020   ; 4
                    jsr fake    ; 12
                    jsr fake    ; 12
                    jsr fake    ; 12
                    jsr fake    ; 12
                    inx     ; 2
                    cpx #anzahl_rasterlines ; 2
                    bne -     ; 3
irq0_end:           nop
                    lda saved_bordercolour
                    sta $d020
                    ldx #<irq1
                    ldy #>irq1
                    lda irq1_d012
                    sta $D012
D011_value:         lda #$8b
                    sta $d011
                    ;                ldx #<irq1
                    stx $FFFE
                    ;                ldy #>irq1
                    sty $FFFF
                    lda #$ff
                    sta $D019
                    lda fw_savea
                    ldx fw_savex
                    ldy fw_savey
                    rti
; ________________________________________________________ RASTERTABLES _________
rastertable1:       !byte blue,grey,light_grey,white
rastertable2:       !byte white,light_grey,grey,blue
; ________________________________________________________ RASTER IRQ1 __________
                    !align 255,0,0
irq1:               sta fw_savea
                    stx fw_savex
                    sty fw_savey
                    lda enable_rasterbars
                    bne +
                    jmp irq1_end
+                   jsr fake
                    jsr fake
                    nop
                    nop
                    nop
                    ldx #0      ; 2
-                   lda rastertable2,x  ; 5
                    sta $d020   ; 4
                    jsr fake    ; 12
                    jsr fake    ; 12
                    jsr fake    ; 12
                    jsr fake    ; 12
                    inx     ; 2
                    cpx #anzahl_rasterlines ; 2
                    bne -     ; 3
irq1_end:           nop
                    lda #light_grey
                    sta $d020
                    ldx #<irq0
                    ldy #>irq0
                    lda irq0_d012
                    sta $D012
                    lda $d011
                    and #%01111111
                    sta $d011
                    stx $FFFE
                    sty $FFFF
                    lda #$ff
                    sta $D019
                    lda fw_savea
                    ldx fw_savex
                    ldy fw_savey
                    rti
; ______________________________________________________________________________
; ______________________________________________________________________________
; ______________________________________________________________________________
; ______________________________________________________________________________
; ______________________________________________________ subroutines ___________
; ______________________________________________________________________________
library:            !if LIB_INCLUDE = 1 {
                         !source"../../lib/library.asm"
                    }
; ______________________________________________________________________________
                    !zone INIT
init_irq:           sei
                    lda #IRQ_LINE0
                    sta $D012
                    lda #<irq0
                    sta $FFFE
                    lda #>irq0
                    sta $FFFF
                    lda #$FF
                    sta $D019
                    lda #$01
                    sta $D01A
                    cli
fake:               rts
; ______________________________________________________________________________

init_vic:           lda $d018
                    sta save_d018
                    and #%00000010
                    sta save_d018_charset_bits
                    lda $d020
                    sta saved_bordercolour
                    lda $d021
                    sta saved_backgroundcolour
                    !if RELEASE = 0 {
                        ldx #0
                        ldy #$20    ; code for "space"
-                       lda einschaltmeldung,x
                        sta destinationscreenram,x
                        tya
                        sta destinationscreenram+$100,x
                        sta destinationscreenram+$200,x
                        sta destinationscreenram+$2e8,x
                        inx
                        bne -
                    }
                    rts
; ______________________________________________________________________________
FillColourRAMwithBorderColour:
                    ldx #0
                    lda $d020
-                   sta $d800,x
                    sta $d900,x
                    sta $da00,x
                    sta $dae8,x
                    inx
                    bne -
                    rts
; ______________________________________________________________________________
copy_screenram:     ldx #0
-                   lda sourcescreenram,x
                    sta destinationscreenram,x
                    lda sourcescreenram+$100,x
                    sta destinationscreenram+$100,x
                    lda sourcescreenram+$200,x
                    sta destinationscreenram+$200,x
                    lda sourcescreenram+$2e8,x
                    sta destinationscreenram+$2e8,x
                    inx
                    bne -
                    rts
; ______________________________________________________________________________
copy_rom_charset:   sei
                    lda $01
                    sta save_01
                    and #%11111011
                    sta $01
                    ldx #0
-                   lda sourcecharset,x
                    sta destinationcharset,x
                    lda sourcecharset+$100,x
                    sta destinationcharset+$100,x
                    lda sourcecharset+$200,x
                    sta destinationcharset+$200,x
                    lda sourcecharset+$300,x
                    sta destinationcharset+$300,x
                    lda sourcecharset+$400,x
                    sta destinationcharset+$400,x
                    lda sourcecharset+$500,x
                    sta destinationcharset+$500,x
                    lda sourcecharset+$600,x
                    sta destinationcharset+$600,x
                    lda sourcecharset+$700,x
                    sta destinationcharset+$700,x
                    lda sourcecharset+$800,x
                    sta destinationcharset+$800,x
                    lda sourcecharset+$900,x
                    sta destinationcharset+$900,x
                    lda sourcecharset+$a00,x
                    sta destinationcharset+$a00,x
                    lda sourcecharset+$b00,x
                    sta destinationcharset+$b00,x
                    lda sourcecharset+$c00,x
                    sta destinationcharset+$c00,x
                    lda sourcecharset+$d00,x
                    sta destinationcharset+$d00,x
                    lda sourcecharset+$e00,x
                    sta destinationcharset+$e00,x
                    lda sourcecharset+$f00,x
                    sta destinationcharset+$f00,x
                    inx
                    bne -
                    lda save_01
                    sta $01
                    cli
                    rts
; ______________________________________________________________________________
pause:    ; pause für mainloop - in der variablen 'pausenwert' muss der Pausenwert übergeben werden
                    sta basicfade_tmp_savea
                    stx basicfade_tmp_savex
                    sty basicfade_tmp_savey
                    ldy pausenwert
                    ldx #0
-                   dex
                    bne -
                    dey
                    bne -
                    ldy basicfade_tmp_savey
                    ldx basicfade_tmp_savex
                    lda basicfade_tmp_savea
                    rts
; ______________________________________________________________________________
pause_short:  ; pause für mainloop - in der variablen 'pausenwert' muss der Pausenwert übergeben werden
                    sta basicfade_tmp_savea
                    stx basicfade_tmp_savex
                    sty basicfade_tmp_savey
                    ldx #160
-                   dex
                    bne -
                    ldy basicfade_tmp_savey
                    ldx basicfade_tmp_savex
                    lda basicfade_tmp_savea
                    rts
; ______________________________________________________________________________
charfill:           lda #charfillspeed      ; speed des Ausblendeffekts
                    sta pausenwert
                    ldx #0
-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_sta
                    jsr pause
                    inx
                    inx
                    cpx #16
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_ora_sta
                    jsr pause
                    inx
                    inx
                    cpx #30
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_sta
                    jsr pause
                    inx
                    inx
                    cpx #44
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_ora_sta
                    jsr pause
                    inx
                    inx
                    cpx #56
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_sta
                    jsr pause
                    inx
                    inx
                    cpx #68
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_ora_sta
                    jsr pause
                    inx
                    inx
                    cpx #78
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_sta
                    jsr pause
                    inx
                    inx
                    cpx #88
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_ora_sta
                    jsr pause
                    inx
                    inx
                    cpx #96
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_sta
                    jsr pause
                    inx
                    inx
                    cpx #104
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_ora_sta
                    jsr pause
                    inx
                    inx
                    cpx #110
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_sta
                    jsr pause
                    inx
                    inx
                    cpx #116
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_ora_sta
                    jsr pause
                    inx
                    inx
                    cpx #120
                    bne -

-                   lda CharFillTable+1,x
                    tay
                    lda CharFillTable,x
                    jsr speedcode_sta
                    jsr pause
                    inx
                    inx
                    cpx #130
                    bne -

                    rts
; ______________________________________________________________________________
speedcode_sta:
;  !src "includes/speedcode_sta.asm"
sta destinationcharset,y
sta destinationcharsetreverse,y
sta destinationcharsettwo,y
sta destinationcharsetreversetwo,y
;
!set sc_counter = 0
!for RelAddress, 0, 1022 {
  !set sc_counter = sc_counter + 1
  !if sc_counter = 8 {
    !set sc_counter = 0
    sta destinationcharset+RelAddress,y
    sta destinationcharsetreverse+RelAddress,y
    sta destinationcharsettwo+RelAddress,y
    sta destinationcharsetreversetwo+RelAddress,y
    }
}
rts
; ______________________________________________________________________________
speedcode_ora_sta:
;  !src "includes/speedcode_ora_sta.asm"
sta pre_ora_save
ora destinationcharset,y
sta destinationcharset,y
lda pre_ora_save
ora destinationcharsetreverse,y
sta destinationcharsetreverse,y
lda pre_ora_save
ora destinationcharsettwo,y
sta destinationcharsettwo,y
lda pre_ora_save
ora destinationcharsetreversetwo,y
sta destinationcharsetreversetwo,y
;
!set sc_counter = 0
!for RelAddress, 0, 1022 {
  !set sc_counter = sc_counter + 1
  !if sc_counter = 8 {
    !set sc_counter = 0
    lda pre_ora_save
    ora destinationcharset+RelAddress,y
    sta destinationcharset+RelAddress,y
    lda pre_ora_save
    ora destinationcharsetreverse+RelAddress,y
    sta destinationcharsetreverse+RelAddress,y
    lda pre_ora_save
    ora destinationcharsettwo+RelAddress,y
    sta destinationcharsettwo+RelAddress,y
    lda pre_ora_save
    ora destinationcharsetreversetwo+RelAddress,y
    sta destinationcharsetreversetwo+RelAddress,y
    }
}
rts
; ______________________________________________________________________________
;!align 255,0,0
; CharFillTable 1-Wert, 2-Index im char
CharFillTable:
!byte %00000001, 0
!byte %00000011, 0
!byte %00000111, 0
!byte %00001111, 0
!byte %00011111, 0
!byte %00111111, 0
!byte %01111111, 0
!byte %11111111, 0  ;x=15
!byte %10000000, 1
!byte %10000000, 2
!byte %10000000, 3
!byte %10000000, 4
!byte %10000000, 5
!byte %10000000, 6
!byte %10000000, 7  ;x=29
!byte %11000000, 7
!byte %11100000, 7
!byte %11110000, 7
!byte %11111000, 7
!byte %11111100, 7
!byte %11111110, 7
!byte %11111111, 7  ;x=43
!byte %10000001, 6
!byte %10000001, 5
!byte %10000001, 4
!byte %10000001, 3
!byte %10000001, 2
!byte %10000001, 1  ;x=55
!byte %10000011, 1
!byte %10000111, 1
!byte %10001111, 1
!byte %10011111, 1
!byte %10111111, 1
!byte %11111111, 1  ;x=67
!byte %11000001, 2
!byte %11000001, 3
!byte %11000001, 4
!byte %11000001, 5
!byte %11000001, 6  ;x=77
!byte %11100001, 6
!byte %11110001, 6
!byte %11111001, 6
!byte %11111101, 6
!byte %11111111, 6  ;x=87
!byte %11000011, 5
!byte %11000011, 4
!byte %11000011, 3
!byte %11000011, 2  ;x=95
!byte %11000111, 2
!byte %11001111, 2
!byte %11011111, 2
!byte %11111111, 2  ;x=103
!byte %11100011, 3
!byte %11100011, 4
!byte %11100011, 5  ;x=109
!byte %11110011, 5
!byte %11111011, 5
!byte %11111111, 5  ;x=115
!byte %11100111, 4
!byte %11100111, 3  ;x=119
!byte %11101111, 3
!byte %11111111, 3  ;x=123
!byte %11110011, 4
!byte %11111011, 4
!byte %11111111, 4  ;x=129
; ______________________________________________________________________________
  !src "includes/sinustabelle_UpperRaster2.asm"
  !src "includes/sinustabelle_LowerRaster2.asm"
; ______________________________________________________________________________

