DEBUG = 0
; ==============================================================================
ENABLE              = $20
DISABLE             = $2c

BLACK               = $00
WHITE               = $01
RED                 = $02
CYAN                = $03
PURPLE              = $04
GREEN               = $05
BLUE                = $06
YELLOW              = $07
ORANGE              = $08
BROWN               = $09
PINK                = $0A
DARK_GREY           = $0B
GREY                = $0C
LIGHT_GREEN         = $0D
LIGHT_BLUE          = $0E
LIGHT_GREY          = $0F

WAITFRAMES          = $09

; ==============================================================================
code_start          = $2000
vicbank             = $0000
charset0            = vicbank+$1000
vidmem0             = vicbank+$0400
;sprite_data         = vicbank+$0c00
;sprite_base         = <((sprite_data-vicbank)/$40)
dd00_val0           = <!(vicbank/$4000) & 3
d018_val0           = <(((vidmem0-vicbank)/$400) << 4)+ <(((charset0-vicbank)/$800) << 1)

; ==============================================================================
                    *= $1ffe
                    !byte <code_start
                    !byte >code_start
                    *= code_start
init_code:          lda $d020
                    sta d020_save+1
                    !if RELEASE = 0 {
                         lda #$1b
                         sta $d011
                    }
squareloop:         ldx #0
                    lda coords_x,x
                    bmi +
                    sta .index_x+1
                    lda coords_y,x
                    sta .index_y+1
.index_y:           ldy #$00
.index_x:           ldx #$00
                    lda x_tab,x
                    sta square_x_start
                    lda y_tab,y
                    sta square_y_start
                    jsr wait
                    lda #$f0
                    cmp $d012
                    bne *-3
                    !if DEBUG = 1 {
                         inc $d020
                    }
                    jsr print_square
                    !if DEBUG = 1 {
                         dec $d020
                    }
                    inc squareloop+1
                    jmp squareloop
+                   lda #$0b
                    sta $d011
                    ldx #0
-                   jsr wait
                    lda colfade_tab,x
                    bmi .exit
                    sta $d020
                    sta $d021
                    inx
                    jmp -
.exit:              !if RELEASE = 1 {
                         jmp fw_back2framework
                    } else {
                         jmp *
                    }
wait_frame:         jmp fw_wait_irq
wait:               lda .wait_count
                    beq .wait_reset
                    dec .wait_count
                    jsr wait_frame
                    jmp wait
.wait_reset:        lda #WAITFRAMES
                    sta .wait_count
                    rts
.wait_count:        !byte WAITFRAMES
!zone PRINT_CHAR
print_char:         pha
                    lda .hibyte_tab,y
                    sta .screen+2
                    lda .lobyte_tab,y
                    sta .screen+1
                    pla
.screen:            sta $0000,x
                    rts
.hibyte_tab:        !for i, 0, 24 {
                    !byte >(vidmem0+(i*40)) }
.lobyte_tab:        !for i, 0, 24 {
                    !byte <(vidmem0+(i*40)) }
!zone SET_COLOR
set_color:          pha
                    lda .hibyte_tab,y
                    sta .colram+2
                    lda .lobyte_tab,y
                    sta .colram+1
                    pla
.colram:            sta $0000,x
                    rts
.hibyte_tab:        !for i, 0, 24 {
                    !byte >($d800+(i*40)) }
.lobyte_tab:        !for i, 0, 24 {
                    !byte <($d800+(i*40)) }
!zone PRINT_SQUARE
print_square:       txa
                    pha
                    tya
                    pha
                    lda square_x_start
                    sta .cur_x+1
                    clc
                    adc square_width
                    sta .dest_x+1
                    lda square_y_start
                    sta .cur_y+1
                    clc
                    adc square_height
                    sta .dest_y+1
.cur_y:             ldy #$00
.cur_x:             ldx #$00
                    lda #$a0
                    jsr print_char
d020_save:          lda #BLACK
                    jsr set_color
.dest_x:            lda #$00
                    cmp .cur_x+1
                    beq .dest_y
                    inc .cur_x+1
                    jmp .cur_x
.dest_y:            lda #$00
                    cmp .cur_y+1
                    beq .done
                    lda square_x_start
                    sta .cur_x+1
                    inc .cur_y+1
                    jmp .cur_y
.done:              pla
                    tay
                    pla
                    tax
                    rts
square_x_start:     !byte $00
square_y_start:     !byte $00
square_height:      !byte $04
square_width:       !byte $04
; ==============================================================================
x_tab:              !byte $00, $05, $0a, $0f, $14, $19, $1e, $23
y_tab:              !byte $00, $05, $0a, $0f, $14
coords_x:           !byte 0, 1, 2, 3, 4, 5, 6, 7
                    !byte 7, 7, 7, 7
                    !byte 6, 5, 4, 3, 2, 1, 0
                    !byte 0, 0, 0
                    !byte 1, 2, 3, 4, 5, 6
                    !byte 6, 6
                    !byte 5, 4, 3, 2, 1
                    !byte 1, 2, 3, 4, 5
                    !byte $ff
coords_y:           !byte 0, 0, 0, 0, 0, 0, 0, 0
                    !byte 1, 2, 3, 4
                    !byte 4, 4, 4, 4, 4, 4, 4
                    !byte 3, 2, 1
                    !byte 1, 1, 1, 1, 1, 1
                    !byte 2, 3
                    !byte 3, 3, 3, 3, 3
                    !byte 2, 2, 2, 2, 2
                    !byte $ff
; ==============================================================================
colfade_tab:        !byte $0e, $03, $0d, $01
                    !byte $01, $0d, $03, $0c, $04, $02, $09, $00
                    !byte $ff
