EFFECT = 3
                    !cpu 6510
                    !source "../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !if EFFECT = 1 {
                         !source "fade01.asm"
                    }
                    !if EFFECT = 2 {
                         !source "fade02.asm"
                    }
                    !if EFFECT = 3 {
                         !source "fade03.asm"
        }

