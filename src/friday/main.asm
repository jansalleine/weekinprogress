DEBUG = 0
LIB_INCLUDE = 0
LIB_BITMAPFILL = 0
LIB_COLRAMFILL = 0
LIB_HEX2SCREEN = 0
LIB_MEMFILL = 0
LIB_VIDMEMFILL = 0

BLACK               = 0x00
WHITE               = 0x01
RED                 = 0x02
CYAN                = 0x03
PURPLE              = 0x04
GREEN               = 0x05
BLUE                = 0x06
YELLOW              = 0x07
ORANGE              = 0x08
BROWN               = 0x09
PINK                = 0x0A
DARK_GREY           = 0x0B
GREY                = 0x0C
LIGHT_GREEN         = 0x0D
LIGHT_BLUE          = 0x0E
LIGHT_GREY          = 0x0F

counter             = $20   ; zählt die animationen

vicbank0            = 0x4000
bitmap0             = vicbank0+0x2000
vidmem0             = vicbank0+0x1C00
colram              = 0xD800
dd00_val0           = <!(vicbank0/0x4000) & 3
d018_val0           = <(((vidmem0-vicbank0)/0x400) << 4)+ <(((bitmap0-vicbank0)/0x800) << 1)


                    !cpu 6510
                    !source"../../lib/framework/frameworksymbols.inc"
                    !if RELEASE = 0 {
                         *= $0400
                         !bin "../../lib/framework/framework-noloader.prg",,2
                    }
                    !source "../../lib/sync.asm"
                    * = bitmap0
                    !bin "includes/bitmap1.bin"

                    * = vidmem0
                    !bin "includes/screen1.bin"

                    *= $1ffe
                    !byte <code_start
                    !byte >code_start
; ==============================================================================
code_start:         lda #0x0B
                    sta 0xD011
                    !if RELEASE=1 { +sync 0x0016B9 + ( 11 * SAFETY_OFFSET ) }
                    jsr fw_wait_irq
                    lda #0x18
                    sta 0xd016
                    lda #d018_val0
                    sta $d018
                    lda #0
                    sta $d020
                    lda #DARK_GREY
                    sta $d021

                    lda #dd00_val0
                    sta $dd00

                    jsr setcolor
                    jsr fw_wait_irq
                    lda #$3b
                    sta $d011

                   ;jmp *

                    lda #0
                    sta counter
; ==============================================================================
; 16 animationen
; ==============================================================================

mainloop:           ldx #$10          ;zeitverzögerung
loop3:              jsr fw_wait_irq
                    dex
                    bne loop3

                    jsr switch

                    inc counter
                    lda counter
                    cmp #16           ; = anzahl der animationen
                    bne mainloop

; ==============================================================================
; exit
; ==============================================================================
code_exit:          jsr fw_wait_irq
                    lda #$0b
                    sta $d011
                    !if RELEASE = 0 {
                      jmp *
                    } else {
                      jmp fw_back2framework
                    }
; ==============================================================================
; color ram kopieren
; ==============================================================================
setcolor:
                    ldx #0
-                   lda colram_src+0x000,x
                    sta 0xD800+0x000,x
                    lda colram_src+0x100,x
                    sta 0xD800+0x100,x
                    lda colram_src+0x200,x
                    sta 0xD800+0x200,x
                    lda colram_src+0x2e8,x
                    sta 0xD800+0x2e8,x
                    inx
                    bne -
                    rts
switch:             lda #0
                    beq +
                    dec switch+1
                    jmp switch1
+                   lda #1
                    sta switch+1
switch0:            jsr bitmap1_bin_to_bitmap2_bin
                    jsr screen1_bin_to_screen2_bin
                    jsr color1_bin_to_color2_bin
                    rts
switch1:            jsr bitmap2_bin_to_bitmap1_bin
                    jsr screen2_bin_to_screen1_bin
                    jsr color2_bin_to_color1_bin
                    rts
; ==============================================================================
library:            !if LIB_INCLUDE = 1 {
                         !source"../../lib/library.asm"
                    }
; ==============================================================================
colram_src:         !bin "includes/color1.bin"
; ==============================================================================
                    !source "includes/sc_bmp.asm"
                    !source "includes/sc_col.asm"
                    !source "includes/sc_scr.asm"
