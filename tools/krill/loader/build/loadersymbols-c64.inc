; zeropage
loadaddrlo      = $e0
loader_zp_first = $e0
loadaddrhi      = $e1
decdestlo       = $e2
decdesthi       = $e3
loader_zp_last  = $ee

; resident
loadcompd       = $0815
loadedtab       = $0a24
decrunch        = $0a44

; install
install         = $3054
