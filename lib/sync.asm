SAFETY_OFFSET       = 50

                    !macro sync .i {
-                       lda fw_global_ct
                        cmp #<(.i AND 0x0000FF)
                        bne -
                        lda fw_global_ct+1
                        cmp #<((.i AND 0x00FF00) >> 8 )
                        bne -
                        lda fw_global_ct+2
                        cmp #<((.i AND 0xFF0000) >> 16 )
                        bne -
                    }
